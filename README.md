# README #

### General description

Sequential model assessment for multi-objective optimization. Here, the optimisation starts with a simple/approximative evaluation model and later continues with a detailed model.


### More information
Method should work with any two evaluation models.
An example is provided, where the method is used in the context of building energy model optimization: RC-model is used as a first simple evaluation model and a whole building energy simulation model as second model.
More information can be found in "GenericSeqOpt/Tutorial.txt".

### Related Publication
Waibel, C., Ramallo-González, A.P., Evins, R., Carmeliet, J. (2015) Reducing the Computing Time of Multi-Objective Building Optimisation using Self-Adaptive Sequential Model Assessment. In: 14th International Conference of the International Building Performance Simulation Association (IBPSA), BS 2015, Hyderabad, India, December 7th – 9th 2015.

DOI: http://www.ibpsa.org/proceedings/BS2015/p2209.pdf

### Authors

Christoph Waibel, Alfonso Ramallo Gonzalez, Ralph Evins

### License / Software used

NSGA2
Matlab implementation by: 
Copyright (c) 2009, Aravind Seshadri

BRCM toolbox:
www.brcm.ethz.ch/

EnergyPlus:
https://energyplus.net/