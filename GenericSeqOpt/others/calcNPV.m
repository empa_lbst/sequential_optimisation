function [NPV] = calcNPV(InvestmentCost, TimeFrame, DiscountRate)

NPV=InvestmentCost*((1+DiscountRate)^TimeFrame);


end