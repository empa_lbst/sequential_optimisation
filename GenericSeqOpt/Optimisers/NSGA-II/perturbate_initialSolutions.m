function f = perturbate_initialSolutions(f_, N, M, V, min_range, max_range, IntegerVariables, evalFnc, evalGen, get_input)


%% function f = evaluate_initialSolutions(initialSet, N, M, V, min_range, max_range, IntegerVariables, evalFnc, evalGen, get_input)
% This function evaluates the initial solution set.
% Each chromosome has the following at this stage
%       * set of decision variables
%       * objective function values
% 
% where,
% N - Population size
% M - Number of objective functions
% V - Number of decision variables
% min_range - A vector of decimal values which indicate the minimum value
% for each decision variable.
% max_range - Vector of maximum possible values for decision variables.

perturbation = 0.5; %perturbate all variables randomly by up to 50% of their initial value
min = min_range;
max = max_range;

% K is the total number of array elements. For ease of computation decision
% variables and objective functions are concatenated to form a single
% array. For crossover and mutation only the decision variables are used
% while for selection, only the objective variable are utilized.

K = M + V;

%% Initialize each chromosome
% For each chromosome perform the following (N is the population size)
for i = 1 : N
    % Initialize the decision variables based on the minimum and maximum
    % possible values. V is the number of decision variable. A random
    % number is picked between the minimum and maximum possible values for
    % the each decision variable.
   for j = 1 : V
        %f(i,j) = min(j) + (max(j) - min(j))*rand(1);
        minP= f_(i,j) - (((max(j)-min(j))/2)*perturbation);
        if minP < min(j)
            minP = min(j);
        end
        maxP = f_(i,j) + (((max(j)-min(j))/2)*perturbation);
        if maxP > max(j)
            maxP = max(j);
        end
        f(i,j) = minP + (maxP - minP)*rand(1);
        
        if IntegerVariables(j) == 1
           f(i,j)=round(f(i,j)); 
        end
    end 
    % For ease of computation and handling data the chromosome also has the
    % vlaue of the objective function concatenated at the end. The elements
    % V + 1 to K has the objective function valued. 
    % The function evaluate_objective takes one chromosome at a time,
    % infact only the decision variables are passed to the function along
    % with information about the number of objective functions which are
    % processed and returns the value for the objective functions. These
    % values are now stored at the end of the chromosome itself.
    [out1,out2]= evalFnc(f(i,1:V), evalGen, i, get_input);
    f(i,V + 1: K) = [out1,out2];
end


% %!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
% %!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
% %!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
% %PARFOR???? NOT WORKING WITH IF ELSE.. in eval fnc
% for i=1:N
%     [out1(i),out2(i)]= evalFnc(f(i,1:V), evalGen, i, get_input); %!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
%    % ['individual ', num2str(i), ' - 1st Generation']
% end
% 
% for i=1:N
%     f(i,V + 1: K) = [out1(i),out2(i)];
% end





