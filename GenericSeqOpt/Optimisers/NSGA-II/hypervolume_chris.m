%% Hypervolume_chris
% Ralph's Code, changed to work with Copyright (c) 2009, Aravind Seshadri
% NSGA-II code...
% ONLY WORKS WITH 2 OBJECTIVES M=2
% Inputs:
% 1. chromo:    chromosome. 
%                   (l x n) matrix
%                   n: V variables, M objectives, Rank, crowding distance,
%                       contributing hypervolume
%                   l: individuals
% 2. ref:       reference point
% ...
% Calculate the hypervolume enclosed by the Paerto front of solutions in pop
% Ref is the nadir point
% the hypervolume is the area between the front and the nadir point
%                    
%    �------------n
%    �            � 
%    x--�         �
%       �         �
%       x----�    �
%            �    �
%            x----� 
%
%%
function v = hypervolume_chris(chromo,ref, M, V)

%O(1,:) = [pop([pop.rank]==1).obj1];
%O(2,:) = [pop([pop.rank]==1).obj2];
O(:,1) = chromo(chromo(:,V+M+1)==1, V+M-1);
O(:,2) = chromo(chromo(:,V+M+1)==1, V+M);

O = sortrows(O);
w = ref(1) - O(1,1); % block width
h = ref(2) - O(1,2); % block height
v = h*w;
if size(O,1) > 1
    for i = 2:size(O,1)
        w = O(i-1,2) - O(i,2); % block width
        h = ref(1) - O(i,1); % block height
        a = h*w;
        v = v + a;
    end
end