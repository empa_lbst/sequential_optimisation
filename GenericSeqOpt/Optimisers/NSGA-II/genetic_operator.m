function f  = genetic_operator(parent_chromosome, M, V, mu, mum, l_limit, u_limit, IntegerVariables, evalFnc, evalGen, get_input, CrossoverOperator)

%% function f  = genetic_operator(parent_chromosome, M, V, mu, mum, l_limit, u_limit)
% 
% This function is utilized to produce offsprings from parent chromosomes.
% The genetic operators corssover and mutation which are carried out with
% slight modifications from the original design. For more information read
% the document enclosed. 
%
% parent_chromosome - the set of selected chromosomes.
% M - number of objective functions
% V - number of decision varaiables
% mu - distribution index for crossover (read the enlcosed pdf file)
% mum - distribution index for mutation (read the enclosed pdf file)
% l_limit - a vector of lower limit for the corresponding decsion variables
% u_limit - a vector of upper limit for the corresponding decsion variables
%
% The genetic operation is performed only on the decision variables, that
% is the first V elements in the chromosome vector. 

%  Copyright (c) 2009, Aravind Seshadri
%  All rights reserved.
%
%  Redistribution and use in source and binary forms, with or without 
%  modification, are permitted provided that the following conditions are 
%  met:
%
%     * Redistributions of source code must retain the above copyright 
%       notice, this list of conditions and the following disclaimer.
%     * Redistributions in binary form must reproduce the above copyright 
%       notice, this list of conditions and the following disclaimer in 
%       the documentation and/or other materials provided with the distribution
%      
%  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" 
%  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
%  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE 
%  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE 
%  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
%  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
%  SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
%  INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
%  CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) 
%  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
%  POSSIBILITY OF SUCH DAMAGE.

%% Settings
d=0.5; % for intermediate recombination. extension of hypercube



%%
[N,m] = size(parent_chromosome);

clear m
p = 1;
% Flags used to set if crossover and mutation were actually performed. 
was_crossover = 0;
was_mutation = 0;


clear parchild1;
clear parchild2;
clear child;
clear child_1;
clear child_2;
clear child_3;

crossCount=0;
mutCount=0;
for i=1:N
    % with 90% probability perform crossover. else mutate
    if rand(1) < 0.9
        crossCount=crossCount+1;
        crossi(crossCount)=i;
    else
        mutCount=mutCount+1;
        muti(mutCount)=i;
    end
end




for i=1:crossCount
    %child_1(i) = [];
    %child_2(i) = [];
    % Select the first parent
    parent_1 = round(N*rand(1));
    if parent_1 < 1
        parent_1 = 1;
    end
    % Select the second parent
    parent_2 = round(N*rand(1));
    if parent_2 < 1
        parent_2 = 1;
    end
    % Make sure both the parents are not the same.
    while isequal(parent_chromosome(parent_1,:),parent_chromosome(parent_2,:))
        parent_2 = round(N*rand(1));
        if parent_2 < 1
            parent_2 = 1;
        end
    end
    % Get the chromosome information for each randomnly selected
    % parents
    parent_1 = parent_chromosome(parent_1,:);
    parent_2 = parent_chromosome(parent_2,:);
    % Perform corssover for each decision variable in the chromosome.
    for j = 1 : V
        if IntegerVariables(j) == 0
            switch CrossoverOperator
                case 'SBX'
                    %% SBX (Simulated Binary Crossover).
                    %For more information about SBX refer the enclosed pdf file.
                    %Generate a random number
                    u = rand(1);
                    if u <= 0.5
                        bq = (2*u)^(1/(mu+1));
                    else
                        bq = (1/(2*(1 - u)))^(1/(mu+1));
                    end
                    % Generate the jth element of first child
                    child_1(i,j) = ...
                        0.5*(((1 + bq)*parent_1(j)) + (1 - bq)*parent_2(j));
                    % Generate the jth element of second child
                    child_2(i,j) = ...
                        0.5*(((1 - bq)*parent_1(j)) + (1 + bq)*parent_2(j));
                    
                    
                case 'Intermediate'
                    %% Intermediate Crossover
                    pointer=rand(1)*(1+2*d)-d;
                    child_1(i,j) = parent_1(j) * pointer + parent_2(j) * (1-pointer);
                    pointer=rand(1)*(1+2*d)-d;
                    child_2(i,j) = parent_1(j) * pointer + parent_2(j) * (1-pointer);
            end
            
            
            
            
            
        else
            %% binary encoding for Integer Variables, single point crossover (per variable)
            sortedUB = sort(u_limit(j),2,'descend');
            BitStringLength = length(dec2bin(sortedUB(1))); %taking largest value to define the necessary bitstring length
            
            pointer=floor(rand(1)*BitStringLength);
            bitstringA=dec2bin(parent_1(j),BitStringLength);
            bitstringB=dec2bin(parent_2(j),BitStringLength);
            NewStringA=[bitstringA(1:pointer),bitstringB(pointer+1:BitStringLength)];
            NewStringB=[bitstringB(1:pointer),bitstringA(pointer+1:BitStringLength)];
            child_1(i,j)=bin2dec(NewStringA);
            child_2(i,j)=bin2dec(NewStringB);
        end
        
        % Make sure that the generated element is within the specified
            % decision space else set it to the appropriate extrema.
        if child_1(i,j) > u_limit(j)
            child_1(i,j) = u_limit(j);
        elseif child_1(i,j) < l_limit(j)
            child_1(i,j) = l_limit(j);
        end
        if child_2(i,j) > u_limit(j)
            child_2(i,j) = u_limit(j);
        elseif child_2(i,j) < l_limit(j)
            child_2(i,j) = l_limit(j);
        end
        
    end
end


%!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
%!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
%!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
%!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
%PARFOR NOT WORKING BECAUSE OF IF ELSE in eval fnc
childCounter=1;
for i=1:crossCount
    % Evaluate the objective function for the offsprings and as before
    % concatenate the offspring chromosome with objective value.
    [outt1,outt2]=evalFnc(child_1(i,:),  evalGen, childCounter, get_input); %!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    childCounter = childCounter+1;
    out1a(i)=outt1;
    out2a(i)=outt2;
    [outt1,outt2]= evalFnc(child_2(i,:), evalGen, childCounter, get_input); %!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    childCounter = childCounter+1;
    out1b(i)=outt1;
    out2b(i)=outt2;

    
    % Set the crossover flag. When crossover is performed two children
    % are generate, while when mutation is performed only only child is
    % generated. 
end

for i=1:crossCount
    child_1(i,V + 1: M + V) = [out1a(i),out2a(i)];
    child_2(i,V + 1: M + V) = [out1b(i),out2b(i)];

    parchild1(i,:)=child_1(i,:);
    parchild2(i,:)=child_2(i,:);
end
    
d=0.5;
if mutCount >0
    for i=1:mutCount
        % Select at random the parent.
        parent_3 = round(N*rand(1));
        if parent_3 < 1
            parent_3 = 1;
        end
        % Get the chromosome information for the randomnly selected parent.
        child_3(i,:) = parent_chromosome(parent_3,:);
        % Perform mutation on eact element of the selected parent.
        for j = 1 : V
%             r = rand(1);
%             if r < 0.5
%                 delta = (2*r)^(1/(mum+1)) - 1;
%             else
%                 delta = 1 - (2*(1 - r))^(1/(mum+1));
%             end
       
%             % Generate the corresponding child element.
%             child_3(i,j) = child_3(i,j) + delta; 
            pointer=rand(1)*(1+2*d)-d;
            child_3(i,j)=child_3(i,j)*pointer;






            % Make sure that the generated element is within the decision
            % space.
            if child_3(i,j) > u_limit(j)
                child_3(i,j) = u_limit(j);
            elseif child_3(i,j) < l_limit(j)
                child_3(i,j) = l_limit(j);
            end
            if IntegerVariables(j) == 1
                child_3(i,j)=round(child_3(i,j));
            end
        end
    end
    
    
    
    %!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
%!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
%!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
%!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
%PARFOR NOT WORKING BECAUSE OF IF ELSE in eval fnc
    for i=1:mutCount
        % Evaluate the objective function for the offspring and as before
        % concatenate the offspring chromosome with objective value.
        [out1(i),out2(i)]=evalFnc(child_3(i,:), evalGen, childCounter, get_input); %!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        childCounter = childCounter+1;
    end
    
    
    
    for i=1:mutCount
        child_3(i,V + 1: M + V) = [out1(i),out2(i)];
        % Set the mutation flag
        
        parchild1(i+crossCount,:)=child_3(i,1:M+V);
        %    parchild2(i)=0;
        
    end
end








p=1;
for i=1:crossCount
    
    child(p,:)=parchild1(i,:);
    child(p+1,:)=parchild2(i,:);
    p=p+2;
    
end

if mutCount >0
    for i=crossCount:mutCount
        child(p,:)=parchild1(i,:);
        p=p+1;
    end
end



% 
% 
% parfor i = 1 : N
%     % With 90 % probability perform crossover
% 
%     if rand(1) < 0.9
%         % Initialize the children to be null vector.
%         child_1 = [];
%         child_2 = [];
%         % Select the first parent
%         parent_1 = round(N*rand(1));
%         if parent_1 < 1
%             parent_1 = 1;
%         end
%         % Select the second parent
%         parent_2 = round(N*rand(1));
%         if parent_2 < 1
%             parent_2 = 1;
%         end
%         % Make sure both the parents are not the same. 
%         while isequal(parent_chromosome(parent_1,:),parent_chromosome(parent_2,:))
%             parent_2 = round(N*rand(1));
%             if parent_2 < 1
%                 parent_2 = 1;
%             end
%         end
%         % Get the chromosome information for each randomnly selected
%         % parents
%         parent_1 = parent_chromosome(parent_1,:);
%         parent_2 = parent_chromosome(parent_2,:);
%         % Perform corssover for each decision variable in the chromosome.
%         for j = 1 : V
%             % SBX (Simulated Binary Crossover).
%             % For more information about SBX refer the enclosed pdf file.
%             % Generate a random number
%             u(i,j) = rand(1);
%             if u(i,j) <= 0.5
%                 bq(i,j) = (2*u(i,j))^(1/(mu+1));
%             else
%                 bq(i,j) = (1/(2*(1 - u(i,j))))^(1/(mu+1));
%             end
%             % Generate the jth element of first child
%             child_1(j) = ...
%                 0.5*(((1 + bq(i,j))*parent_1(j)) + (1 - bq(i,j))*parent_2(j));
%             % Generate the jth element of second child
%             child_2(j) = ...
%                 0.5*(((1 - bq(i,j))*parent_1(j)) + (1 + bq(i,j))*parent_2(j));
%             % Make sure that the generated element is within the specified
%             % decision space else set it to the appropriate extrema.
%             if child_1(j) > u_limit(j)
%                 child_1(j) = u_limit(j);
%             elseif child_1(j) < l_limit(j)
%                 child_1(j) = l_limit(j);
%             end
%             if child_2(j) > u_limit(j)
%                 child_2(j) = u_limit(j);
%             elseif child_2(j) < l_limit(j)
%                 child_2(j) = l_limit(j);
%             end
%         end
%         % Evaluate the objective function for the offsprings and as before
%         % concatenate the offspring chromosome with objective value.
%         [out1,out2]=evalFnc(child_1, M,V);%Horizon, profiles); %!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
%         child_1(:,V + 1: M + V) = [out1,out2];
%         
%         [out1,out2]= evalFnc(child_2, M,V);%Horizon, profiles); %!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
%         child_2(:,V + 1: M + V) = [out1,out2];
%         % Set the crossover flag. When crossover is performed two children
%         % are generate, while when mutation is performed only only child is
%         % generated.
%         was_crossover = 1;
%         was_mutation = 0;
%         
%         parmut(i)=0;
%         parcross(i)=1;
%         parchild1(i)=child_1;
%         parchild2(i)=child_2;
%         
% 
% 
%     % With 10 % probability perform mutation. Mutation is based on
%     % polynomial mutation. 
%     else
%         % Select at random the parent.
%         parent_3 = round(N*rand(1));
%         if parent_3 < 1
%             parent_3 = 1;
%         end
%         % Get the chromosome information for the randomnly selected parent.
%         child_3 = parent_chromosome(parent_3,:);
%         % Perform mutation on eact element of the selected parent.
%         for j = 1 : V
%            r(i,j) = rand(1);
%            if r(i,j) < 0.5
%                delta(i,j) = (2*r(i,j))^(1/(mum+1)) - 1;
%            else
%                delta(i,j) = 1 - (2*(1 - r(i,j)))^(1/(mum+1));
%            end
%            % Generate the corresponding child element.
%            child_3(j) = child_3(j) + delta(i,j);
%            % Make sure that the generated element is within the decision
%            % space.
%            if child_3(j) > u_limit(j)
%                child_3(j) = u_limit(j);
%            elseif child_3(j) < l_limit(j)
%                child_3(j) = l_limit(j);
%            end
%         end
%         % Evaluate the objective function for the offspring and as before
%         % concatenate the offspring chromosome with objective value.
%         [out1,out2]=evalFnc(child_3,  M,V);%Horizon, profiles); %!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
%         child_3(:,V + 1: M + V) = [out1,out2];
%         % Set the mutation flag
%         was_mutation = 1;
%         was_crossover = 0;
%         
%         parmut(i)=1;
%         parcross(i)=0;
%        parchild1(i)=child_3;
%        parchild2(i)=0;
%     end
%     % Keep proper count and appropriately fill the child variable with all
%     % the generated children for the particular generation.
% %     if was_crossover
% %         child(p,:) = child_1;
% %         child(p+1,:) = child_2;
% %         was_cossover = 0;
% %         p = p + 2;
% %     elseif was_mutation
% %         child(p,:) = child_3(1,1 : M + V);
% %         was_mutation = 0;
% %         p = p + 1;
% %     end
% end
% 
% for i=1:N
%    if parcross(i)
%        child(p,:)=parchild1(i);
%        child(p+1,:)=parchild2(i);
%        p=p+2;
%    elseif parmut(i)
%        child(p,:)=parchild1(i);
%        p=p+1;
%    end
% end


f = child;
