function BRCM_newcsv(outpath, lines, strToFind, replaceString, replacePosition)

outfile=outpath;


newlines = lines;
for k=1:size(strToFind,2)
    
    for i=1:size(lines,2)
        finder=strfind(lines{i},strToFind{k});
        if isempty(finder)==false
            str = strsplit(lines{i},';');
            for u=1:(size(str,2)-1)
                if u==1
                    if u ~= replacePosition(k) && replacePosition(k) ~= 999     %999 means replacing entire line
                        newstr=strcat(str(1),';');
                    else
                        newstr=strcat(replaceString{k},';');
                    end
                else
                    if u ~= replacePosition(k) && replacePosition(k) ~= 999     %999 means replacing entire line
                        newstr=strcat(newstr,str(u),';');
                    else
                        newstr=strcat(newstr,replaceString{k},';');
                    end
                end
            end
            if replacePosition(k) == 999
               newstr={newstr}; 
            end

            newlines(i) = newstr;
        end
        
    end
    
    
end

fid = fopen(outfile, 'w');
for i=1:length(lines)
    fprintf(fid, '%s \r\n',newlines{i});
end
fclose(fid);
fclose('all');

end