function dblArea = BRCM_getPolygonArea(Pts, XYZPlane)
% input: 
% Pts = {{x1,y1,z1},{x2,y2,z3},...,{xn,yn,zn}};
% XYZPlane = 'X' || 'Y' || 'Z' ....... indicates, on which plane

% returns area of polygon: (doesn't consider self-intersection. points have to be in order, clock-wise or counter-clock-wise
% dblArea = ((x1y2-y1x2)+(x2y3-y2x3),...,+(xny1-ynx1)) / 2


switch XYZPlane
    case 'X'
        X=2;
        Y=3;
    case 'Y'
        X=1;
        Y=3;
    case 'Z'
        X=1;
        Y=2;
end

dblArea = 0;

for i=1:size(Pts,2)
    if i ~= size(Pts,2)
        dblArea = dblArea + (Pts{i}{X}*Pts{i+1}{Y} - Pts{i}{Y}*Pts{i+1}{X});
    else
        dblArea = dblArea + (Pts{i}{X}*Pts{1}{Y} - Pts{i}{Y}*Pts{1}{X});
    end
    
end

dblArea = abs(dblArea) / 2;


end