function BRCM_newcsv_Storeys(outpath, lines, strToFind, replaceString, replacePosition, strToFindSub)

outfile=outpath;


newlines = lines;
% for k=1:size(strToFind,2)
%     if k==1
%         for i=1:size(lines,2)
%             finder = strfind(lines{i}, strToFind{k});
%             if isempty(finder)==false
%                 str = strsplit(lines{i},';');
%                 finder2 = strfind(lines{i},strToFindSub{1});
%                 if isempty(finder2)==false %finding the ceiling, which becomes a roof   ... e.g.:     {EP Surface Names:Block9:Zone1_Ceiling_1_0_0,Block10:Zone1_Floor_0_0_0}
%                     str2 = strsplit(str{2},strcat(',',strToFind{1}));                                % {EP Surface Names:Block9:Zone1_Ceiling_1_0_0} {:Zone1_Floor_0_0_0}
%                     str3 = strsplit(str2{1},'Ceiling');                                              % {EP Surface Names:Block9:Zone1_} {_1_0_0}
%                     str4 = strcat(str3{1},'Roof',str3{2});                                           % {EP Surface Names:Block9:Zone1_Roof_1_0_0}
%                     for u=3:(size(str,2)-1)
%                         if u==3
%                             newstr = strcat(str(1), ';', str4, ';', str(3), ';');
%                         else
%                             newstr = strcat(newstr, str(u), ';');
%                         end
%                     end
%                 else
%                     for u=1:(size(str,2)-1)
%                         if u==1
%                             if u ~= replacePosition(k) && replacePosition(k) ~= 999     %999 means replacing entire line
%                                 newstr=strcat(str(1),';');
%                             else
%                                 newstr=strcat(replaceString{k},';');
%                             end
%                         else
%                             if u ~= replacePosition(k) && replacePosition(k) ~= 999     %999 means replacing entire line
%                                 newstr=strcat(newstr,str(u),';');
%                             else
%                                 newstr=strcat(newstr,replaceString{k},';');
%                             end
%                         end
%                     end
%                     if replacePosition(k) == 999
%                         newstr={newstr};
%                     end
%                 end
%             end
%         end
%     else
%         for i=1:size(lines,2)
%             finder=strfind(lines{i},strToFind{k});
%             if isempty(finder)==false
%                 str = strsplit(lines{i},';');
%                 for u=1:(size(str,2)-1)
%                     if u==1
%                         if u ~= replacePosition(k) && replacePosition(k) ~= 999     %999 means replacing entire line
%                             newstr=strcat(str(1),';');
%                         else
%                             newstr=strcat(replaceString{k},';');
%                         end
%                     else
%                         if u ~= replacePosition(k) && replacePosition(k) ~= 999     %999 means replacing entire line
%                             newstr=strcat(newstr,str(u),';');
%                         else
%                             newstr=strcat(newstr,replaceString{k},';');
%                         end
%                     end
%                 end
%                 if replacePosition(k) == 999
%                     newstr={newstr};
%                 end
%             end
%         end
%     end
%     newlines(i) = newstr;
%     
% end








for i=1:size(lines,2)
    for k=1:size(strToFind,2)
        if k==1
            finder = strfind(lines{i}, strToFind{k});
            if isempty(finder)==false
                str = strsplit(lines{i},';');
                finder2 = strfind(lines{i},strToFindSub{1});
                if isempty(finder2)==false %finding the ceiling, which becomes a roof   ... e.g.:     {EP Surface Names:Block9:Zone1_Ceiling_1_0_0,Block10:Zone1_Floor_0_0_0}
                    str2 = strsplit(str{2},strcat(',',strToFind{1}));                                % {EP Surface Names:Block9:Zone1_Ceiling_1_0_0} {:Zone1_Floor_0_0_0}
                    str3 = strsplit(str2{1},'Ceiling');                                              % {EP Surface Names:Block9:Zone1_} {_1_0_0}
                    str4 = strcat(str3{1},'Roof',str3{2});                                           % {EP Surface Names:Block9:Zone1_Roof_1_0_0}
                    for u=4:(size(str,2)-1)
                        if u==4
                            newstr = strcat(str(1), ';', str4, ';', str(3), ';', 'AMB;');
                        else
                            newstr = strcat(newstr, str(u), ';');
                        end
                    end
                else
                    for u=1:(size(str,2)-1)
                        if u==1
                            if u ~= replacePosition(k) && replacePosition(k) ~= 999     %999 means replacing entire line
                                newstr=strcat(str(1),';');
                            else
                                newstr=strcat(replaceString{k},';');
                            end
                        else
                            if u ~= replacePosition(k) && replacePosition(k) ~= 999     %999 means replacing entire line
                                newstr=strcat(newstr,str(u),';');
                            else
                                newstr=strcat(newstr,replaceString{k},';');
                            end
                        end
                    end
                    if replacePosition(k) == 999
                        newstr={newstr};
                    end
                end
                newlines(i) = newstr;
            end
        else
            finder=strfind(lines{i},strToFind{k});
            if isempty(finder)==false
                str = strsplit(lines{i},';');
                for u=1:(size(str,2)-1)
                    if u==1
                        if u ~= replacePosition(k) && replacePosition(k) ~= 999     %999 means replacing entire line
                            newstr=strcat(str(1),';');
                        else
                            newstr=strcat(replaceString{k},';');
                        end
                    else
                        if u ~= replacePosition(k) && replacePosition(k) ~= 999     %999 means replacing entire line
                            newstr=strcat(newstr,str(u),';');
                        else
                            newstr=strcat(newstr,replaceString{k},';');
                        end
                    end
                end
                if replacePosition(k) == 999
                    newstr={newstr};
                end
                newlines(i) = newstr;
            end
        end
    end

end






















fid = fopen(outfile, 'w');
for i=1:length(lines)
    fprintf(fid, '%s \r\n',newlines{i});
end
fclose(fid);
fclose('all');

end