function [ENERGY, CAPITAL] = BRCM_run(var, generation, job, get_input)
%% function [ENERGY, CAPITAL] = SeqAss_BRCM_run(var, generation, job, get_input)
%
% BRCM-model: model modification and simulation
% var = [0.4  	0.8   	0.5     0       -0.8       5     5       0.51     0.51     0.01     0.01     1       1       1       3       0];




tic
%% variables and inputs -------------------------------------------------------------------------------------------
% -----------------------------------------------------------------------------------------------------------------
orientation = var(4);
disturbances=get_input.disturbances;
BRCMfilepath=get_input.BRCMfilepath;




%% modify building      -------------------------------------------------------------------------------------------
% -----------------------------------------------------------------------------------------------------------------
[RotateBldn, ThermalModelType, EHFModelType] = BRCM_build(var, get_input);








%% Load Building Data   -------------------------------------------------------------------------------------------
% -----------------------------------------------------------------------------------------------------------------
BRCMBuilding = 'BRCMBuilding';
thermalModelDataDir =   [BRCMfilepath,filesep,ThermalModelType];
EHFModelDataDir =       [BRCMfilepath,filesep,EHFModelType];

buildingIdentifier = BRCMBuilding;
B = Building(buildingIdentifier);
B.loadThermalModelData(thermalModelDataDir);
%reference disturbances
EHFModelClassFile = 'BuildingHull.m';
EHFModelDataFile = [EHFModelDataDir,filesep,'buildinghull'];
EHFModelIdentifier = 'BuildingHull';
B.declareEHFModel(EHFModelClassFile,EHFModelDataFile,EHFModelIdentifier);
% InternalGains
EHFModelClassFile = 'InternalGains.m';
EHFModelDataFile = [EHFModelDataDir,filesep,'internalgains'];
EHFModelIdentifier = 'IG';
B.declareEHFModel(EHFModelClassFile,EHFModelDataFile,EHFModelIdentifier);
% Radiators
EHFModelClassFile = 'Radiators.m';
EHFModelDataFile = [EHFModelDataDir,filesep,'radiators'];
EHFModelIdentifier = 'Rad';
B.declareEHFModel(EHFModelClassFile,EHFModelDataFile,EHFModelIdentifier);



% Print the thermal model data in the Command Window for an overview
% B.printThermalModelData;

% 3-D plot of Building
%B.drawBuilding;

% It is possible to control the labeling
% B.drawBuilding('NoBELabels');
% B.drawBuilding('NoLabels');
% B.drawBuilding('NoZoneLabels');

% % 2-D plot of Building
% B.drawBuilding('Floorplan');
%
% % Drawing parts of the building can be done by a cell array of zone group and/or zone identifiers
% B.drawBuilding({'ZoneGrp_North'});
% B.drawBuilding({'Z0002','Z0003'});







%% Generate thermal model and full model --------------------------------------------------------------------------
% -----------------------------------------------------------------------------------------------------------------

% Generate thermal model (optional)
%B1.generateThermalModel;


%!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
% Generate (full) building model (includes thermal model generation if not yet done)
B.generateBuildingModel;

% Display all available identifiers (these are the names of the control inputs / disturbances / states in the same order as they appear in the matrices)
% B.building_model.printIdentifiers;

% Disretization
Ts_hrs = 1;
B.building_model.setDiscretizationStep(Ts_hrs);
B.building_model.discretize();








%% Discrete-time simulation of building model   -------------------------------------------------------------------
% -----------------------------------------------------------------------------------------------------------------

% The class SimulationExperiment provides a simulation environment. An instantiation requires
% a Building object that at least containts a thermal model and a the sampling time (this will be
% also the simulation timestep). Once the SimulationExperiment object is instantiated, its building
% object in  object cannot be manipulated anymore.

SimExp = SimulationExperiment(B);




% It is possible to print/access the identifiers and to access the model/simulation time step
% SimExp.printIdentifiers();
identifiers = SimExp.getIdentifiers();
len_v = length(identifiers.v);
len_u = length(identifiers.u);
len_x = length(identifiers.x);
Ts_hrs = SimExp.getSamplingTime();



% C matrix
% necessary for matrix multiplication. Tells, which state to consider (conveniently only the zone-volumes, not the walls)
C=zeros(len_u,len_x);   % for every radiator (or zone)
for i=1:len_u
    C(i,i)=1;
end




% Number of simulation time steps and initial condition must be set before the simulation
n_timeSteps = 8760;
SimExp.setNumberOfSimulationTimeSteps(n_timeSteps);
x0 = 22*ones(len_x,1);
SimExp.setInitialState(x0);



% set up input and disturbance sequence
V = zeros(len_v,n_timeSteps);           % disturbances
U = zeros(len_u,n_timeSteps);           % radiator

idx_v_Tamb = getIdIndex('v_Tamb',identifiers.v);
idx_v_solGlobFac_S = getIdIndex('v_solGlobFac_S',identifiers.v);
idx_v_solGlobFac_N = getIdIndex('v_solGlobFac_N',identifiers.v);
idx_v_solGlobFac_E = getIdIndex('v_solGlobFac_E',identifiers.v);
idx_v_solGlobFac_W = getIdIndex('v_solGlobFac_W',identifiers.v);
idx_v_solGlobFac_Roof = getIdIndex('v_solGlobFac_Roof',identifiers.v);
idx_v_solGlobFac_NBlinds = getIdIndex('v_solGlobFac_NBlinds',identifiers.v); %assuming no blinds
idx_v_solGlobFac_SBlinds = getIdIndex('v_solGlobFac_SBlinds',identifiers.v); %assuming no blinds
idx_v_solGlobFac_WBlinds = getIdIndex('v_solGlobFac_WBlinds',identifiers.v); %assuming no blinds
idx_v_solGlobFac_EBlinds = getIdIndex('v_solGlobFac_EBlinds',identifiers.v); %assuming no blinds
idx_v_IG_Offices = getIdIndex('v_IG_Offices', identifiers.v);



V(idx_v_Tamb,:) = disturbances.data(1:8760,1); % ambient temperature
V(idx_v_solGlobFac_Roof,:) = disturbances.data(1:8760,6); % solar radiation on roof (pre-calculated, e.g. from Energyplus)


% rotate or flip building, according to control point x/y-location (var 1 & 2)
if RotateBldn==0
    solarN=disturbances.data(1:8760,8);
    solarS=disturbances.data(1:8760,4);
    solarW=disturbances.data(1:8760,5);
    solarE=disturbances.data(1:8760,7);
elseif RotateBldn == 90
    solarN=disturbances.data(1:8760,7);
    solarS=disturbances.data(1:8760,5);
    solarW=disturbances.data(1:8760,8);
    solarE=disturbances.data(1:8760,4);
elseif RotateBldn == -90
    solarN=disturbances.data(1:8760,5);
    solarS=disturbances.data(1:8760,7);
    solarW=disturbances.data(1:8760,4);
    solarE=disturbances.data(1:8760,8);
elseif RotateBldn == 180
    solarN=disturbances.data(1:8760,4);
    solarS=disturbances.data(1:8760,8);
    solarW=disturbances.data(1:8760,7);
    solarE=disturbances.data(1:8760,5);
elseif RotateBldn == 9999       % flip east west
    solarN=disturbances.data(1:8760,8);
    solarS=disturbances.data(1:8760,4);
    solarW=disturbances.data(1:8760,7);
    solarE=disturbances.data(1:8760,5);
elseif RotateBldn == 8888       % flip north suoth
    solarN=disturbances.data(1:8760,4);
    solarS=disturbances.data(1:8760,8);
    solarW=disturbances.data(1:8760,5);
    solarE=disturbances.data(1:8760,7);
end


%interpolate solar data (W/sqm) on facades, to account for rotation(orientation) of building
rotfac = (1/90)* orientation;
if rotfac > 0
    V(idx_v_solGlobFac_S,:) = (1-rotfac) * solarS + rotfac * solarW;
    V(idx_v_solGlobFac_N,:) = (1-rotfac) * solarN + rotfac * solarE;
    V(idx_v_solGlobFac_W,:) = (1-rotfac) * solarW + rotfac * solarN;
    V(idx_v_solGlobFac_E,:) = (1-rotfac) * solarE + rotfac * solarS;
elseif rotfac < 0
    V(idx_v_solGlobFac_S,:) = (1-abs(rotfac)) * solarS + abs(rotfac) * solarE;
    V(idx_v_solGlobFac_N,:) = (1-abs(rotfac)) * solarN + abs(rotfac) * solarW;
    V(idx_v_solGlobFac_W,:) = (1-abs(rotfac)) * solarW + abs(rotfac) * solarS;
    V(idx_v_solGlobFac_E,:) = (1-abs(rotfac)) * solarE + abs(rotfac) * solarN;
else
    V(idx_v_solGlobFac_S,:) = solarS;
    V(idx_v_solGlobFac_N,:) = solarN;
    V(idx_v_solGlobFac_W,:) = solarW;
    V(idx_v_solGlobFac_E,:) = solarE;
end


V(idx_v_solGlobFac_NBlinds,:) = V(idx_v_solGlobFac_N,:);
V(idx_v_solGlobFac_SBlinds,:) = V(idx_v_solGlobFac_S,:);
V(idx_v_solGlobFac_WBlinds,:) = V(idx_v_solGlobFac_W,:);
V(idx_v_solGlobFac_EBlinds,:) = V(idx_v_solGlobFac_E,:);
V(idx_v_IG_Offices,:) = disturbances.data(1:8760,9);



r=zeros(len_u,n_timeSteps);  % reference temperature => set point
rLB = disturbances.data(1:8760,12); % lower bound (setpoint and setback has to be defined in spreadsheet for every t)
rUB = disturbances.data(1:8760,13); % upper bound
htgclg = zeros(len_u,1);    % identify, whether a room needs heating, cooling or nothing
htgclg(:,1) = 3; % 1=heating necessary, 2=cooling, 3=nothing

Uopt=zeros(len_u,n_timeSteps);
X=zeros(len_x,n_timeSteps);
X(:,1)=20;                      % Initial state (room temperatures)



%% natural ventilation stuff
% Vwindspeed=disturbances.data(1:8760,10);
% Vwinddirection=disturbances.data(1:8760,11);
% Xtest=zeros(len_x,1);
% Utest=zeros(len_u,1);
% Qvent=zeros(len_u,n_timeSteps);




%% Model discretisation
A=B.building_model.discrete_time_model.A;
Bu=B.building_model.discrete_time_model.Bu;
Bv=B.building_model.discrete_time_model.Bv;



%% Simulation
for t=1:n_timeSteps-1
%     t
    %% limit solar radiation on windows to 200W/m2. assume external louvers
    if (V(idx_v_solGlobFac_NBlinds,t)>150)
        V(idx_v_solGlobFac_NBlinds,t)=150;
    end
    if (V(idx_v_solGlobFac_SBlinds,t)>150)
        V(idx_v_solGlobFac_SBlinds,t)=150;
    end
    if (V(idx_v_solGlobFac_EBlinds,t)>150)
        V(idx_v_solGlobFac_EBlinds,t)=150;
    end
    if (V(idx_v_solGlobFac_WBlinds,t)>150)
        V(idx_v_solGlobFac_WBlinds,t)=150;
    end
    
    
    %% natural ventilation
% %     test inside temperature without heating/cooling
%         Xtest(:)=A*X(:,t)+Bu*Utest+Bv*V(:,t);
%         Qvent(:,t)=BRCM_getNaturalVentilation(Xtest,V(idx_v_Tamb,t), Vwindspeed(t), Vwinddirection(t), B, 1, 2.5);
    
    
    %% setpoint cooling or heating
    for i=1:len_u
        if X(i,t) < rLB(t)
            r(i,t+1) = rLB(t);
            htgclg(i)=1; %'heating';
        elseif X(i,t) > rUB(t)
            r(i,t+1) = rUB(t);
            htgclg(i)=1; % 'cooling';
        else
            r(i,t+1) = X(i,t);
            htgclg(i)=0; % 'nothing';
        end
    end
    
    
    %% solving for optimal radiator input (cooling or heating)
    Uopt = ((C*Bu)^-1)*(r(:,t+1)-C*A*X(:,t)-C*Bv*V(:,t));
    U(:,t) = Uopt .* htgclg;
    X(:,t+1)=A*X(:,t)+Bu*U(:,t)+Bv*V(:,t);
    
end







%% Cost calculation -----------------------------------------------------------------------------------------------
% -----------------------------------------------------------------------------------------------------------------

%1st obejctive ENERGY in [kWh/a]
heatingdemand = (U>0).*U;
heatingdemand=(sum(sum(heatingdemand,2)) / len_u) / 1000;       % in: [kWh / sqm a]
coolingdemand = (U<0).*U;
coolingdemand=(sum(sum(coolingdemand,2)) / len_u) / 1000;       % in: [kWh / sqm a]
ENERGY = (heatingdemand + coolingdemand*-1);                    % in: [kWh / sqm a]


%2nd obejctive CAPITAL in [CHF/a]
CAPITAL = BRCM_capital(B, ThermalModelType, var);







%%
%
% % Plot the simulation results. Every call to SimExp.plot generates a separate figure
% % The command takes a cell array of cells. Every cell produces a subplot for every
% % identifier contained in it.
%
% firstFigure{1} = identifiers.x(1:3);         % any state/input/disturbance identifier can be used here
% % firstFigure{2} = {'Z0001','Z0002'};          % zone identifiers work too
% % firstFigure{3} = {'ZoneGrp_East','Z0003'};   % zone group identifiers work too
% fh1 = SimExp.plot(firstFigure);
%
% secondFigure{1} = identifiers.u;
% secondFigure{2} = identifiers.v;
% fh2 = SimExp.plot(secondFigure);

toc


end

