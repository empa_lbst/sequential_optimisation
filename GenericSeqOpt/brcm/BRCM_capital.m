function CAPITAL = BRCM_capital(B, ThermalModelType, var)
% Variables:
% var(1)    -   Control Point X [-1,1]      changing typology and floorplan size
% var(2)    -   Control Point Y [-1,1]      changing typology and floorplan size
% var(3)    -   Control Point Z [0,2]       changing typology and floorplan size
% var(4)    -   Orientation     [-30,30]    orientation of the building, in degree deviation from North (0�)
% var(5)    -   Length/Width    [-1,1]      Length/Width Ratio. stretching east/west (1) or north/south (-1)
% var(6)    -   Ceiling Height  [3,5]       Ceiling height per floor, in meters
% var(7)    -   No. of storeys  [1,10]      Number of storeys.
% var(8)    -   Glazing S       [0.01,0.9]  Glazing Ratio on South Facades
% var(9)    -   Glazing E       [0.01,0.9]  Glazing Ratio on East Facades
% var(10)   -   Glazing N       [0.01,0.9]  Glazing Ratio on North Facades
% var(11)   -   Glazing W       [0.01,0.9]  Glazing Ratio on West Facades
% var(12)   -   Walls Constr.   [1,2]       Construction type for Walls (light heavy)
% var(13)   -   Floors Constr.  [1,2]       Construction type for Floors (light heavy)
% var(14)   -   Roof Constr.    [1,2]       Construction type for Roof (light heavy)
% var(15)   -   Glazing Type    [1,3]       Glazing Type windows (WSG 2, WSG 3, SSG 2)
% var(16)   -   Insulation      [50,300]    Insulation thickness for walls in mm. Same insulation material always





costGround = [300 350];        % per sqm 
costWall = [200 250];        % per sqm
costFloor = [200 250];       % per sqm
costRoof = [450 500];        % per sqm
costWin = [500 800 1000];            % per sqm
costInsul = 1.3;       % per mm sqm


TotCostGround = 0;
TotCostFloor = 0;
TotCostWall = 0;
TotCostRoof = 0;
TotCostInsul = 0;



switch ThermalModelType
    case 'ThermalModel1'
         index = 2;
        indexWin = 1;
        winArea1 =  str2double(B.thermal_model_data.windows(1, indexWin).glass_area) * var(7);
        winArea2 =  str2double(B.thermal_model_data.windows(1, indexWin+1).glass_area) * var(7);
        winArea3 =  str2double(B.thermal_model_data.windows(1,indexWin+2).glass_area) * var(7);
        winArea4 =  str2double(B.thermal_model_data.windows(1,indexWin+3).glass_area) * var(7);
      
        wallArea1=(str2double(B.thermal_model_data.building_elements(1,index+1).area) * var(7)) - winArea1 ;
        wallArea2=(str2double(B.thermal_model_data.building_elements(1,index+2).area ) * var(7)) - winArea2;
        wallArea3=(str2double(B.thermal_model_data.building_elements(1,index+3).area ) * var(7)) - winArea3;
        wallArea4=(str2double(B.thermal_model_data.building_elements(1,index+4).area ) * var(7)) - winArea4;
    
        TotCostGround = str2double(B.thermal_model_data.building_elements(1,1).area) * costGround(var(13));
       
        TotalFloorArea = (str2double(B.thermal_model_data.building_elements(1,index).area)) * var(7);
        TotCostFloor = (TotalFloorArea * costFloor(var(13)))  - ((TotalFloorArea/var(7)) * costFloor(var(13)));
                
        TotCostRoof = (str2double(B.thermal_model_data.building_elements(1,index).area) * costRoof(var(14)));
        
        TotCostInsul = (wallArea1+wallArea2+wallArea3+wallArea4) * var(16) * costInsul;
        TotCostWall = (wallArea1+wallArea2+wallArea3+wallArea4) * costWall(var(12));
        TotCostWin = (winArea1+winArea2+winArea3+winArea4)  * costWin(var(15));
        
        
        
    case 'ThermalModel2'
         index = 7;
        indexWin = 1;
        winArea1 =  str2double(B.thermal_model_data.windows(1, indexWin).glass_area) * var(7);
        winArea2 =  str2double(B.thermal_model_data.windows(1, indexWin+1).glass_area) * var(7);
        winArea3 =  str2double(B.thermal_model_data.windows(1,indexWin+2).glass_area) * var(7);
        winArea4 =  str2double(B.thermal_model_data.windows(1,indexWin+3).glass_area) * var(7);
        winArea5 =  str2double(B.thermal_model_data.windows(1,indexWin+4).glass_area) * var(7);
        winArea6 = str2double( B.thermal_model_data.windows(1,indexWin+5).glass_area) * var(7);
        winArea7 = str2double( B.thermal_model_data.windows(1,indexWin+6).glass_area) * var(7);
        winArea8 = str2double (B.thermal_model_data.windows(1,indexWin+7).glass_area) * var(7);
        wallArea1=(str2double(B.thermal_model_data.building_elements(1,index+6).area) * var(7)) - winArea1 ;
        wallArea2=(str2double(B.thermal_model_data.building_elements(1,index+7).area ) * var(7)) - winArea2;
        wallArea3=(str2double(B.thermal_model_data.building_elements(1,index+8).area ) * var(7)) - winArea3;
        wallArea4=(str2double(B.thermal_model_data.building_elements(1,index+9).area ) * var(7)) - winArea4;
        wallArea5=(str2double(B.thermal_model_data.building_elements(1,index+10).area  ) * var(7))- winArea5;
        wallArea6=(str2double(B.thermal_model_data.building_elements(1,index+11).area  ) * var(7))- winArea6;
        wallArea7=(str2double(B.thermal_model_data.building_elements(1,index+12).area  ) * var(7))  - winArea7;
        wallArea8=(str2double( B.thermal_model_data.building_elements(1,index+13).area)   * var(7))- winArea8;
        
        TotCostGround = str2double(B.thermal_model_data.building_elements(1,1).area) * costGround(var(13)) ...
            + str2double(B.thermal_model_data.building_elements(1,2).area) * costGround(var(13)) ...
            + str2double(B.thermal_model_data.building_elements(1,3).area) * costGround(var(13)) ...
            + str2double(B.thermal_model_data.building_elements(1,4).area) * costGround(var(13)) ...
            + str2double(B.thermal_model_data.building_elements(1,5).area) * costGround(var(13)) ...
            + str2double(B.thermal_model_data.building_elements(1,6).area) * costGround(var(13));
                
        TotalFloorArea =  ((str2double(B.thermal_model_data.building_elements(1,index).area) ...
            + str2double(B.thermal_model_data.building_elements(1,index+1).area) + ...
            + str2double(B.thermal_model_data.building_elements(1,index+2).area) + ...
            str2double(B.thermal_model_data.building_elements(1,index+3).area) + ...
            str2double(B.thermal_model_data.building_elements(1,index+4).area) + ...
            str2double(B.thermal_model_data.building_elements(1,index+5).area))) * var(7);
        
        TotCostFloor = (TotalFloorArea * costFloor(var(13))) - ((TotalFloorArea/var(7)) * costFloor(var(13)));
        
        TotCostRoof = (str2double(B.thermal_model_data.building_elements(1,index).area) ...
            + str2double(B.thermal_model_data.building_elements(1,index+1).area) + ...
            + str2double(B.thermal_model_data.building_elements(1,index+2).area) + ...
            str2double(B.thermal_model_data.building_elements(1,index+3).area) + ...
            str2double(B.thermal_model_data.building_elements(1,index+4).area) + ...
            str2double(B.thermal_model_data.building_elements(1,index+5).area)) ...
            * costRoof(var(14));
        
        TotCostInsul = (wallArea1+wallArea2+wallArea3+wallArea4+wallArea5+wallArea6+wallArea7+wallArea8) * var(16) * costInsul;
        TotCostWall = (wallArea1+wallArea2+wallArea3+wallArea4+wallArea5+wallArea6+wallArea7+wallArea8) * costWall(var(12));
        TotCostWin = (winArea1+winArea2+winArea3+winArea4+winArea5+winArea6+winArea7+winArea8)  * costWin(var(15));
        
        
    case 'ThermalModel3'
        index = 4;
        indexWin = 1;
        winArea1 =  str2double(B.thermal_model_data.windows(1, indexWin).glass_area) * var(7);
        winArea2 =  str2double(B.thermal_model_data.windows(1, indexWin+1).glass_area) * var(7);
        winArea3 =  str2double(B.thermal_model_data.windows(1,indexWin+2).glass_area) * var(7);
        winArea4 =  str2double(B.thermal_model_data.windows(1,indexWin+3).glass_area) * var(7);
        winArea5 =  str2double(B.thermal_model_data.windows(1,indexWin+4).glass_area) * var(7);
        winArea6 = str2double( B.thermal_model_data.windows(1,indexWin+5).glass_area) * var(7);
        winArea7 = str2double( B.thermal_model_data.windows(1,indexWin+6).glass_area) * var(7);
        winArea8 = str2double (B.thermal_model_data.windows(1,indexWin+7).glass_area) * var(7);
        wallArea1=(str2double(B.thermal_model_data.building_elements(1,index+3).area) * var(7)) - winArea1 ;
        wallArea2=(str2double(B.thermal_model_data.building_elements(1,index+4).area ) * var(7)) - winArea2;
        wallArea3=(str2double(B.thermal_model_data.building_elements(1,index+5).area ) * var(7)) - winArea3;
        wallArea4=(str2double(B.thermal_model_data.building_elements(1,index+6).area ) * var(7)) - winArea4;
        wallArea5=(str2double(B.thermal_model_data.building_elements(1,index+7).area  ) * var(7))- winArea5;
        wallArea6=(str2double(B.thermal_model_data.building_elements(1,index+8).area  ) * var(7))- winArea6;
        wallArea7=(str2double(B.thermal_model_data.building_elements(1,index+9).area  ) * var(7))  - winArea7;
        wallArea8=(str2double( B.thermal_model_data.building_elements(1,index+10).area)   * var(7))- winArea8;
        
        TotCostGround = str2double(B.thermal_model_data.building_elements(1,1).area) * costGround(var(13)) ...
            + str2double(B.thermal_model_data.building_elements(1,2).area) * costGround(var(13)) ...
            + str2double(B.thermal_model_data.building_elements(1,3).area) * costGround(var(13));
        
        TotalFloorArea = (str2double(B.thermal_model_data.building_elements(1,index).area) ...
            + str2double(B.thermal_model_data.building_elements(1,index+1).area) + ...
            + str2double(B.thermal_model_data.building_elements(1,index+2).area)) * var(7);
        
        TotCostFloor =  (TotalFloorArea * costFloor(var(13))) - ((TotalFloorArea/var(7)) * costFloor(var(13)));
        
        TotCostRoof = (str2double(B.thermal_model_data.building_elements(1,index).area) ...
            + str2double(B.thermal_model_data.building_elements(1,index+1).area) + ...
            + str2double(B.thermal_model_data.building_elements(1,index+2).area)) ...
            * costRoof(var(14));
        
        TotCostInsul = (wallArea1+wallArea2+wallArea3+wallArea4+wallArea5+wallArea6+wallArea7+wallArea8) * var(16) * costInsul;
        TotCostWall = (wallArea1+wallArea2+wallArea3+wallArea4+wallArea5+wallArea6+wallArea7+wallArea8) * costWall(var(12));
        TotCostWin = (winArea1+winArea2+winArea3+winArea4+winArea5+winArea6+winArea7+winArea8)  * costWin(var(15));
        
    case 'ThermalModel4'
         index = 3;
        indexWin = 1;
        winArea1 =  str2double(B.thermal_model_data.windows(1, indexWin).glass_area) * var(7);
        winArea2 =  str2double(B.thermal_model_data.windows(1, indexWin+1).glass_area) * var(7);
        winArea3 =  str2double(B.thermal_model_data.windows(1,indexWin+2).glass_area) * var(7);
        winArea4 =  str2double(B.thermal_model_data.windows(1,indexWin+3).glass_area) * var(7);
        winArea5 =  str2double(B.thermal_model_data.windows(1,indexWin+4).glass_area) * var(7);
        winArea6 = str2double( B.thermal_model_data.windows(1,indexWin+5).glass_area) * var(7);
        wallArea1=(str2double(B.thermal_model_data.building_elements(1,index+2).area) * var(7)) - winArea1 ;
        wallArea2=(str2double(B.thermal_model_data.building_elements(1,index+3).area ) * var(7)) - winArea2;
        wallArea3=(str2double(B.thermal_model_data.building_elements(1,index+4).area ) * var(7)) - winArea3;
        wallArea4=(str2double(B.thermal_model_data.building_elements(1,index+5).area ) * var(7)) - winArea4;
        wallArea5=(str2double(B.thermal_model_data.building_elements(1,index+6).area  ) * var(7))- winArea5;
        wallArea6=(str2double(B.thermal_model_data.building_elements(1,index+7).area  ) * var(7))- winArea6;
 
         TotCostGround = str2double(B.thermal_model_data.building_elements(1,1).area) * costGround(var(13)) ...
            + str2double(B.thermal_model_data.building_elements(1,2).area) * costGround(var(13));
        
        
        TotalFloorArea = (str2double(B.thermal_model_data.building_elements(1,index).area) ...
            + str2double(B.thermal_model_data.building_elements(1,index+1).area)) * var(7);
        TotCostFloor =  (TotalFloorArea * costFloor(var(13))) - ((TotalFloorArea/var(7)) * costFloor(var(13)));
        
        TotCostRoof = (str2double(B.thermal_model_data.building_elements(1,index).area) ...
            + str2double(B.thermal_model_data.building_elements(1,index+1).area)) ...
            * costRoof(var(14));
        
        TotCostInsul = (wallArea1+wallArea2+wallArea3+wallArea4+wallArea5+wallArea6) * var(16) * costInsul;
        TotCostWall = (wallArea1+wallArea2+wallArea3+wallArea4+wallArea5+wallArea6) * costWall(var(12));
        TotCostWin = (winArea1+winArea2+winArea3+winArea4+winArea5+winArea6)  * costWin(var(15));

end




DiscountRate = 0.02;

discGround = calcCapital(TotCostGround, 70, DiscountRate);
discFloor = calcCapital(TotCostFloor, 70, DiscountRate);
discRoof = calcCapital(TotCostRoof, 50, DiscountRate);
discInsul = calcCapital(TotCostInsul, 40, DiscountRate);
discWall = calcCapital(TotCostWall, 70, DiscountRate);
discWin = calcCapital(TotCostWin, 40, DiscountRate);



CAPITAL = (discGround + discFloor + discRoof + discInsul+ discWall + discWin) / TotalFloorArea;  % specific capital (per sqm), otherwise minimising total costs and shrinking building

end



function [discountedCap] = calcCapital(InvestmentCost, TimeFrame, DiscountRate)

% discountedCap=(InvestmentCost*((1+DiscountRate)^TimeFrame))/TimeFrame;
discountedCap= InvestmentCost / TimeFrame;

end