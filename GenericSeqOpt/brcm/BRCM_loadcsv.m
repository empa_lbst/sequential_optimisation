
function lines = BRCM_loadcsv(string, thermOrEHF, BRCMfilepath)


str = (strcat(BRCMfilepath,'\',thermOrEHF,string));
fid = fopen(str,'r+');
lines=[];
while feof(fid)==0
    lines{end+1}=fgetl(fid); % get all lines
end
fclose(fid);



end