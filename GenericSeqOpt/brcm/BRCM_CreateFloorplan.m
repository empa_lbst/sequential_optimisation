function BRCM_CreateFloorplan(var, BElines, WINlines, Typology, filepath)
% BElines = NEWbuildingelements... each line of the csv file buildingelements.csv, contains coordinates, which have to be changed
% var(1)    -   Control Point X [-1,1]      changing typology and floorplan size
% var(2)    -   Control Point Y [-1,1]      changing typology and floorplan size
% var(3)    -   Control Point Z [0,2]       changing typology and floorplan size
% var(5)    -   Length/Width    [-1,1]      Length/Width Ratio. stretching east/west (1) or north/south (-1)
% var(6)    -   Ceiling Height  [3,5]       Ceiling height per floor, in meters
% var(7)    -   No. of storeys  [1,10]      Number of storeys.
% var(8)    -   Glazing S       [0.01,0.9]  Glazing Ratio on South Facades
% var(9)    -   Glazing E       [0.01,0.9]  Glazing Ratio on East Facades
% var(10)    -   Glazing N       [0.01,0.9]  Glazing Ratio on North Facades
% var(11)   -   Glazing W       [0.01,0.9]  Glazing Ratio on West Facades




% var(5) > 0            % var(5) = 0        % var(5) < 0
%      _____            %    ___            %        _
%     |_____|           %   |   |           %       | |
%                           |___|           %       | |
%                                           %       |_|
% stretch X (1.5)       % square            % stretch Y (1.5)
% compress Y (0.5)                          % compress Y (0.5)

FloorplanPtCorner = -5*var(3)+20;   % max 40m x 40m 
FloorplanPtCornerStretchX = (0.5*var(5) + 1) * FloorplanPtCorner;
FloorplanPtCornerStretchY = (-0.5*var(5)+ 1) * FloorplanPtCorner;
FloorplanPt1 = {FloorplanPtCornerStretchX, -FloorplanPtCornerStretchY, 0};
FloorplanPt2 = {-FloorplanPtCornerStretchX, -FloorplanPtCornerStretchY, 0};
FloorplanPt3 = {-FloorplanPtCornerStretchX, FloorplanPtCornerStretchY, 0};
FloorplanPt4 = {FloorplanPtCornerStretchX, FloorplanPtCornerStretchY, 0};
FloorplanPts = {FloorplanPt1,FloorplanPt2,FloorplanPt3,FloorplanPt4};

% CourtyardPtCorner = -5*var(3)+10;   % max 20m x 20m 
% CourtyardPtCornerStretchX = (0.5*var(5) + 1) * CourtyardPtCorner;
% CourtyardPtCornerStretchY = (-0.5*var(5)+ 1) * CourtyardPtCorner;
% CourtyardPt1 = {CourtyardPtCornerStretchX, -CourtyardPtCornerStretchY, 0};
% CourtyardPt2 = {-CourtyardPtCornerStretchX, -CourtyardPtCornerStretchY, 0};
% CourtyardPt3 = {-CourtyardPtCornerStretchX, CourtyardPtCornerStretchY, 0};
% CourtyardPt4 = {CourtyardPtCornerStretchX, CourtyardPtCornerStretchY, 0};
% CourtyardPts = {CourtyardPt1,CourtyardPt2,CourtyardPt3,CourtyardPt4};


switch Typology
    case 'ThermalModel1'    % Block
        [BElines, WINlines] = BRCM_floorplan_Block(BElines, WINlines, var(6), var(7), var(8), var(9), var(10), var(11), FloorplanPts);
        
        
        
        
        
        
        
        
        
        
        
        
    case 'ThermalModel2'    % Courtyard    
        % modify courtyard according to var(1) and var(2)
        MinDisToWall = 1;

        CourtyardPtCorner = -5*var(3)+10;   % max 20m x 20m
        CourtyardPtCornerStretchX = (0.5*var(5) + 1) * CourtyardPtCorner;
        CourtyardPtCornerStretchY = (-0.5*var(5)+ 1) * CourtyardPtCorner;
        
        CourtyardPlusX = var(1) * FloorplanPtCornerStretchX;
        CourtyardPlusY = var(2) * FloorplanPtCornerStretchY;
        
        %min distance courtyard to wall... check difference courtyard point to
        %floorplan point. if differene is less than min distance, then make
        %it min distance
        CourtyardPt1 = {CourtyardPtCornerStretchX + CourtyardPlusX, -CourtyardPtCornerStretchY + CourtyardPlusY, 0};
        if abs(FloorplanPt1{1} - CourtyardPt1{1} ) < MinDisToWall
            CourtyardPt1{1} = FloorplanPt1{1} - MinDisToWall;
        end
        if abs(FloorplanPt1{2} - CourtyardPt1{2}) < MinDisToWall
            CourtyardPt1{2} = FloorplanPt1{2} + MinDisToWall;
        end
        
        CourtyardPt2 = {-CourtyardPtCornerStretchX + CourtyardPlusX, -CourtyardPtCornerStretchY + CourtyardPlusY, 0};
        if abs(FloorplanPt2{1} - CourtyardPt2{1}) < MinDisToWall
            CourtyardPt2{1} = FloorplanPt2{1} + MinDisToWall;
        end
        if abs(FloorplanPt2{2} - CourtyardPt2{2}) < MinDisToWall
            CourtyardPt2{2} = FloorplanPt2{2} + MinDisToWall;
        end
        
        CourtyardPt3 = {-CourtyardPtCornerStretchX + CourtyardPlusX, CourtyardPtCornerStretchY + CourtyardPlusY, 0};
        if abs(FloorplanPt3{1} - CourtyardPt3{1}) < MinDisToWall
            CourtyardPt3{1} = FloorplanPt3{1} + MinDisToWall;
        end
        if abs(FloorplanPt3{2} - CourtyardPt3{2}) < MinDisToWall
            CourtyardPt3{2} = FloorplanPt3{2} - MinDisToWall;
        end
        
        CourtyardPt4 = {CourtyardPtCornerStretchX + CourtyardPlusX, CourtyardPtCornerStretchY + CourtyardPlusY, 0};
        if abs(FloorplanPt4{1} - CourtyardPt4{1}) < MinDisToWall
            CourtyardPt4{1} = FloorplanPt4{1} - MinDisToWall;
        end
        if abs(FloorplanPt4{2} - CourtyardPt4{2}) < MinDisToWall
            CourtyardPt4{2} = FloorplanPt4{2} - MinDisToWall;
        end
        
        CourtyardPts = {CourtyardPt1,CourtyardPt2,CourtyardPt3,CourtyardPt4};

        [BElines, WINlines] = BRCM_floorplan_Courtyard(BElines, WINlines, ...
            var(6), var(7), var(8), var(9), var(10), var(11), ...
            FloorplanPts, CourtyardPts);
        
        
        
        
        
        
        
        
        
        
        
    case 'ThermalModel3'        % U-Shape
        % modify courtyard according to var(1) and var(2)
        MinDisToWall = 1;
        
        CourtyardPtCorner = -5*var(3)+10;   % max 20m x 20m
        CourtyardPtCornerStretchX = (0.5*var(5) + 1) * CourtyardPtCorner;
        CourtyardPtCornerStretchY = (-0.5*var(5)+ 1) * CourtyardPtCorner;
        CourtyardPlusX = var(1) * FloorplanPtCornerStretchX;
        CourtyardPlusY = var(2) * FloorplanPtCornerStretchY;        % var(2) *-1 case 180
        
        if abs(var(1)) > abs(var(2))   % [  or   ] .... rotate floorplan and cuortyard pts
            if var(1) > 0
                theta = -pi/2;
                rotation = -90;
            else
                theta = pi/2;
                rotation = 90;
            end

            CourtyardPt1 = {CourtyardPtCornerStretchX + CourtyardPlusX, -CourtyardPtCornerStretchY + CourtyardPlusY, 0};
            if abs(FloorplanPt1{2} - CourtyardPt1{2}) < MinDisToWall
                CourtyardPt1{2} = FloorplanPt1{2} + MinDisToWall;
            end
            CourtyardPt2 = {-CourtyardPtCornerStretchX + CourtyardPlusX, -CourtyardPtCornerStretchY + CourtyardPlusY, 0};
            if abs(FloorplanPt2{2} - CourtyardPt2{2}) < MinDisToWall
                CourtyardPt2{2} = FloorplanPt2{2} + MinDisToWall;
            end
            CourtyardPt3 = {-CourtyardPtCornerStretchX + CourtyardPlusX, CourtyardPtCornerStretchY + CourtyardPlusY, 0};
            if abs(FloorplanPt3{2} - CourtyardPt3{2}) < MinDisToWall
                CourtyardPt3{2} = FloorplanPt3{2} - MinDisToWall;
            end
            CourtyardPt4 = {CourtyardPtCornerStretchX + CourtyardPlusX, CourtyardPtCornerStretchY + CourtyardPlusY, 0};
            if abs(FloorplanPt4{2} - CourtyardPt4{2}) < MinDisToWall
                CourtyardPt4{2} = FloorplanPt4{2} - MinDisToWall;
            end
            CourtyardPts = {CourtyardPt1,CourtyardPt2,CourtyardPt3,CourtyardPt4};
            
            xIn = [FloorplanPts{1}{1} FloorplanPts{2}{1} FloorplanPts{3}{1} FloorplanPts{4}{1}];
            yIn = [FloorplanPts{1}{2} FloorplanPts{2}{2} FloorplanPts{3}{2} FloorplanPts{4}{2}];
            x_center = 0;
            y_center = 0;
            [x_rot, y_rot] = BRCM_rotatePoints(xIn, yIn, x_center, y_center, theta);
            FloorplanPt1 = {x_rot(1), y_rot(1), 0};
            FloorplanPt2 = {x_rot(2), y_rot(2), 0};
            FloorplanPt3 = {x_rot(3), y_rot(3), 0};
            FloorplanPt4 = {x_rot(4), y_rot(4), 0};
            FloorplanPts = {FloorplanPt1,FloorplanPt2,FloorplanPt3,FloorplanPt4};
            
            xIn = [CourtyardPts{1}{1} CourtyardPts{2}{1} CourtyardPts{3}{1} CourtyardPts{4}{1}];
            yIn = [CourtyardPts{1}{2} CourtyardPts{2}{2} CourtyardPts{3}{2} CourtyardPts{4}{2}];
            x_center = 0;
            y_center = 0;
            [x_rot, y_rot] = BRCM_rotatePoints(xIn, yIn, x_center, y_center, theta);
            if var(1) < 0
                CourtyardPt1 = {x_rot(1), y_rot(1), 0};
                CourtyardPt2 = {x_rot(2), FloorplanPts{2}{2}, 0};
                CourtyardPt3 = {x_rot(3), FloorplanPts{3}{2}, 0};
                CourtyardPt4 = {x_rot(4), y_rot(4), 0};
            else
                CourtyardPt1 = {x_rot(1), FloorplanPts{1}{2}, 0};
                CourtyardPt2 = {x_rot(2), y_rot(2), 0};
                CourtyardPt3 = {x_rot(3), y_rot(3), 0};
                CourtyardPt4 = {x_rot(4), FloorplanPts{1}{2}, 0};
            end

            CourtyardPts = {CourtyardPt1,CourtyardPt2,CourtyardPt3,CourtyardPt4};
            
        else                            % U  or    A   ... dont rotate floorplan pts, only make var(2) negative
            rotation = 0;
            if var(2) > 0
                var(2) = var(2)*-1;
                rotation = 180;
                CourtyardPlusY = var(2) * FloorplanPtCornerStretchY;        % var(2) *-1 case 180
            end

            CourtyardPt1 = {CourtyardPtCornerStretchX + CourtyardPlusX, FloorplanPts{1}{2}, 0};
            CourtyardPt2 = {-CourtyardPtCornerStretchX + CourtyardPlusX, FloorplanPts{1}{2}, 0};
            CourtyardPt3 = {-CourtyardPtCornerStretchX + CourtyardPlusX, CourtyardPtCornerStretchY + CourtyardPlusY, 0};
            CourtyardPt4 = {CourtyardPtCornerStretchX + CourtyardPlusX, CourtyardPtCornerStretchY + CourtyardPlusY, 0};
            CourtyardPts = {CourtyardPt1,CourtyardPt2,CourtyardPt3,CourtyardPt4};
        end
        
        [BElines, WINlines] = BRCM_floorplan_UShape(BElines, WINlines, ...
            var(6), var(7), var(8), var(9), var(10), var(11), ...
            FloorplanPts, CourtyardPts, rotation);
   
    
    
    
    
    
    
    
    
    
    
    case 'ThermalModel4'       % L-Shape
       
        
        if var(1) > 0 && var(2) > 0   %  |_      flip north south
            var(2) = var(2)*-1;
            
            
                                         %   _
        elseif var(1) < 0 && var(2) < 0  %    |     flip east west
            var(1) = var(1)*-1;
            
            
        elseif var(1) < 0 && var(2) > 0  %  _|      rotate -180
            var(1) = var(1)*-1;
            var(2) = var(2)*-1;
            
                                           %_
%         else % var(1) > 0 && var(2) < 0   %|        normal case
            
            
            
        end
        
        CourtyardPtCorner = -5*var(3)+10;   % max 20m x 20m
        CourtyardPtCornerStretchX = (0.5*var(5) + 1) * CourtyardPtCorner;
        CourtyardPtCornerStretchY = (-0.5*var(5)+ 1) * CourtyardPtCorner;
        CourtyardPlusX = var(1) * FloorplanPtCornerStretchX;
        CourtyardPlusY = var(2) * FloorplanPtCornerStretchY;
        
        CourtyardPt1 = {CourtyardPtCornerStretchX + CourtyardPlusX, -CourtyardPtCornerStretchY + CourtyardPlusY, 0};
        CourtyardPt2 = {-CourtyardPtCornerStretchX + CourtyardPlusX, FloorplanPts{2}{2}, 0};
        CourtyardPt3 = {-CourtyardPtCornerStretchX + CourtyardPlusX, CourtyardPtCornerStretchY + CourtyardPlusY, 0};
        CourtyardPt4 = {FloorplanPts{4}{1}, CourtyardPtCornerStretchY + CourtyardPlusY, 0};
        
        
        
        CourtyardPts = {CourtyardPt1,CourtyardPt2,CourtyardPt3,CourtyardPt4};
        
        [BElines, WINlines] = BRCM_floorplan_LShape(BElines, WINlines, ...
            var(6), var(7), var(8), var(9), var(10), var(11), ...
            FloorplanPts, CourtyardPts);
        
end




strToFind={};
replaceString={};
replacePosition=[];

BRCM_newcsv(strcat(filepath,'\',Typology,'\buildingelements.csv'), BElines, strToFind, replaceString, replacePosition);

BRCM_newcsv(strcat(filepath,'\',Typology,'\windows.csv'), WINlines, strToFind, replaceString, replacePosition);







end




function [x_rot, y_rot] = BRCM_rotatePoints(x, y, x_center, y_center, theta)


% define a 60 degree counter-clockwise rotation matrix

% theta = -pi/2; % pi/3 radians = 60 degrees

R = [cos(theta) -sin(theta) 0; sin(theta) cos(theta) 0; 0 0 1];

%Define affine transformation for translation

a = [1 0 x_center;0 1 y_center; 0 0 1];

c = [1 0 -x_center;0 1 -y_center; 0 0 1];

M = a*R*c;

for i=1:size(x,2)
    
    rot(:,i) = M*[x(i) y(i) 1]';
    
end

% pick out the vectors of rotated x- and y-data

x_rot = rot(1,:);

y_rot = rot(2,:);

% make a plot

% plot(x,y,'k-',x_rot, y_rot, 'r-', x_center, y_center, 'bo');


end




function strVertices = BRCM_getVertices(Pts)

strVertices = strcat(...
    '(',num2str(Pts{1}{1}),',',num2str(Pts{1}{2}),',',num2str(Pts{1}{3}),')',',', ...
    '(',num2str(Pts{2}{1}),',',num2str(Pts{2}{2}),',',num2str(Pts{2}{3}),')',',', ...
    '(',num2str(Pts{3}{1}),',',num2str(Pts{3}{2}),',',num2str(Pts{3}{3}),')',',', ...
    '(',num2str(Pts{4}{1}),',',num2str(Pts{4}{2}),',',num2str(Pts{4}{3}),')');
end


%% Block
function [BElines, WINlines] = BRCM_floorplan_Block(BElines, WINlines, var6, var7, var8, var9, var10, var11, FloorplanPts)



str = strsplit(BElines{2},';');
strVert = BRCM_getVertices(FloorplanPts);
area=num2str(BRCM_getPolygonArea(FloorplanPts,'Z'));
BElines(2) = strcat(str(1),';',str(2),';',str(3),';',str(4),';',str(5),';',str(6),';',area,';',strVert,';');


for i=0:var7-1
    
    % ceiling building elements
    index = i*5+3;
    PtsNew = {{FloorplanPts{1}{1},FloorplanPts{1}{2},FloorplanPts{1}{3}+((i+1)*var6)}, ...
        {FloorplanPts{2}{1},FloorplanPts{2}{2},FloorplanPts{2}{3}+((i+1)*var6)},...
        {FloorplanPts{3}{1}, FloorplanPts{3}{2}, FloorplanPts{3}{3}+((i+1)*var6)},...
        {FloorplanPts{4}{1}, FloorplanPts{4}{2}, FloorplanPts{4}{3}+((i+1)*var6)}};
    str = strsplit(BElines{index}, ';');
    strVert = BRCM_getVertices(PtsNew);
    area = num2str(BRCM_getPolygonArea(PtsNew,'Z'));
    BElines(index) = strcat(str(1),';', str(2),';', str(3),';', str(4),';', str(5),';', str(6),';', area,';', strVert,';'); % ceiling
    
    
    
    % wall east buildingelement
    index = i*5+3+1;
    PtsNew = {{FloorplanPts{1}{1},FloorplanPts{1}{2},FloorplanPts{1}{3}+((i)*var6)}, ...
        {FloorplanPts{4}{1},FloorplanPts{4}{2},FloorplanPts{4}{3}+((i)*var6)},...
        {FloorplanPts{4}{1}, FloorplanPts{4}{2}, FloorplanPts{4}{3}+((i+1)*var6)},...
        {FloorplanPts{1}{1}, FloorplanPts{1}{2}, FloorplanPts{1}{3}+((i+1)*var6)}};
    str = strsplit(BElines{index}, ';');
    strVert = BRCM_getVertices(PtsNew);
    area = BRCM_getPolygonArea(PtsNew,'X');
    BElines(index) = strcat(str(1),';', str(2),';', str(3),';', str(4),';', str(5),';', str(6),';', num2str(area),';', strVert,';');   %wall east
    % wall east window
    index = i*4 +2;
    str = strsplit(WINlines{index}, ';');
    area = area * var9;
    WINlines(index) = strcat(str(1),';',str(2),';',num2str(area),';0.0;',str(5),';',str(6),';');
     
    
    
    
    
     
    % wall north
    index = i*5+3+2;
    PtsNew = {{FloorplanPts{4}{1},FloorplanPts{4}{2},FloorplanPts{4}{3}+((i)*var6)}, ...
        {FloorplanPts{3}{1},FloorplanPts{3}{2},FloorplanPts{3}{3}+((i)*var6)},...
        {FloorplanPts{3}{1}, FloorplanPts{3}{2}, FloorplanPts{3}{3}+((i+1)*var6)},...
        {FloorplanPts{4}{1}, FloorplanPts{4}{2}, FloorplanPts{4}{3}+((i+1)*var6)}};
    str = strsplit(BElines{index}, ';');
    strVert = BRCM_getVertices(PtsNew);
    area = BRCM_getPolygonArea(PtsNew,'Y');
    BElines(index) = strcat(str(1),';', str(2),';', str(3),';', str(4),';', str(5),';', str(6),';', num2str(area),';', strVert,';');   %wall north
    % wall north window
    index = i*4 +3;
    str = strsplit(WINlines{index}, ';');
    area = area * var10;
    WINlines(index) = strcat(str(1),';',str(2),';',num2str(area),';0.0;',str(5),';',str(6),';');
    
    
    
    
    % wall west
    index = i*5+3+3;
    PtsNew = {{FloorplanPts{3}{1},FloorplanPts{3}{2},FloorplanPts{3}{3}+((i)*var6)}, ...
        {FloorplanPts{2}{1},FloorplanPts{2}{2},FloorplanPts{2}{3}+((i)*var6)},...
        {FloorplanPts{2}{1}, FloorplanPts{2}{2}, FloorplanPts{2}{3}+((i+1)*var6)},...
        {FloorplanPts{3}{1}, FloorplanPts{3}{2}, FloorplanPts{3}{3}+((i+1)*var6)}};
    str = strsplit(BElines{index}, ';');
    strVert = BRCM_getVertices(PtsNew);
    area = BRCM_getPolygonArea(PtsNew,'X');
    BElines(index) = strcat(str(1),';', str(2),';', str(3),';', str(4),';', str(5),';', str(6),';', num2str(area),';', strVert,';');   %wall west
    % wall north window
    index = i*4 +4;
    str = strsplit(WINlines{index}, ';');
    area = area * var11;
    WINlines(index) = strcat(str(1),';',str(2),';',num2str(area),';0.0;',str(5),';',str(6),';');
    
    
    
    
    % wall south
    index = i*5+3+4;
    PtsNew = {{FloorplanPts{2}{1},FloorplanPts{2}{2},FloorplanPts{2}{3}+((i)*var6)}, ...
        {FloorplanPts{1}{1},FloorplanPts{1}{2},FloorplanPts{1}{3}+((i)*var6)},...
        {FloorplanPts{1}{1}, FloorplanPts{1}{2}, FloorplanPts{1}{3}+((i+1)*var6)},...
        {FloorplanPts{2}{1}, FloorplanPts{2}{2}, FloorplanPts{2}{3}+((i+1)*var6)}};
    str = strsplit(BElines{index}, ';');
    strVert = BRCM_getVertices(PtsNew);
    area = BRCM_getPolygonArea(PtsNew,'Y');
    BElines(index) = strcat(str(1),';', str(2),';', str(3),';', str(4),';', str(5),';', str(6),';', num2str(area),';', strVert,';');   %wall south
    % wall south window
    index = i*4 +5;
    str = strsplit(WINlines{index}, ';');
    area = area * var8;
    WINlines(index) = strcat(str(1),';',str(2),';',num2str(area),';0.0;',str(5),';',str(6),';');
    
end



end



%% Courtyard
function [BElines, WINlines] = BRCM_floorplan_Courtyard(BElines, WINlines, var6, var7, var8, var9, var10, var11, FloorplanPts, CourtyardPts)


% groundfloors
midCourtyardY = CourtyardPts{3}{2} - (abs(CourtyardPts{3}{2} - CourtyardPts{2}{2}) / 2 );
%groundfloor surface 1/6
str = strsplit(BElines{2},';');
PtsNew = {CourtyardPts{2}, ...
    {FloorplanPts{2}{1},CourtyardPts{2}{2},0}, ...
    {FloorplanPts{2}{1},midCourtyardY,0}, ...
    {CourtyardPts{2}{1},midCourtyardY,0}};
strVert = BRCM_getVertices(PtsNew);
area=num2str(BRCM_getPolygonArea(PtsNew,'Z'));
BElines(2) = strcat(str(1),';',str(2),';',str(3),';',str(4),';',str(5),';',str(6),';',area,';',strVert,';');
%groundfloor surface 2/6
str = strsplit(BElines{3},';');
PtsNew = {{CourtyardPts{1}{1}, FloorplanPts{1}{2}, 0}, ...
    FloorplanPts{2}, ...
    {FloorplanPts{2}{1},CourtyardPts{2}{2},0}, ...
    CourtyardPts{1}};
strVert = BRCM_getVertices(PtsNew);
area=num2str(BRCM_getPolygonArea(PtsNew,'Z'));
BElines(3) = strcat(str(1),';',str(2),';',str(3),';',str(4),';',str(5),';',str(6),';',area,';',strVert,';');
%groundfloor surface 3/6
str = strsplit(BElines{4},';');
PtsNew = {FloorplanPts{1}, ...
    {CourtyardPts{1}{1},FloorplanPts{1}{2},0}, ...
    {CourtyardPts{1}{1}, midCourtyardY, 0}, ...
    {FloorplanPts{1}{1},midCourtyardY, 0}};
strVert = BRCM_getVertices(PtsNew);
area=num2str(BRCM_getPolygonArea(PtsNew,'Z'));
BElines(4) = strcat(str(1),';',str(2),';',str(3),';',str(4),';',str(5),';',str(6),';',area,';',strVert,';');
%groundfloor surface 4/6
str = strsplit(BElines{5},';');
PtsNew = {{FloorplanPts{1}{1}, midCourtyardY, 0}, ...
    {CourtyardPts{1}{1}, midCourtyardY, 0}, ...
    CourtyardPts{4}, ...
    {FloorplanPts{1}{1},CourtyardPts{4}{2}, 0}};
strVert = BRCM_getVertices(PtsNew);
area=num2str(BRCM_getPolygonArea(PtsNew,'Z'));
BElines(5) = strcat(str(1),';',str(2),';',str(3),';',str(4),';',str(5),';',str(6),';',area,';',strVert,';');
%groundfloor surface 5/6
str = strsplit(BElines{6},';');
PtsNew = {{FloorplanPts{4}{1}, CourtyardPts{4}{2}, 0}, ...
    CourtyardPts{3}, ...
    {CourtyardPts{3}{1}, FloorplanPts{3}{2}, 0}, ...
    FloorplanPts{4}};
strVert = BRCM_getVertices(PtsNew);
area=num2str(BRCM_getPolygonArea(PtsNew,'Z'));
BElines(6) = strcat(str(1),';',str(2),';',str(3),';',str(4),';',str(5),';',str(6),';',area,';',strVert,';');
%groundfloor surface 6/6
str = strsplit(BElines{7},';');
PtsNew = {{CourtyardPts{2}{1},midCourtyardY,0}, ...
    {FloorplanPts{2}{1},midCourtyardY,0}, ...
    FloorplanPts{3}, ...
    {CourtyardPts{3}{1},FloorplanPts{3}{2},0}};
strVert = BRCM_getVertices(PtsNew);
area=num2str(BRCM_getPolygonArea(PtsNew,'Z'));
BElines(7) = strcat(str(1),';',str(2),';',str(3),';',str(4),';',str(5),';',str(6),';',area,';',strVert,';');


for i=0:var7-1
    
    % ceiling building elements
    %ceiling surface 1/6
    index = i*14+8;
    PtsNew = {{CourtyardPts{2}{1}, CourtyardPts{2}{2}, (i+1)*var6}, ...
        {FloorplanPts{2}{1},CourtyardPts{2}{2}, (i+1)*var6}, ...
        {FloorplanPts{2}{1},midCourtyardY,(i+1)*var6}, ...
        {CourtyardPts{2}{1},midCourtyardY,(i+1)*var6}};
    str = strsplit(BElines{index},';');
    strVert = BRCM_getVertices(PtsNew);
    area=num2str(BRCM_getPolygonArea(PtsNew,'Z'));
    BElines(index) = strcat(str(1),';',str(2),';',str(3),';',str(4),';',str(5),';',str(6),';',area,';',strVert,';');
    
    %ceiling surface 2/6
    index = i*14+8+1;
    str = strsplit(BElines{index},';');
    PtsNew = {{CourtyardPts{1}{1}, FloorplanPts{1}{2}, (i+1)*var6}, ...
        {FloorplanPts{2}{1}, FloorplanPts{2}{2},(i+1)*var6}, ...
        {FloorplanPts{2}{1}, CourtyardPts{2}{2},(i+1)*var6}, ...
        {CourtyardPts{1}{1}, CourtyardPts{1}{2},(i+1)*var6}};
    strVert = BRCM_getVertices(PtsNew);
    area=num2str(BRCM_getPolygonArea(PtsNew,'Z'));
    BElines(index) = strcat(str(1),';',str(2),';',str(3),';',str(4),';',str(5),';',str(6),';',area,';',strVert,';');
    
    %ceiling surface 3/6
    index = i*14+8+2;
    str = strsplit(BElines{index},';');
    PtsNew = {{FloorplanPts{1}{1}, FloorplanPts{1}{2}, (i+1)*var6}, ...
        {CourtyardPts{1}{1},FloorplanPts{1}{2},(i+1)*var6}, ...
        {CourtyardPts{1}{1}, midCourtyardY, (i+1)*var6}, ...
        {FloorplanPts{1}{1},midCourtyardY, (i+1)*var6}};
    strVert = BRCM_getVertices(PtsNew);
    area=num2str(BRCM_getPolygonArea(PtsNew,'Z'));
    BElines(index) = strcat(str(1),';',str(2),';',str(3),';',str(4),';',str(5),';',str(6),';',area,';',strVert,';');
    
    %ceiling surface 4/6
    index = i*14+8+3;
    str = strsplit(BElines{index},';');
    PtsNew = {{FloorplanPts{1}{1}, midCourtyardY, (i+1)*var6}, ...
        {CourtyardPts{1}{1}, midCourtyardY, (i+1)*var6}, ...
        {CourtyardPts{4}{1}, CourtyardPts{4}{2}, (i+1)*var6}, ...
        {FloorplanPts{1}{1},CourtyardPts{4}{2}, (i+1)*var6}};
    strVert = BRCM_getVertices(PtsNew);
    area=num2str(BRCM_getPolygonArea(PtsNew,'Z'));
    BElines(index) = strcat(str(1),';',str(2),';',str(3),';',str(4),';',str(5),';',str(6),';',area,';',strVert,';');
    
    %ceiling surface 5/6
    index = i*14+8+4;
    str = strsplit(BElines{index},';');
    PtsNew = {{FloorplanPts{4}{1}, CourtyardPts{4}{2}, (i+1)*var6}, ...
        {CourtyardPts{3}{1}, CourtyardPts{3}{2},(i+1)*var6}, ...
        {CourtyardPts{3}{1}, FloorplanPts{3}{2}, (i+1)*var6}, ...
        {FloorplanPts{4}{1},FloorplanPts{4}{2},(i+1)*var6}};
    strVert = BRCM_getVertices(PtsNew);
    area=num2str(BRCM_getPolygonArea(PtsNew,'Z'));
    BElines(index) = strcat(str(1),';',str(2),';',str(3),';',str(4),';',str(5),';',str(6),';',area,';',strVert,';');
        
    %ceiling surface 6/6
    index = i*14+8+5;
    str = strsplit(BElines{index},';');
PtsNew = {{CourtyardPts{2}{1},midCourtyardY,(i+1)*var6}, ...
    {FloorplanPts{2}{1},midCourtyardY,(i+1)*var6}, ...
    {FloorplanPts{3}{1},FloorplanPts{3}{2},(i+1)*var6}, ...
    {CourtyardPts{3}{1},FloorplanPts{3}{2},(i+1)*var6}};
strVert = BRCM_getVertices(PtsNew);
area=num2str(BRCM_getPolygonArea(PtsNew,'Z'));
BElines(index) = strcat(str(1),';',str(2),';',str(3),';',str(4),';',str(5),';',str(6),';',area,';',strVert,';');
    
    
    
    
    % wall east buildingelement 1/8
    index = i*14+8+6;
    PtsNew = {{FloorplanPts{1}{1},FloorplanPts{1}{2},FloorplanPts{1}{3}+((i)*var6)}, ...
        {FloorplanPts{4}{1},FloorplanPts{4}{2},FloorplanPts{4}{3}+((i)*var6)},...
        {FloorplanPts{4}{1}, FloorplanPts{4}{2}, FloorplanPts{4}{3}+((i+1)*var6)},...
        {FloorplanPts{1}{1}, FloorplanPts{1}{2}, FloorplanPts{1}{3}+((i+1)*var6)}};
    str = strsplit(BElines{index}, ';');
    strVert = BRCM_getVertices(PtsNew);
    area = BRCM_getPolygonArea(PtsNew,'X');
    BElines(index) = strcat(str(1),';', str(2),';', str(3),';', str(4),';', str(5),';', str(6),';', num2str(area),';', strVert,';');   %wall east
    % wall east window 1/8
    index = i*8 +2; 
    str = strsplit(WINlines{index}, ';');
    area = area * var9;
    WINlines(index) = strcat(str(1),';',str(2),';',num2str(area),';0.0;',str(5),';',str(6),';');
    
    % wall north buildingelement 2/8
    index = i*14+8+7;
    PtsNew = {{FloorplanPts{4}{1},FloorplanPts{4}{2},FloorplanPts{4}{3}+((i)*var6)}, ...
        {FloorplanPts{3}{1},FloorplanPts{3}{2},FloorplanPts{3}{3}+((i)*var6)},...
        {FloorplanPts{3}{1}, FloorplanPts{3}{2}, FloorplanPts{3}{3}+((i+1)*var6)},...
        {FloorplanPts{4}{1}, FloorplanPts{4}{2}, FloorplanPts{4}{3}+((i+1)*var6)}};
    str = strsplit(BElines{index}, ';');
    strVert = BRCM_getVertices(PtsNew);
    area = BRCM_getPolygonArea(PtsNew,'Y');
    BElines(index) = strcat(str(1),';', str(2),';', str(3),';', str(4),';', str(5),';', str(6),';', num2str(area),';', strVert,';');   %wall north
    % wall north window window 2/8
    index = i*8 +3; 
    str = strsplit(WINlines{index}, ';');
    area = area * var10;
    WINlines(index) = strcat(str(1),';',str(2),';',num2str(area),';0.0;',str(5),';',str(6),';');
    
    % wall west buildingelement 3/8
    index = i*14+8+8;
    PtsNew = {{FloorplanPts{3}{1},FloorplanPts{3}{2},FloorplanPts{3}{3}+((i)*var6)}, ...
        {FloorplanPts{2}{1},FloorplanPts{2}{2},FloorplanPts{2}{3}+((i)*var6)},...
        {FloorplanPts{2}{1}, FloorplanPts{2}{2}, FloorplanPts{2}{3}+((i+1)*var6)},...
        {FloorplanPts{3}{1}, FloorplanPts{3}{2}, FloorplanPts{3}{3}+((i+1)*var6)}};
    str = strsplit(BElines{index}, ';');
    strVert = BRCM_getVertices(PtsNew);
    area = BRCM_getPolygonArea(PtsNew,'X');
    BElines(index) = strcat(str(1),';', str(2),';', str(3),';', str(4),';', str(5),';', str(6),';', num2str(area),';', strVert,';');   %wall west
    % wall north window 3/8
    index = i*8 +4; 
    str = strsplit(WINlines{index}, ';');
    area = area * var11;
    WINlines(index) = strcat(str(1),';',str(2),';',num2str(area),';0.0;',str(5),';',str(6),';');

    % wall south buildingelement 4/8
    index = i*14+8+9;
    PtsNew = {{FloorplanPts{2}{1},FloorplanPts{2}{2},FloorplanPts{2}{3}+((i)*var6)}, ...
        {FloorplanPts{1}{1},FloorplanPts{1}{2},FloorplanPts{1}{3}+((i)*var6)},...
        {FloorplanPts{1}{1}, FloorplanPts{1}{2}, FloorplanPts{1}{3}+((i+1)*var6)},...
        {FloorplanPts{2}{1}, FloorplanPts{2}{2}, FloorplanPts{2}{3}+((i+1)*var6)}};
    str = strsplit(BElines{index}, ';');
    strVert = BRCM_getVertices(PtsNew);
    area = BRCM_getPolygonArea(PtsNew,'Y');
    BElines(index) = strcat(str(1),';', str(2),';', str(3),';', str(4),';', str(5),';', str(6),';', num2str(area),';', strVert,';');   %wall south
    % wall south window 4/8
    index = i*8 +5; 
    str = strsplit(WINlines{index}, ';');
    area = area * var8;
    WINlines(index) = strcat(str(1),';',str(2),';',num2str(area),';0.0;',str(5),';',str(6),';');
    
    
    
    
    
    
    
    
    % wall east buildingelement 5/8
    index = i*14+8+10;
    PtsNew = {{CourtyardPts{4}{1},CourtyardPts{4}{2},CourtyardPts{4}{3}+((i)*var6)}, ...
        {CourtyardPts{1}{1},CourtyardPts{1}{2},CourtyardPts{1}{3}+((i)*var6)},...
        {CourtyardPts{1}{1}, CourtyardPts{1}{2}, CourtyardPts{1}{3}+((i+1)*var6)},...
        {CourtyardPts{4}{1}, CourtyardPts{4}{2}, CourtyardPts{4}{3}+((i+1)*var6)}};
    str = strsplit(BElines{index}, ';');
    strVert = BRCM_getVertices(PtsNew);
    area = BRCM_getPolygonArea(PtsNew,'X');
    BElines(index) = strcat(str(1),';', str(2),';', str(3),';', str(4),';', str(5),';', str(6),';', num2str(area),';', strVert,';');   %wall east
    % wall east window 5/8
    index = i*8 +6; 
    str = strsplit(WINlines{index}, ';');
    area = area * var11;
    WINlines(index) = strcat(str(1),';',str(2),';',num2str(area),';0.0;',str(5),';',str(6),';');
    
    % wall north buildingelement 6/8
    index = i*14+8+11;
    PtsNew = {{CourtyardPts{1}{1},CourtyardPts{1}{2},CourtyardPts{1}{3}+((i)*var6)}, ...
        {CourtyardPts{2}{1},CourtyardPts{2}{2},CourtyardPts{2}{3}+((i)*var6)},...
        {CourtyardPts{2}{1}, CourtyardPts{2}{2}, CourtyardPts{2}{3}+((i+1)*var6)},...
        {CourtyardPts{1}{1}, CourtyardPts{1}{2}, CourtyardPts{1}{3}+((i+1)*var6)}};
    str = strsplit(BElines{index}, ';');
    strVert = BRCM_getVertices(PtsNew);
    area = BRCM_getPolygonArea(PtsNew,'Y');
    BElines(index) = strcat(str(1),';', str(2),';', str(3),';', str(4),';', str(5),';', str(6),';', num2str(area),';', strVert,';');   %wall north
    % wall north window window 6/8
    index = i*8 +7; 
    str = strsplit(WINlines{index}, ';');
    area = area * var10;
    WINlines(index) = strcat(str(1),';',str(2),';',num2str(area),';0.0;',str(5),';',str(6),';');
    
    % wall west buildingelement 7/8
    index = i*14+8+12;
    PtsNew = {{CourtyardPts{2}{1},CourtyardPts{2}{2},CourtyardPts{2}{3}+((i)*var6)}, ...
        {CourtyardPts{3}{1},CourtyardPts{3}{2},CourtyardPts{3}{3}+((i)*var6)},...
        {CourtyardPts{3}{1}, CourtyardPts{3}{2}, CourtyardPts{3}{3}+((i+1)*var6)},...
        {CourtyardPts{2}{1}, CourtyardPts{2}{2}, CourtyardPts{2}{3}+((i+1)*var6)}};
    str = strsplit(BElines{index}, ';');
    strVert = BRCM_getVertices(PtsNew);
    area = BRCM_getPolygonArea(PtsNew,'X');
    BElines(index) = strcat(str(1),';', str(2),';', str(3),';', str(4),';', str(5),';', str(6),';', num2str(area),';', strVert,';');   %wall west
    % wall north window 7/8
    index = i*8 +8; 
    str = strsplit(WINlines{index}, ';');
    area = area * var9;
    WINlines(index) = strcat(str(1),';',str(2),';',num2str(area),';0.0;',str(5),';',str(6),';');

    % wall south buildingelement 8/8
    index = i*14+8+13;
    PtsNew = {{CourtyardPts{3}{1},CourtyardPts{3}{2},CourtyardPts{3}{3}+((i)*var6)}, ...
        {CourtyardPts{4}{1},CourtyardPts{4}{2},CourtyardPts{4}{3}+((i)*var6)},...
        {CourtyardPts{4}{1}, CourtyardPts{4}{2}, CourtyardPts{4}{3}+((i+1)*var6)},...
        {CourtyardPts{3}{1}, CourtyardPts{3}{2}, CourtyardPts{3}{3}+((i+1)*var6)}};
    str = strsplit(BElines{index}, ';');
    strVert = BRCM_getVertices(PtsNew);
    area = BRCM_getPolygonArea(PtsNew,'Y');
    BElines(index) = strcat(str(1),';', str(2),';', str(3),';', str(4),';', str(5),';', str(6),';', num2str(area),';', strVert,';');   %wall south
    % wall south window 8/8
    index = i*8 +9; 
    str = strsplit(WINlines{index}, ';');
    area = area * var8;
    WINlines(index) = strcat(str(1),';',str(2),';',num2str(area),';0.0;',str(5),';',str(6),';');
    
end




end



%% UShape
function [BElines, WINlines] = BRCM_floorplan_UShape(BElines, WINlines, var6, var7, var8, var9, var10, var11, FloorplanPts, CourtyardPts, rotation)


switch rotation
    case {0,180}  % seperate case 180
        f1=FloorplanPts{1};
        f2=FloorplanPts{2};
        f3=FloorplanPts{3};
        f4=FloorplanPts{4};
        c1=CourtyardPts{1};
        c2=CourtyardPts{2};
        c3=CourtyardPts{3};
        c4=CourtyardPts{4};
        
        FloorplanPts{1} = f2;
        FloorplanPts{2} = f3;
        FloorplanPts{3} = f4;
        FloorplanPts{4} = f1;
        CourtyardPts{1} = c2;
        CourtyardPts{2} = c3;
        CourtyardPts{3} = c4;
        CourtyardPts{4} = c1;
 
    case 90
        f1=FloorplanPts{1};
        f2=FloorplanPts{2};
        f3=FloorplanPts{3};
        f4=FloorplanPts{4};
        c1=CourtyardPts{1};
        c2=CourtyardPts{2};
        c3=CourtyardPts{3};
        c4=CourtyardPts{4};
        
        FloorplanPts{1} = f3;
        FloorplanPts{2} = f4;
        FloorplanPts{3} = f1;
        FloorplanPts{4} = f2;
        CourtyardPts{1} = c3;
        CourtyardPts{2} = c4;
        CourtyardPts{3} = c1;
        CourtyardPts{4} = c2;
end


%var(8)    -   Glazing S       [0.01,0.9]  Glazing Ratio on South Facades
% var(9)    -   Glazing E       [0.01,0.9]  Glazing Ratio on East Facades
% var(10)    -   Glazing N       [0.01,0.9]  Glazing Ratio on North Facades
% var(11)   -   Glazing W       [0.01,0.9]  Glazing Ratio on West Facades

% groundfloors
%groundfloor surface 1/3
str = strsplit(BElines{2},';');
PtsNew = {FloorplanPts{4}, ...
    CourtyardPts{4}, ...
     CourtyardPts{3}, ...
    {FloorplanPts{4}{1},CourtyardPts{3}{2},0}};
strVert = BRCM_getVertices(PtsNew);
area=num2str(BRCM_getPolygonArea(PtsNew,'Z'));
BElines(2) = strcat(str(1),';',str(2),';',str(3),';',str(4),';',str(5),';',str(6),';',area,';',strVert,';');
%groundfloor surface 2/3
str = strsplit(BElines{3},';');
PtsNew = {{FloorplanPts{4}{1}, CourtyardPts{3}{2}, 0}, ...
    CourtyardPts{2}, ...
    {CourtyardPts{2}{1},FloorplanPts{2}{2},0}, ...
    FloorplanPts{3}};
strVert = BRCM_getVertices(PtsNew);
area=num2str(BRCM_getPolygonArea(PtsNew,'Z'));
BElines(3) = strcat(str(1),';',str(2),';',str(3),';',str(4),';',str(5),';',str(6),';',area,';',strVert,';');
%groundfloor surface 3/3
str = strsplit(BElines{4},';');
PtsNew = {CourtyardPts{1}, ...
    FloorplanPts{1}, ...
    FloorplanPts{2}, ...
    {CourtyardPts{2}{1},FloorplanPts{2}{2}, 0}};
strVert = BRCM_getVertices(PtsNew);
area=num2str(BRCM_getPolygonArea(PtsNew,'Z'));
BElines(4) = strcat(str(1),';',str(2),';',str(3),';',str(4),';',str(5),';',str(6),';',area,';',strVert,';');



for i=0:var7-1
    
    % ceiling building elements
    %ceiling surface 1/3
    index = i*11+5;
    str = strsplit(BElines{index},';');
    PtsNew = {{FloorplanPts{4}{1},FloorplanPts{4}{2},(i+1)*var6} ...
        {CourtyardPts{4}{1},CourtyardPts{4}{2},(i+1)*var6}, ...
        {CourtyardPts{3}{1},CourtyardPts{3}{2},(i+1)*var6}, ...
        {FloorplanPts{4}{1},CourtyardPts{3}{2},(i+1)*var6}};
    strVert = BRCM_getVertices(PtsNew);
    area=num2str(BRCM_getPolygonArea(PtsNew,'Z'));
    BElines(index) = strcat(str(1),';',str(2),';',str(3),';',str(4),';',str(5),';',str(6),';',area,';',strVert,';');
    
    %ceiling surface 2/3
    index = i*11+5+1;
    str = strsplit(BElines{index},';');
    PtsNew = {{FloorplanPts{4}{1}, CourtyardPts{3}{2}, (i+1)*var6}, ...
        {CourtyardPts{2}{1},CourtyardPts{2}{2},(i+1)*var6}, ...
        {CourtyardPts{2}{1},FloorplanPts{2}{2},(i+1)*var6}, ...
        {FloorplanPts{3}{1},FloorplanPts{3}{2},(i+1)*var6}};
    strVert = BRCM_getVertices(PtsNew);
    area=num2str(BRCM_getPolygonArea(PtsNew,'Z'));
    BElines(index) = strcat(str(1),';',str(2),';',str(3),';',str(4),';',str(5),';',str(6),';',area,';',strVert,';');
    
    %ceiling surface 3/3
    index = i*11+5+2;
    str = strsplit(BElines{index},';');
    PtsNew = {{CourtyardPts{1}{1},CourtyardPts{1}{2},(i+1)*var6}, ...
        {FloorplanPts{1}{1},FloorplanPts{1}{2},(i+1)*var6}, ...
        {FloorplanPts{2}{1},FloorplanPts{2}{2},(i+1)*var6}, ...
        {CourtyardPts{2}{1},FloorplanPts{2}{2}, (i+1)*var6}};
    strVert = BRCM_getVertices(PtsNew);
    area=num2str(BRCM_getPolygonArea(PtsNew,'Z'));
    BElines(index) = strcat(str(1),';',str(2),';',str(3),';',str(4),';',str(5),';',str(6),';',area,';',strVert,';');
    
    
    
    
    % wall  buildingelement 1/8
    index = i*11+5+3;
    PtsNew = {{FloorplanPts{3}{1},FloorplanPts{3}{2},FloorplanPts{3}{3}+((i)*var6)}, ...
        {FloorplanPts{2}{1},FloorplanPts{2}{2},FloorplanPts{2}{3}+((i)*var6)},...
        {FloorplanPts{2}{1}, FloorplanPts{2}{2}, FloorplanPts{2}{3}+((i+1)*var6)},...
        {FloorplanPts{3}{1}, FloorplanPts{3}{2}, FloorplanPts{3}{3}+((i+1)*var6)}};
    str = strsplit(BElines{index}, ';');
    strVert = BRCM_getVertices(PtsNew);
    area = BRCM_getPolygonArea(PtsNew,'Y');
    BElines(index) = strcat(str(1),';', str(2),';', str(3),';', str(4),';', str(5),';', str(6),';', num2str(area),';', strVert,';');   %wall east
    % wall  window 1/8
    index = i*8 +2; 
    str = strsplit(WINlines{index}, ';');
    area = area * var10;
    WINlines(index) = strcat(str(1),';',str(2),';',num2str(area),';0.0;',str(5),';',str(6),';');
    
    % wall north buildingelement 2/8
    index = i*11+5+4;
    PtsNew = {{FloorplanPts{2}{1},FloorplanPts{2}{2},FloorplanPts{2}{3}+((i)*var6)}, ...
        {FloorplanPts{1}{1},FloorplanPts{1}{2},FloorplanPts{1}{3}+((i)*var6)},...
        {FloorplanPts{1}{1}, FloorplanPts{1}{2}, FloorplanPts{1}{3}+((i+1)*var6)},...
        {FloorplanPts{2}{1}, FloorplanPts{2}{2}, FloorplanPts{2}{3}+((i+1)*var6)}};
    str = strsplit(BElines{index}, ';');
    strVert = BRCM_getVertices(PtsNew);
    area = BRCM_getPolygonArea(PtsNew,'X');
    BElines(index) = strcat(str(1),';', str(2),';', str(3),';', str(4),';', str(5),';', str(6),';', num2str(area),';', strVert,';');   %wall north
    % wall north window window 2/8
    index = i*8 +3; 
    str = strsplit(WINlines{index}, ';');
    area = area * var11;
    WINlines(index) = strcat(str(1),';',str(2),';',num2str(area),';0.0;',str(5),';',str(6),';');
    
    % wall west buildingelement 3/8
    index = i*11+5+5;
    PtsNew = {{FloorplanPts{1}{1},FloorplanPts{1}{2},FloorplanPts{1}{3}+((i)*var6)}, ...
        {CourtyardPts{1}{1},CourtyardPts{1}{2},CourtyardPts{1}{3}+((i)*var6)},...
        {CourtyardPts{1}{1}, CourtyardPts{1}{2}, CourtyardPts{1}{3}+((i+1)*var6)},...
        {FloorplanPts{1}{1}, FloorplanPts{1}{2}, FloorplanPts{1}{3}+((i+1)*var6)}};
    str = strsplit(BElines{index}, ';');
    strVert = BRCM_getVertices(PtsNew);
    area = BRCM_getPolygonArea(PtsNew,'Y');
    BElines(index) = strcat(str(1),';', str(2),';', str(3),';', str(4),';', str(5),';', str(6),';', num2str(area),';', strVert,';');   %wall west
    % wall north window 3/8
    index = i*8 +4; 
    str = strsplit(WINlines{index}, ';');
    area = area * var8;
    WINlines(index) = strcat(str(1),';',str(2),';',num2str(area),';0.0;',str(5),';',str(6),';');

    % wall south buildingelement 4/8
    index = i*11+5+6;
    PtsNew = {{CourtyardPts{1}{1},CourtyardPts{1}{2},CourtyardPts{1}{3}+((i)*var6)}, ...
        {CourtyardPts{2}{1},CourtyardPts{2}{2},CourtyardPts{2}{3}+((i)*var6)},...
        {CourtyardPts{2}{1}, CourtyardPts{2}{2}, CourtyardPts{2}{3}+((i+1)*var6)},...
        {CourtyardPts{1}{1}, CourtyardPts{1}{2}, CourtyardPts{1}{3}+((i+1)*var6)}};
    str = strsplit(BElines{index}, ';');
    strVert = BRCM_getVertices(PtsNew);
    area = BRCM_getPolygonArea(PtsNew,'X');
    BElines(index) = strcat(str(1),';', str(2),';', str(3),';', str(4),';', str(5),';', str(6),';', num2str(area),';', strVert,';');   %wall south
    % wall south window 4/8
    index = i*8 +5; 
    str = strsplit(WINlines{index}, ';');
    area = area * var9;
    WINlines(index) = strcat(str(1),';',str(2),';',num2str(area),';0.0;',str(5),';',str(6),';');
    
    
    

    % wall east buildingelement 5/8
    index = i*11+5+7;
    PtsNew = {{CourtyardPts{2}{1},CourtyardPts{2}{2},CourtyardPts{2}{3}+((i)*var6)}, ...
        {CourtyardPts{3}{1},CourtyardPts{3}{2},CourtyardPts{3}{3}+((i)*var6)},...
        {CourtyardPts{3}{1}, CourtyardPts{3}{2}, CourtyardPts{3}{3}+((i+1)*var6)},...
        {CourtyardPts{2}{1}, CourtyardPts{2}{2}, CourtyardPts{2}{3}+((i+1)*var6)}};
    str = strsplit(BElines{index}, ';');
    strVert = BRCM_getVertices(PtsNew);
    area = BRCM_getPolygonArea(PtsNew,'Y');
    BElines(index) = strcat(str(1),';', str(2),';', str(3),';', str(4),';', str(5),';', str(6),';', num2str(area),';', strVert,';');   %wall east
    % wall east window 5/8
    index = i*8 +6; 
    str = strsplit(WINlines{index}, ';');
    area = area * var8;
    WINlines(index) = strcat(str(1),';',str(2),';',num2str(area),';0.0;',str(5),';',str(6),';');
    
    % wall north buildingelement 6/8
    index = i*11+5+8;
    PtsNew = {{CourtyardPts{3}{1},CourtyardPts{3}{2},CourtyardPts{3}{3}+((i)*var6)}, ...
        {CourtyardPts{4}{1},CourtyardPts{4}{2},CourtyardPts{4}{3}+((i)*var6)},...
        {CourtyardPts{4}{1}, CourtyardPts{4}{2}, CourtyardPts{4}{3}+((i+1)*var6)},...
        {CourtyardPts{3}{1}, CourtyardPts{3}{2}, CourtyardPts{3}{3}+((i+1)*var6)}};
    str = strsplit(BElines{index}, ';');
    strVert = BRCM_getVertices(PtsNew);
    area = BRCM_getPolygonArea(PtsNew,'X');
    BElines(index) = strcat(str(1),';', str(2),';', str(3),';', str(4),';', str(5),';', str(6),';', num2str(area),';', strVert,';');   %wall north
    % wall north window window 6/8
    index = i*8 +7; 
    str = strsplit(WINlines{index}, ';');
    area = area * var11;
    WINlines(index) = strcat(str(1),';',str(2),';',num2str(area),';0.0;',str(5),';',str(6),';');
    
    % wall west buildingelement 7/8
    index = i*11+5+9;
    PtsNew = {{CourtyardPts{4}{1},CourtyardPts{4}{2},CourtyardPts{4}{3}+((i)*var6)}, ...
        {FloorplanPts{4}{1},FloorplanPts{4}{2},FloorplanPts{4}{3}+((i)*var6)},...
        {FloorplanPts{4}{1}, FloorplanPts{4}{2}, FloorplanPts{4}{3}+((i+1)*var6)},...
        {CourtyardPts{4}{1}, CourtyardPts{4}{2}, CourtyardPts{4}{3}+((i+1)*var6)}};
    str = strsplit(BElines{index}, ';');
    strVert = BRCM_getVertices(PtsNew);
    area = BRCM_getPolygonArea(PtsNew,'Y');
    BElines(index) = strcat(str(1),';', str(2),';', str(3),';', str(4),';', str(5),';', str(6),';', num2str(area),';', strVert,';');   %wall west
    % wall north window 7/8
    index = i*8 +8; 
    str = strsplit(WINlines{index}, ';');
    area = area * var8;
    WINlines(index) = strcat(str(1),';',str(2),';',num2str(area),';0.0;',str(5),';',str(6),';');

    % wall south buildingelement 8/8
    index = i*11+5+10;
    PtsNew = {{FloorplanPts{4}{1},FloorplanPts{4}{2},FloorplanPts{4}{3}+((i)*var6)}, ...
        {FloorplanPts{3}{1},FloorplanPts{3}{2},FloorplanPts{3}{3}+((i)*var6)},...
        {FloorplanPts{3}{1}, FloorplanPts{3}{2}, FloorplanPts{3}{3}+((i+1)*var6)},...
        {FloorplanPts{4}{1}, FloorplanPts{4}{2}, FloorplanPts{4}{3}+((i+1)*var6)}};
    str = strsplit(BElines{index}, ';');
    strVert = BRCM_getVertices(PtsNew);
    area = BRCM_getPolygonArea(PtsNew,'X');
    BElines(index) = strcat(str(1),';', str(2),';', str(3),';', str(4),';', str(5),';', str(6),';', num2str(area),';', strVert,';');   %wall south
    % wall south window 8/8
    index = i*8 +9; 
    str = strsplit(WINlines{index}, ';');
    area = area * var9;
    WINlines(index) = strcat(str(1),';',str(2),';',num2str(area),';0.0;',str(5),';',str(6),';');
    
end




end



%% LShape
function [BElines, WINlines] = BRCM_floorplan_LShape(BElines, WINlines, var6, var7, var8, var9, var10, var11, FloorplanPts, CourtyardPts)

%var(8)    -   Glazing S       [0.01,0.9]  Glazing Ratio on South Facades
% var(9)    -   Glazing E       [0.01,0.9]  Glazing Ratio on East Facades
% var(10)    -   Glazing N       [0.01,0.9]  Glazing Ratio on North Facades
% var(11)   -   Glazing W       [0.01,0.9]  Glazing Ratio on West Facades

% groundfloors
%groundfloor surface 1/2
str = strsplit(BElines{2},';');
PtsNew = {FloorplanPts{2}, ...
    CourtyardPts{2}, ...
     CourtyardPts{3}, ...
    {FloorplanPts{2}{1},CourtyardPts{3}{2},0}};
strVert = BRCM_getVertices(PtsNew);
area=num2str(BRCM_getPolygonArea(PtsNew,'Z'));
BElines(2) = strcat(str(1),';',str(2),';',str(3),';',str(4),';',str(5),';',str(6),';',area,';',strVert,';');
%groundfloor surface 2/2
str = strsplit(BElines{3},';');
PtsNew = {{FloorplanPts{2}{1},CourtyardPts{3}{2},0}, ...
    CourtyardPts{4}, ...
    FloorplanPts{4}, ...
    FloorplanPts{3}};
strVert = BRCM_getVertices(PtsNew);
area=num2str(BRCM_getPolygonArea(PtsNew,'Z'));
BElines(3) = strcat(str(1),';',str(2),';',str(3),';',str(4),';',str(5),';',str(6),';',area,';',strVert,';');


for i=0:var7-1
    
    % ceiling building elements
    %ceiling surface 1/2
    index = i*8+4;
    str = strsplit(BElines{index},';');
    PtsNew = {{FloorplanPts{2}{1},FloorplanPts{2}{2},(i+1)*var6} ...
        {CourtyardPts{2}{1},CourtyardPts{2}{2},(i+1)*var6}, ...
        {CourtyardPts{3}{1},CourtyardPts{3}{2},(i+1)*var6}, ...
        {FloorplanPts{2}{1},CourtyardPts{3}{2},(i+1)*var6}};
    strVert = BRCM_getVertices(PtsNew);
    area=num2str(BRCM_getPolygonArea(PtsNew,'Z'));
    BElines(index) = strcat(str(1),';',str(2),';',str(3),';',str(4),';',str(5),';',str(6),';',area,';',strVert,';');
    
    %ceiling surface 2/2
    index = i*8+4+1;
    str = strsplit(BElines{index},';');
    PtsNew = {{FloorplanPts{2}{1}, CourtyardPts{3}{2}, (i+1)*var6}, ...
        {CourtyardPts{4}{1},CourtyardPts{4}{2},(i+1)*var6}, ...
        {FloorplanPts{4}{1},FloorplanPts{4}{2},(i+1)*var6}, ...
        {FloorplanPts{3}{1},FloorplanPts{3}{2},(i+1)*var6}};
    strVert = BRCM_getVertices(PtsNew);
    area=num2str(BRCM_getPolygonArea(PtsNew,'Z'));
    BElines(index) = strcat(str(1),';',str(2),';',str(3),';',str(4),';',str(5),';',str(6),';',area,';',strVert,';');
    
    
    
    
    % wall  buildingelement 1/6
    index = i*8+4+2;
    PtsNew = {{CourtyardPts{4}{1},CourtyardPts{4}{2},CourtyardPts{4}{3}+((i)*var6)}, ...
        {FloorplanPts{4}{1},FloorplanPts{4}{2},FloorplanPts{4}{3}+((i)*var6)},...
        {FloorplanPts{4}{1},FloorplanPts{4}{2},FloorplanPts{4}{3}+((i+1)*var6)},...
        {CourtyardPts{4}{1},CourtyardPts{4}{2},CourtyardPts{4}{3}+((i+1)*var6)}};
    str = strsplit(BElines{index}, ';');
    strVert = BRCM_getVertices(PtsNew);
    area = BRCM_getPolygonArea(PtsNew,'X');
    BElines(index) = strcat(str(1),';', str(2),';', str(3),';', str(4),';', str(5),';', str(6),';', num2str(area),';', strVert,';');   %wall east
    % wall  window 1/6
    index = i*6 +2; 
    str = strsplit(WINlines{index}, ';');
    area = area * var9;
    WINlines(index) = strcat(str(1),';',str(2),';',num2str(area),';0.0;',str(5),';',str(6),';');
    
    % wall north buildingelement 2/6
     index = i*8+4+3;
    PtsNew = {{FloorplanPts{4}{1},FloorplanPts{4}{2},FloorplanPts{4}{3}+((i)*var6)}, ...
        {FloorplanPts{3}{1},FloorplanPts{3}{2},FloorplanPts{3}{3}+((i)*var6)},...
        {FloorplanPts{3}{1}, FloorplanPts{3}{2}, FloorplanPts{3}{3}+((i+1)*var6)},...
        {FloorplanPts{4}{1},FloorplanPts{4}{2},FloorplanPts{4}{3}+((i+1)*var6)}};
    str = strsplit(BElines{index}, ';');
    strVert = BRCM_getVertices(PtsNew);
    area = BRCM_getPolygonArea(PtsNew,'Y');
    BElines(index) = strcat(str(1),';', str(2),';', str(3),';', str(4),';', str(5),';', str(6),';', num2str(area),';', strVert,';');   %wall east
    % wall  window 2/6
    index = i*6 +3; 
    str = strsplit(WINlines{index}, ';');
    area = area * var10;
    WINlines(index) = strcat(str(1),';',str(2),';',num2str(area),';0.0;',str(5),';',str(6),';');
    
   % wall north buildingelement 3/6
     index = i*8+4+4;
    PtsNew = {{FloorplanPts{3}{1},FloorplanPts{3}{2},FloorplanPts{3}{3}+((i)*var6)}, ...
        {FloorplanPts{2}{1},FloorplanPts{2}{2},FloorplanPts{2}{3}+((i)*var6)},...
        {FloorplanPts{2}{1}, FloorplanPts{2}{2}, FloorplanPts{2}{3}+((i+1)*var6)},...
        {FloorplanPts{3}{1}, FloorplanPts{3}{2}, FloorplanPts{3}{3}+((i+1)*var6)}};
    str = strsplit(BElines{index}, ';');
    strVert = BRCM_getVertices(PtsNew);
    area = BRCM_getPolygonArea(PtsNew,'X');
    BElines(index) = strcat(str(1),';', str(2),';', str(3),';', str(4),';', str(5),';', str(6),';', num2str(area),';', strVert,';');   %wall east
    % wall  window 3/6
    index = i*6 +4; 
    str = strsplit(WINlines{index}, ';');
    area = area * var11;
    WINlines(index) = strcat(str(1),';',str(2),';',num2str(area),';0.0;',str(5),';',str(6),';');

    % wall north buildingelement 4/6
     index = i*8+4+5;
    PtsNew = {{FloorplanPts{2}{1},FloorplanPts{2}{2},FloorplanPts{2}{3}+((i)*var6)}, ...
        {CourtyardPts{2}{1},CourtyardPts{2}{2},CourtyardPts{2}{3}+((i)*var6)},...
        {CourtyardPts{2}{1}, CourtyardPts{2}{2}, CourtyardPts{2}{3}+((i+1)*var6)},...
        {FloorplanPts{2}{1},FloorplanPts{2}{2},FloorplanPts{2}{3}+((i+1)*var6)}};
    str = strsplit(BElines{index}, ';');
    strVert = BRCM_getVertices(PtsNew);
    area = BRCM_getPolygonArea(PtsNew,'Y');
    BElines(index) = strcat(str(1),';', str(2),';', str(3),';', str(4),';', str(5),';', str(6),';', num2str(area),';', strVert,';');   %wall east
    % wall  window 4/6
    index = i*6 +5; 
    str = strsplit(WINlines{index}, ';');
    area = area * var8;
    WINlines(index) = strcat(str(1),';',str(2),';',num2str(area),';0.0;',str(5),';',str(6),';');
    
    
    
   % wall north buildingelement 5/6
     index = i*8+4+6;
    PtsNew = {{CourtyardPts{2}{1},CourtyardPts{2}{2},CourtyardPts{2}{3}+((i)*var6)}, ...
        {CourtyardPts{3}{1},CourtyardPts{3}{2},CourtyardPts{3}{3}+((i)*var6)},...
        {CourtyardPts{3}{1}, CourtyardPts{3}{2}, CourtyardPts{3}{3}+((i+1)*var6)},...
        {CourtyardPts{2}{1}, CourtyardPts{2}{2}, CourtyardPts{2}{3}+((i+1)*var6)}};
    str = strsplit(BElines{index}, ';');
    strVert = BRCM_getVertices(PtsNew);
    area = BRCM_getPolygonArea(PtsNew,'X');
    BElines(index) = strcat(str(1),';', str(2),';', str(3),';', str(4),';', str(5),';', str(6),';', num2str(area),';', strVert,';');   %wall east
    % wall  window 5/6
    index = i*6 +6; 
    str = strsplit(WINlines{index}, ';');
    area = area * var9;
    WINlines(index) = strcat(str(1),';',str(2),';',num2str(area),';0.0;',str(5),';',str(6),';');
    
    % wall north buildingelement 6/6
     index = i*8+4+7;
    PtsNew = {{CourtyardPts{3}{1},CourtyardPts{3}{2},CourtyardPts{3}{3}+((i)*var6)}, ...
        {CourtyardPts{4}{1},CourtyardPts{4}{2},CourtyardPts{4}{3}+((i)*var6)},...
        {CourtyardPts{4}{1}, CourtyardPts{4}{2}, CourtyardPts{4}{3}+((i+1)*var6)},...
        {CourtyardPts{3}{1}, CourtyardPts{3}{2}, CourtyardPts{3}{3}+((i+1)*var6)}};
    str = strsplit(BElines{index}, ';');
    strVert = BRCM_getVertices(PtsNew);
    area = BRCM_getPolygonArea(PtsNew,'Y');
    BElines(index) = strcat(str(1),';', str(2),';', str(3),';', str(4),';', str(5),';', str(6),';', num2str(area),';', strVert,';');   %wall east
    % wall  window 6/6
    index = i*6 +7; 
    str = strsplit(WINlines{index}, ';');
    area = area * var8;
    WINlines(index) = strcat(str(1),';',str(2),';',num2str(area),';0.0;',str(5),';',str(6),';');
    
end





end