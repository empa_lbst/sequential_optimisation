function [RotateBldn, ThermalModelType, EHFModelType, var] = BRCM_TypologySelector(var)
% case selector for typology: B1, .B2, .B3, .B4.... depending on control-point variable!
% 
% 
% 
%
% 
%           o           control point very far away (z). therefore small floorplan
%           .
%           .
%           __
%          /_/
%

%           o           distance >= 1 z   makes it block. also, the further (z max:1) the smaller the floorplan (x/y)
%         ______
%        /     /
%       /_____/
%

%          __ o ___       distance < 1 z   makes it courtyard, and makes floorplan bigger in x/y
%         /  __   /
%        /  /_/  /
%       /_______/

%         ___________      if control point distance to center > 0.5 then it becomes a U-Shape
%        /          /
%       /    o     /
%      /  _____   /
%     /__/    /__/


%          ____________     if control point also moves in 2nd direction (can be either y or x) > 0.5 away from the center, then it becomes a L-Shape
%         /           /
%        /   ________/
%       /   /     o
%      /___/





% var(8)    -   Glazing S       [0.01,0.9]  Glazing Ratio on South Facades
% var(9)    -   Glazing E       [0.01,0.9]  Glazing Ratio on East Facades
% var(10)    -   Glazing N       [0.01,0.9]  Glazing Ratio on North Facades
% var(11)   -   Glazing W       [0.01,0.9]  Glazing Ratio on West Facades

RotateBldn=0;
if (abs(var(1)) > 0.5 && abs(var(2)) <= 0.5 && var(3) < 1) || (abs(var(2)) > 0.5 && abs(var(1)) <= 0.5 && var(3) < 1)
    %     B = get_input.B3;   % U shape, typ 3
    ThermalModelType = 'ThermalModel3';
    EHFModelType = 'EHFM3'; % U-Shape
    if abs(var(1)) > 0.5 && abs(var(2)) <= 0.5  % ]   [
        if var(1) > 0    % rotate -90�
            RotateBldn = -90;           
            var = [var(1) var(2) var(3) var(4) var(5) var(6) var(7) var(9) var(10) var(11) var(8) var(12) var(13) var(14) var(15) var(16)]; % change window parameters to correct orientations
        else % var(1) negative. rotate +90�
            RotateBldn = 90;
            var = [var(1) var(2) var(3) var(4) var(5) var(6) var(7) var(11) var(8) var(9) var(10) var(12) var(13) var(14) var(15) var(16)]; % change window parameters to correct orientations
        end
    elseif abs(var(1)) <= 0.5 && abs(var(2)) > 0.5      % U  A
        if var(2) > 0   % rotate 180� 
            RotateBldn = 180;
            var = [var(1) var(2) var(3) var(4) var(5) var(6) var(7) var(10) var(11) var(8) var(9) var(12) var(13) var(14) var(15) var(16)]; % change window parameters to correct orientations
        else    % no rotation
            RotateBldn = 0;
        end
    end
elseif abs(var(1)) > 0.5 && abs(var(2)) > 0.5  && var(3) < 1    % .-
    %     B = get_input.B4;   % L shape, typ 4
    ThermalModelType = 'ThermalModel4'; %L-Shape
    EHFModelType = 'EHFM4';
    if var(1) > 0 && var(2) < 0
        RotateBldn = 0;
    elseif var(1) < 0 && var(2) < 0 %flip West and East .- -.
        RotateBldn = 9999;
        var = [var(1) var(2) var(3) var(4) var(5) var(6) var(7) var(8) var(11) var(10) var(9) var(12) var(13) var(14) var(15) var(16)]; % change window parameters to correct orientations
    elseif var(1) < 0 && var(2) > 0 % .-  _.
        RotateBldn = 180;
        var = [var(1) var(2) var(3) var(4) var(5) var(6) var(7) var(10) var(11) var(8) var(9) var(12) var(13) var(14) var(15) var(16)]; % change window parameters to correct orientations
    elseif var(1) > 0 && var(2) > 0 % flip north south  .-  ._
        RotateBldn = 8888;
        var = [var(1) var(2) var(3) var(4) var(5) var(6) var(7) var(10) var(9) var(8) var(11) var(12) var(13) var(14) var(15) var(16)]; % change window parameters to correct orientations
    end
elseif abs(var(1)) <= 0.5 && abs(var(2)) <= 0.5 && var(3) < 1
    %     B = get_input.B2;   % courtyard, typ 2
    ThermalModelType = 'ThermalModel2';
    EHFModelType = 'EHFM2';
else %if var(3) >= 1
    %     B = get_input.B1;   % block, typ 1
    ThermalModelType = 'ThermalModel1';
    EHFModelType = 'EHFM1';
end






end