function BRCM_newcsv_buildinghull(outpath, lines, Typology, Storeys)

outfile=outpath;




switch Typology
    case 'EHFM1'
        newlines=Typ1(lines, Storeys);
    case 'EHFM2'
        newlines=Typ2(lines, Storeys);
    case 'EHFM3'
        newlines=Typ3(lines, Storeys);
    case 'EHFM4'
        newlines=Typ4(lines, Storeys);
end







fid = fopen(outfile, 'w');
for i=1:length(newlines)
    fprintf(fid, '%s \r\n',newlines{i});
end
fclose(fid);
fclose('all');

end




%% Block
function newlines=Typ1(lines, Storeys)

%within start and end line, delete everything.
%but for facade, add roof disturbances
facade_StartLine = 2;
facade_EndLine = Storeys*4+1;
blinds_StartLine = 47;
blinds_EndLine = Storeys*4+46;
zones_StartLine = 90;
zones_EndLine = Storeys+90-1;


roof_BE1 = Storeys*5-3;

if roof_BE1 <10
    strRoof_BE1 = strcat('B000',num2str(roof_BE1));
else
    strRoof_BE1 = strcat('B00',num2str(roof_BE1));
end

newlines = [];

for i=1:facade_EndLine
    newlines{i} = lines{i};
end
newlines{end+1} = strcat('NaN;',strRoof_BE1,';Roof;0.6;NaN;');
newlines{end+1} = 'NaN;NaN;NaN;NaN;NaN;';

for i=blinds_StartLine-1:blinds_EndLine
    newlines{end+1} = lines{i};
end
newlines{end+1} = 'NaN;NaN;NaN;NaN;NaN;';
for i=zones_StartLine-1:zones_EndLine
    newlines{end+1} = lines{i};
end




end




%% Courtyard
function newlines=Typ2(lines, Storeys)

%within start and end line, delete everything.
%but for facade, add roof disturbances
facade_StartLine = 2;
facade_EndLine = Storeys*8+1;
blinds_StartLine = 95;
blinds_EndLine = Storeys*8+94;
zones_StartLine = 184;
zones_EndLine = Storeys+184-1;


roof_BE = [Storeys*14-7 Storeys*14-6 Storeys*14-5 Storeys*14-4 Storeys*14-3 Storeys*14-2];
strRoof_BE = {};
for i=1:size(roof_BE,2)
    if roof_BE(i) < 10
        strRoof_BE{i} = strcat('B000',num2str(roof_BE(i)));
    elseif roof_BE(i) >= 10 && roof_BE(i) < 100
        strRoof_BE{i} = strcat('B00',num2str(roof_BE(i)));
    else
        strRoof_BE{i} = strcat('B0',num2str(roof_BE(i)));
    end
end


newlines = [];

for i=1:facade_EndLine
    newlines{i} = lines{i};
end
for i=1:size(roof_BE,2)
    newlines{end+1} = strcat('NaN;',strRoof_BE{i},';Roof;0.6;NaN;');
end

newlines{end+1} = 'NaN;NaN;NaN;NaN;NaN;';

for i=blinds_StartLine-1:blinds_EndLine
    newlines{end+1} = lines{i};
end
newlines{end+1} = 'NaN;NaN;NaN;NaN;NaN;';
for i=zones_StartLine-1:zones_EndLine
    newlines{end+1} = lines{i};
end



end




%% U-Shape
function newlines=Typ3(lines, Storeys)

%within start and end line, delete everything.
%but for facade, add roof disturbances
facade_StartLine = 2;
facade_EndLine = Storeys*8+1;
blinds_StartLine = 95;
blinds_EndLine = Storeys*8+94;
zones_StartLine = 184;
zones_EndLine = Storeys+184-1;


roof_BE1 = Storeys*11-4-3;
roof_BE2 = Storeys*11-4-2;
roof_BE3 = Storeys*11-4-1;
if roof_BE1 < 10
    strRoof_BE1 = strcat('B000',num2str(roof_BE1));
elseif roof_BE1 >= 10 && roof_BE1 < 100
    strRoof_BE1 = strcat('B00',num2str(roof_BE1));
else
    strRoof_BE1 = strcat('B0',num2str(roof_BE1));
end
if roof_BE2< 10
    strRoof_BE2 = strcat('B000',num2str(roof_BE2));
elseif roof_BE2 >= 10 && roof_BE2 < 100
    strRoof_BE2 = strcat('B00',num2str(roof_BE2));
else roof_BE2 >= 100
    strRoof_BE2 = strcat('B0',num2str(roof_BE2));
end
if roof_BE3 < 10
    strRoof_BE3 = strcat('B000',num2str(roof_BE3));
elseif roof_BE3 >= 10 && roof_BE3 < 100
    strRoof_BE3 = strcat('B00',num2str(roof_BE3));
else roof_BE3 >= 100
    strRoof_BE3 = strcat('B0',num2str(roof_BE3));
end

newlines = [];

for i=1:facade_EndLine
    newlines{i} = lines{i};
end
newlines{end+1} = strcat('NaN;',strRoof_BE1,';Roof;0.6;NaN;');
newlines{end+1} = strcat('NaN;',strRoof_BE2,';Roof;0.6;NaN;');
newlines{end+1} = strcat('NaN;',strRoof_BE3,';Roof;0.6;NaN;');
newlines{end+1} = 'NaN;NaN;NaN;NaN;NaN;';

for i=blinds_StartLine-1:blinds_EndLine
    newlines{end+1} = lines{i};
end
newlines{end+1} = 'NaN;NaN;NaN;NaN;NaN;';
for i=zones_StartLine-1:zones_EndLine
    newlines{end+1} = lines{i};
end



end





%% L-Shape
function newlines=Typ4(lines, Storeys)

%within start and end line, delete everything.
%but for facade, add roof disturbances
facade_StartLine = 2;
facade_EndLine = Storeys*6+1;
blinds_StartLine = 71;
blinds_EndLine = Storeys*6+70;
zones_StartLine = 137;
zones_EndLine = Storeys+137-1;


roof_BE1 = Storeys*8-3-2;
roof_BE2 = Storeys*8-3-1;
if roof_BE1 <10
    strRoof_BE1 = strcat('B000',num2str(roof_BE1));
else
    strRoof_BE1 = strcat('B00',num2str(roof_BE1));
end
if roof_BE2 < 10
    strRoof_BE2 = strcat('B000',num2str(roof_BE2));
else
    strRoof_BE2 = strcat('B00',num2str(roof_BE2));
end

newlines = [];

for i=1:facade_EndLine
    newlines{i} = lines{i};
end
newlines{end+1} = strcat('NaN;',strRoof_BE1,';Roof;0.6;NaN;');
newlines{end+1} = strcat('NaN;',strRoof_BE2,';Roof;0.6;NaN;');
newlines{end+1} = 'NaN;NaN;NaN;NaN;NaN;';

for i=blinds_StartLine-1:blinds_EndLine
    newlines{end+1} = lines{i};
end
newlines{end+1} = 'NaN;NaN;NaN;NaN;NaN;';
for i=zones_StartLine-1:zones_EndLine
    newlines{end+1} = lines{i};
end


end
