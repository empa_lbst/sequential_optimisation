function [RotateBldn, ThermalModelType, EHFModelType] = BRCM_build(var, get_input)


% Input:
% var:          decision variables, for model generation
% generation:   integer. just dummy, so optimisers work
% job:          integer. just dummy, so optimisers work
% get_input:    structure. contais all data and information necessary for this evaluation function (e.g. weather data...)
%
%
% Variables:
% var(1)    -   Control Point X [-1,1]      changing typology and floorplan size
% var(2)    -   Control Point Y [-1,1]      changing typology and floorplan size
% var(3)    -   Control Point Z [0,2]       changing typology and floorplan size
% var(4)    -   Orientation     [-30,30]    orientation of the building, in degree deviation from North (0�)
% var(5)    -   Length/Width    [-1,1]      Length/Width Ratio. stretching east/west (1) or north/south (-1)
% var(6)    -   Ceiling Height  [3,5]       Ceiling height per floor, in meters
% var(7)    -   No. of storeys  [1,10]      Number of storeys.
% var(8)    -   Glazing S       [0.01,0.9]  Glazing Ratio on South Facades
% var(9)    -   Glazing E       [0.01,0.9]  Glazing Ratio on East Facades
% var(10)   -   Glazing N       [0.01,0.9]  Glazing Ratio on North Facades
% var(11)   -   Glazing W       [0.01,0.9]  Glazing Ratio on West Facades
% var(12)   -   Walls Constr.   [1,2]       Construction type for Walls (light heavy)
% var(13)   -   Floors Constr.  [1,2]       Construction type for Floors (light heavy)
% var(14)   -   Roof Constr.    [1,2]       Construction type for Roof (light heavy)
% var(15)   -   Glazing Type    [1,3]       Glazing Type windows (WSG 2, WSG 3, SSG 2)
% var(16)   -   Insulation      [50,300]    Insulation thickness for walls in mm. Same insulation material always


%        (1)    (2)    (3)    (4)     (5)     (6)   (7)     (8)     (9)    (10)     (11)    (12)   (13)     (14)    (15)   (16)
% var = [-1  	0.0   	0.4     0       -1       5     6       0.9     0.9     0.1     0.1     2       2       2       3       300];



%% variables and inputs -------------------------------------------------------------------------------------------
% -----------------------------------------------------------------------------------------------------------------
% orientation = var(4);
% disturbances=get_input.disturbances;
BRCMfilepath=get_input.BRCMfilepath;


%% Typology selector    -------------------------------------------------------------------------------------------
% -----------------------------------------------------------------------------------------------------------------
% Block, Courtyard, U-Shape, L-Shape
% also rotates / flips building

[RotateBldn, ThermalModelType, EHFModelType, var] = BRCM_TypologySelector(var);





%% Load Data of chosen Typology -----------------------------------------------------------------------------------
% -----------------------------------------------------------------------------------------------------------------

ORIGbuildingelements    = BRCM_loadcsv('\ORIGINAL\buildingelements.csv',ThermalModelType,BRCMfilepath);
ORIGconstructions       = BRCM_loadcsv('\ORIGINAL\constructions.csv',ThermalModelType,BRCMfilepath);
ORIGmaterials           = BRCM_loadcsv('\ORIGINAL\materials.csv',ThermalModelType,BRCMfilepath); %!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
ORIGnomassconstructions = BRCM_loadcsv('\ORIGINAL\nomassconstructions.csv',ThermalModelType,BRCMfilepath); %!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
ORIGparameters          = BRCM_loadcsv('\ORIGINAL\parameters.csv',ThermalModelType,BRCMfilepath);
ORIGwindows             = BRCM_loadcsv('\ORIGINAL\windows.csv',ThermalModelType,BRCMfilepath);
ORIGzones               = BRCM_loadcsv('\ORIGINAL\zones.csv',ThermalModelType,BRCMfilepath);

ORIGbuildinghull        = BRCM_loadcsv('\ORIGINAL\buildinghull.csv', EHFModelType, BRCMfilepath);
ORIGinternalgains       = BRCM_loadcsv('\ORIGINAL\internalgains.csv',EHFModelType,BRCMfilepath);
ORIGradiators           = BRCM_loadcsv('\ORIGINAL\radiators.csv',EHFModelType,BRCMfilepath);





%% Geometry: No. of storeys ---------------------------------------------------------------------------------------
% -----------------------------------------------------------------------------------------------------------------
% var(7)    -   No. of storeys  [1,10]      Number of storeys.

% ThermalModel: buildingelements , windows, zones
strToFind={};
strToFindSub={};
replaceString={};
replacePosition=[];
if var(7)~=10
    strToFindSub{1}=strcat('Block',num2str(var(7)));  % this is to find ceilings, which shouldn't disappear (NaN) but be transformed to roofs
    for i=(var(7)+1):10
        strToFind{i-var(7)}=strcat('Block',num2str(i));
        replaceString{i-var(7)}='NaN';
        replacePosition(i-var(7))=999;
    end
end
BRCM_newcsv_Storeys(strcat(BRCMfilepath,'\',ThermalModelType,'\buildingelements.csv'), ORIGbuildingelements, strToFind, replaceString, replacePosition, strToFindSub);
BRCM_newcsv(strcat(BRCMfilepath,'\',ThermalModelType,'\windows.csv'), ORIGwindows, strToFind, replaceString, replacePosition);
BRCM_newcsv(strcat(BRCMfilepath,'\',ThermalModelType,'\zones.csv'), ORIGzones, strToFind, replaceString, replacePosition);



% EHFM: internalgains, radiators (1 zone per storey)
strToFind={};
replaceString={};
replacePosition=[];
if var(7)~=10
    for i=(var(7)+1):9
        strToFind{i-var(7)}=strcat('Z000',num2str(i));
        replaceString{i-var(7)}='NaN';
        replacePosition(i-var(7))=999;
    end
    strToFind{10-var(7)} = 'Z0010';
    replaceString{10-var(7)} = 'NaN';
    replacePosition(10-var(7)) = 999;
end
BRCM_newcsv(strcat(BRCMfilepath,'\',EHFModelType,'\internalgains.csv'), ORIGinternalgains, strToFind, replaceString, replacePosition);
BRCM_newcsv(strcat(BRCMfilepath,'\',EHFModelType,'\radiators.csv'), ORIGradiators, strToFind, replaceString, replacePosition);


% buildinghull
BRCM_newcsv_buildinghull(strcat(BRCMfilepath,'\',EHFModelType,'\buildinghull.csv'), ORIGbuildinghull, EHFModelType, var(7));






%% Update Data       ----------------------------------------------------------------------------------------------
% -----------------------------------------------------------------------------------------------------------------

NEWbuildingelementsTemp    = BRCM_loadcsv('\buildingelements.csv',ThermalModelType,BRCMfilepath);
NEWbuildingelements = [];
i=1;
breakit = false;
% while isempty(strfind(NEWbuildingelementsTemp{i}, 'NaN;NaN;NaN;NaN;NaN;NaN;NaN;NaN;')) == true
while breakit == false    
    NEWbuildingelements{end+1} = NEWbuildingelementsTemp{i};
    i=i+1;
    if i-1 == size(NEWbuildingelementsTemp,2)
       breakit = true; 
    end
    if i <= size(NEWbuildingelementsTemp,2)
       if  strfind(NEWbuildingelementsTemp{i}, 'NaN;NaN;NaN;NaN;NaN;NaN;NaN;NaN;') == true
          breakit = true;  
       end
    end
end


NEWwindowsTemp = BRCM_loadcsv('\windows.csv',ThermalModelType,BRCMfilepath);
NEWwindows = [];
i=1;
breakit = false;
% while isempty(strfind(NEWwindowsTemp{i}, 'NaN;NaN;NaN;NaN;NaN;NaN;')) == true
while breakit == false 
    NEWwindows{end+1} = NEWwindowsTemp{i};
    i=i+1;
    if i-1 == size(NEWwindowsTemp,2)
       breakit = true; 
    end
    if i <= size(NEWwindowsTemp,2)
       if  strfind(NEWwindowsTemp{i}, 'NaN;NaN;NaN;NaN;NaN;NaN;') == true
          breakit = true;  
       end
    end
end

NEWzonesTemp = BRCM_loadcsv('\zones.csv',ThermalModelType,BRCMfilepath);
NEWzones = [];
i=1;
breakit = false;
% while isempty(strfind(NEWzonesTemp{i}, 'NaN;NaN;NaN;NaN;NaN;')) == true
while breakit == false 
       NEWzones{end+1} = NEWzonesTemp{i};
    i=i+1;
    if i-1 == size(NEWzonesTemp,2)
       breakit = true; 
    end
    if i <= size(NEWzonesTemp,2)
       if  strfind(NEWzonesTemp{i}, 'NaN;NaN;NaN;NaN;NaN;') == true
          breakit = true;  
       end
    end
end

NEWinternalgainsTemp = BRCM_loadcsv('\internalgains.csv',EHFModelType,BRCMfilepath);
NEWinternalgains = [];
NEWradiatorsTemp = BRCM_loadcsv('\radiators.csv',EHFModelType,BRCMfilepath);
NEWradiators = [];
i=1;
breakit = false;
% while isempty(strfind(NEWinternalgainsTemp{i}, 'NaN;NaN;')) == true
while breakit == false 
    NEWinternalgains{end+1} = NEWinternalgainsTemp{i};
    NEWradiators{end+1} = NEWradiatorsTemp{i};
    i=i+1;
    if i-1 == size(NEWinternalgainsTemp,2)
        breakit = true;
    end
    if i <= size(NEWinternalgainsTemp,2)
        if  strfind(NEWinternalgainsTemp{i}, 'NaN;NaN;') == true
            breakit = true;
        end
    end
end





%% Geometry: Control Point modification ---------------------------------------------------------------------------
%  Geometry: Ceiling Height modification  -------------------------------------------------------------------------
%  Geometry: Width/Length modification  ---------------------------------------------------------------------------
% Geometry: Glazing Ratio modification  ---------------------------------------------------------------------------
% -----------------------------------------------------------------------------------------------------------------
% courtyard and block typology: just take control point directly
% L shape: use control point only for y < 0 and x > 0..... translate coordinates of control point to this domain
% U shape: use control point only for y < 0. keep x as it is, for -1 to 1

% var(1)    -   Control Point X [-1,1]      changing typology and floorplan size
% var(2)    -   Control Point Y [-1,1]      changing typology and floorplan size
% var(3)    -   Control Point Z [0,2]       changing typology and floorplan size
% var(5)    -   Length/Width    [-1,1]      Length/Width Ratio. stretching east/west (-1) or north/south (1)
% var(6)    -   Ceiling Height  [3,5]       Ceiling height per floor, in meters
% var(7)    -   No. of storeys  [1,10]      Number of storeys.
% var(8)    -   Glazing S       [0.01,0.9]  Glazing Ratio on South Facades
% var(9)    -   Glazing E       [0.01,0.9]  Glazing Ratio on East Facades
% var(10)    -   Glazing N       [0.01,0.9]  Glazing Ratio on North Facades
% var(11)   -   Glazing W       [0.01,0.9]  Glazing Ratio on West Facades


% overwrite \buildingelements.csv with NEWbuildingelements
BRCM_CreateFloorplan(var, NEWbuildingelements, NEWwindows, ThermalModelType, BRCMfilepath);

NEWbuildingelements    = BRCM_loadcsv('\buildingelements.csv',ThermalModelType,BRCMfilepath);


%% var 12 - 16: constructions and materials modification ----------------------------------------------------------
% -----------------------------------------------------------------------------------------------------------------
% var(12)   -   Walls Constr.   [1,2]       Construction type for Walls (light heavy)
% var(13)   -   Floors Constr.  [1,2]       Construction type for Floors (light heavy)
% var(14)   -   Roof Constr.    [1,2]       Construction type for Roof (light heavy)
if var(12)==1
    ConstrWall = 'C0003';
else
    ConstrWall = 'C0004';
end
if var(13)==1
    ConstrGroundFloor = 'C0001';
    ConstrCeiling = 'C0010';
else
    ConstrGroundFloor = 'C0002';
    ConstrCeiling = 'C0008';
end
if var(14) == 1
    ConstrRoof = 'C0005';
else
    ConstrRoof = 'C0006';
end
strToFind = {'Wall','GroundFloor','Ceiling','Roof'};
replaceString = {ConstrWall,ConstrGroundFloor,ConstrCeiling,ConstrRoof};
replacePosition = [3 3 3 3];
BRCM_newcsv(strcat(BRCMfilepath,'\',ThermalModelType,'\buildingelements.csv'), NEWbuildingelements, strToFind, replaceString, replacePosition);


% var(15)   -   Glazing Type    [1,3]       Glazing Type windows (WSG 2, SSG 2, SSG 3)
if var(15) == 1
    ConstrGValueWin = '0.761';
    ConstrUValueWin = '2.5';
elseif var(15) == 2
    ConstrGValueWin = '0.595';
    ConstrUValueWin = '1.5';
else
    ConstrGValueWin = '0.47';
    ConstrUValueWin = '0.8';
end
strToFind = {'GValue_Window','UValue_Window'};
replaceString = {ConstrGValueWin,ConstrUValueWin};
replacePosition = [3 3];
BRCM_newcsv(strcat(BRCMfilepath,'\',ThermalModelType,'\parameters.csv'), ORIGparameters, strToFind, replaceString, replacePosition);


% var(16)   -   Insulation      [50,300]    Insulation thickness for walls in mm. Same insulation material always
strToFind = {'C0003', 'C0004'};
replaceString = {strcat('0.006,',num2str(0.128+(var(16)/1000)),',0.013'), strcat('0.105,',num2str(0.1175+(var(16)/1000)),',0.1,0.013')};
replacePosition = [4 4];
BRCM_newcsv(strcat(BRCMfilepath,'\',ThermalModelType,'\constructions.csv'), ORIGconstructions, strToFind, replaceString, replacePosition);




%% copy materials.csv and nomassconstruction.csv files   ----------------------------------------------------------
% -----------------------------------------------------------------------------------------------------------------
strToFind={};
replaceString={};
replacePosition=[];
BRCM_newcsv(strcat(BRCMfilepath,'\',ThermalModelType,'\materials.csv'), ORIGmaterials, strToFind, replaceString, replacePosition);
BRCM_newcsv(strcat(BRCMfilepath,'\',ThermalModelType,'\nomassconstructions.csv'), ORIGnomassconstructions, strToFind, replaceString, replacePosition);




end