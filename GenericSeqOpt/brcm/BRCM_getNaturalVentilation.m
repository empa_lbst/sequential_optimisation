
function Qt = BRCM_getNaturalVentilation(tempIn, tempOut, WindSpeed, WindDirection, B, typology, window_height)

% This function outputs the ventilation rate for that temperature difference and windspeed*/
% Ventilation=Vdesign * Fschedule * (A + B*|(Tzone-Todb)| + C*WindSpd + D * WindSpd**2)   */

%we suppose the wind is always going perpendiculat to the main facade and there are four
%windows of 1 meter around each wall*/
%
%                Facade 2
%             ------------
%             |          |
%     Facade 3|          | Facade 4
%             |          |
%             |          |
%             ------------
%                  /M\  Facade 1
%                   |
%                   |  Wind
%
%
Cw = 0.025;
Cd = 0.6;

switch typology
    case 1  %block
        grndBE = 1;
    case 2  %courtyard
        grndBE = 6;
    case 3  %U-shape
        grndBE = 3;
    case 4  %L-shape
        grndBE = 2;
end

NrZones=length(B.thermal_model_data.zones);
NrWindows=length(B.thermal_model_data.windows);
NrWinPerZone = NrWindows/NrZones;
NrBEperZone = (length(B.thermal_model_data.building_elements)-grndBE) / NrZones;

Qt=zeros(NrZones,1);




for i=1:1:NrZones

%     'i'
%     i
    if tempIn(i) > tempOut      % only ventilate, when its colder outside
        
        % DO THIS ONLY ONE TIME!!!
        % CREATE TABLE / MATRIX WITH DIRECTIONDIFFERENCE... ONLY FOR ONE FLOOR
        % OTHER FLOORS ARE IDENTICAL
        
        % THEN, Only iterate for stuff which changes: tempIn & windspeed & wind direction
        
        for k=1:1:NrBEperZone
            kk=NrBEperZone*(i-1)+k+grndBE;
%             kk
            if strcmp(B.thermal_model_data.building_elements(1, kk).window_identifier,'') ~= 1
                
                %go through 3 vertices of wall and calculate surface normal -> E,W,S,N? important to know for wind direction
                p1=[B.thermal_model_data.building_elements(1, kk).vertices(1).x ...
                    B.thermal_model_data.building_elements(1, kk).vertices(1).y ...
                    B.thermal_model_data.building_elements(1, kk).vertices(1).z];
                p2=[B.thermal_model_data.building_elements(1, kk).vertices(2).x ...
                    B.thermal_model_data.building_elements(1, kk).vertices(2).y ...
                    B.thermal_model_data.building_elements(1, kk).vertices(2).z];
                p3=[B.thermal_model_data.building_elements(1, kk).vertices(3).x ...
                    B.thermal_model_data.building_elements(1, kk).vertices(3).y ...
                    B.thermal_model_data.building_elements(1, kk).vertices(3).z];
                
                U=p2-p1;
                V=p3-p1;
                Nx=U(2)*V(3)-U(3)*V(2);
                Ny=U(3)*V(1)-U(1)*V(3);     % calculate surface-normal of triangle, to know which facade direction this is
                %                 Nz=U(1)*V(2)-U(2)*V(1);
                %                 N=[Nx Ny Nz];
                
                %                     wallArea = cross((p2-p1),(p3-p1));
                wallArea = B.thermal_model_data.building_elements(1,kk).area;
                window_id = B.thermal_model_data.building_elements(1,kk).window_identifier;
                window_area = B.thermal_model_data.getValue(window_id,'glass_area');
                %                 window_height = 2.5;
                
                
                if Nx > 0 && Ny == 0        % East facade ... 90� wind dir
                    FacadeDir=90;
                    %                         abs(90-WindDirection)
                    %                         1/180* abs(90-WindDirection)
                    %                         effWindSpeed= WindSpeed * WindDirection %or something
                elseif Nx < 0 && Ny == 0    % West facade ... 270� wind dir
                    FacadeDir=270;
                elseif Nx == 0 && Ny > 0    % North facade ... 360� / 0� wind dir
                    FacadeDir=0;
                elseif Nx == 0 && Ny < 0    % South facade ... 180� wind dir
                    FacadeDir=180;
                end
                DirectionDifference = abs(FacadeDir - WindDirection);
                if DirectionDifference > 180
                    DirectionDifference = 180-(DirectionDifference - 180);
                end
                DirectionFactor = (1/180)*DirectionDifference;
                effWindSpeed = DirectionFactor * WindSpeed;
                
                Qw = Cw * window_area * effWindSpeed/4;
                Qs = Cd*(window_area/3)*sqrt(9.81*window_height*(tempIn(i) - tempOut)/(0.5*(tempIn(i)+tempOut)+273.5));
                
                if (Qs > Qw)
                    Qt(i) = Qt(i) + Qs;
                else
                    Qt(i) = Qt(i) + Qw;
                end
                
                
            end
            
        end

        
        
        
    end
end





% 
% 
% i=0;
% 
% Cw = 0.025;
% Cd = 0.6;
% %      Qt=0;
% 
% ventilationMinTemp = 30;
% 
% if (tempIn > ventilationMinTemp)
%     % CIBSE method*/
%     i=1;
%     while (i<=length(EP_model.window))
%         
%         if (EP_model.window(i).length > 0.2 && EP_model.window(i).height>0.2)
%             Aw = EP_model.window(i).length * 0.2;
%         else
%             Aw = EP_model.window(i).length * EP_model.window(i).height;
%         end
%         
%         Qw = Cw * Aw * effWindSpeed/4;
%         
%         Qs = Cd*(Aw/3)*sqrt(9.81*EP_model.window(i).height*(tempIn - tempOut)/(0.5*(tempIn+tempOut)+273.5));
%         
%         if (Qs > Qw)
%             Qt = Qt + Qs;
%         else
%             
%             Qt = Qt + Qw;
%         end
%         i=i+1;
%     end
% end
end


