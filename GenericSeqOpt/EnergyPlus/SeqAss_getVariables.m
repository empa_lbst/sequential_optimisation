function [construction] = SeqAss_getVariables(wall, floor, roof, window)


%% Defining Building Mass Type (heavy (2), light (1))
% replace lines in "BuildingSurface:Detailed", ->"constructions"
switch wall
    case 1 %'light'
        construction.wall = 'Wall - State-of-the-art - Lightweight (data modified when loaded to file)';
        
    case 2 %'heavy'
        construction.wall = 'Wall - State-of-the-art - Heavyweight (data modified when loaded to file)';
        
end


switch floor
    case 1 %light
        construction.floor = 'Combined ground floor - State-of-the-art - Lightweight (data modified when loaded to file)';
        construction.slab = '100mm concrete slab_Reversed';
        construction.slabrev = '100mm concrete slab_Reversed_Rev';
    case 2 %heavy
        construction.floor = 'Combined ground floor - State-of-the-art - Heavyweight (data modified when loaded to file)';
        construction.slab = '300mm concrete slab_Reversed';
        construction.slabrev = '300mm concrete slab_Reversed_Rev';
end


switch roof
    case 1 %light
        construction.roof = 'Combined flat roof - State-of-the-art - Lightweight (data modified when loaded to file)';
    case 2 %heavy
        construction.roof = 'Combined flat roof - State-of-the-art - Heavyweight (data modified when loaded to file)';
end



switch window
    case 1 %win1
        construction.window = 'win1';
    case 2 %win2
        construction.window = 'win2';
    case 3 %win3
        construction.window = 'win3';
end

end