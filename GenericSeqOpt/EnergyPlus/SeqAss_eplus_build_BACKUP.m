%% EPlus Run
% Set up an EnergyPlus model
% repobj.type = 'Shading:Site:Detailed'; % type of object
% repobj.name = '*'; % name of specific object instance, or * for all
% repobj.propno = 3; % property number i.e. line number in idf editor
% repobj.propval = val(2); % new value
%%
function file = SeqAss_eplus_build(get_input, var, generation, job)

% var = [0.4  	0.8   	0     0       -0.8       5     8       0.9     0.9     0.1     0.1     1       1       1       3       500];
% generation = 1;
% job = 1;

% Variables:
% var(1)    -   Control Point X [-1,1]      changing typology and floorplan size
% var(2)    -   Control Point Y [-1,1]      changing typology and floorplan size
% var(3)    -   Control Point Z [0,2]       changing typology and floorplan size
% var(4)    -   Orientation     [-30,30]    orientation of the building, in degree deviation from North (0�)
% var(5)    -   Length/Width    [-1,1]      Length/Width Ratio. stretching east/west (1) or north/south (-1)
% var(6)    -   Ceiling Height  [3,5]       Ceiling height per floor, in meters
% var(7)    -   No. of storeys  [1,10]      Number of storeys.
% var(8)    -   Glazing S       [0.01,0.9]  Glazing Ratio on South Facades
% var(9)    -   Glazing E       [0.01,0.9]  Glazing Ratio on East Facades
% var(10)    -   Glazing N       [0.01,0.9]  Glazing Ratio on North Facades
% var(11)   -   Glazing W       [0.01,0.9]  Glazing Ratio on West Facades
% var(12)   -   Walls Constr.   [1,2]       Construction type for Walls (light heavy)
% var(13)   -   Floors Constr.  [1,2]       Construction type for Floors (light heavy)
% var(14)   -   Roof Constr.    [1,2]       Construction type for Roof (light heavy)
% var(15)   -   Glazing Type    [1,3]       Glazing Type windows (WSG 2, WSG 3, SSG 2)
% var(16)   -   Insulation      [50,300]    Insulation thickness for walls in mm. Same insulation material always




%% get some variables for eplus build  ----------------------------------------------------------------------------
% -----------------------------------------------------------------------------------------------------------------
[VarConstr] = SeqAss_getVariables(var(12),var(13),var(14),var(15));







%% modify building, using BRCM scripts ----------------------------------------------------------------------------
% -----------------------------------------------------------------------------------------------------------------
% USE BRCM builder and load stuff to build Eplus
[RotateBldn, ThermalModelType, EHFModelType] = BRCM_build(var, get_input);


% files : 4 files for each typology. select correct file and path
switch ThermalModelType
    case 'ThermalModel1'
        file = get_input.inputFiles{1};
    case 'ThermalModel2'
        file = get_input.inputFiles{2};
    case 'ThermalModel3'
        file = get_input.inputFiles{3};
    case 'ThermalModel4'
        file = get_input.inputFiles{4};
end
   
    


%% load in lines from original idf file ---------------------------------------------------------------------------
% -----------------------------------------------------------------------------------------------------------------
infile = ['C:\eplus\' file '.idf']; % base file
try
    fid = fopen(infile,'r+');
catch
    fprintf('ERROR: FILE NOT FOUND\n');
    err=-1;
end

lines=[];
while feof(fid)==0
    lines{end+1}=fgetl(fid); % get all lines
end
fclose(fid);






%% initialise repobject counter         ---------------------------------------------------------------------------
% -----------------------------------------------------------------------------------------------------------------
iRepobj=1;
linesFlag = zeros(size(lines,2),1);





%% Load Geometry from BRCM ----------------------------------------------------------------------------------------
% -----------------------------------------------------------------------------------------------------------------
% load BRCM files
BRCMbuildingelements    = BRCM_loadcsv('\buildingelements.csv',ThermalModelType,get_input.BRCMfilepath);
BRCMwindows             = BRCM_loadcsv('\windows.csv',ThermalModelType,get_input.BRCMfilepath);





%% find Rotation/Flip according to control point-------------------------------------------------------------------
% -----------------------------------------------------------------------------------------------------------------
% look into BRCMbuild
% add rotation (RotateBldn) to var(4)... rotates E+ Building object

% flipBldnY = 1;     flipBldnX = 1;  if -1, than mirrored on axis X or Y, later when loading vertices
% mirror N/S or E/W according to control point. Only for L-Shape. look into BRCMbuild    
% RotateBldn = 9999  flip West and East   --->  x' = -x
% RotateBldn = 8888  flip North and South --->  y' = -y



if (abs(var(1)) > 0.5 && abs(var(2)) <= 0.5 && var(3) < 1) || (abs(var(2)) > 0.5 && abs(var(1)) <= 0.5 && var(3) < 1)
    % U shape, typ 3
    %     ThermalModelType = 'ThermalModel3';
    if abs(var(1)) > 0.5 && abs(var(2)) <= 0.5  % ]   [
        if var(1) > 0    % rotate -90�
            RotateBldn = -90;
            flipBldnX = 1;
            flipBldnY = 1;
        else % var(1) negative. rotate +90�
            RotateBldn = 90;
            flipBldnX = 1;
            flipBldnY = 1;
        end
    elseif abs(var(1)) <= 0.5 && abs(var(2)) > 0.5      % U  A
        if var(2) > 0   % rotate 180� and flip E/W
            RotateBldn = 180;
            flipBldnX = -1;
            flipBldnY = 1;
        else    % no rotation
            RotateBldn = 0;
            flipBldnX = 1;
            flipBldnY = 1;
        end
    end

elseif abs(var(1)) > 0.5 && abs(var(2)) > 0.5  && var(3) < 1    % .-
    % L shape, typ 4
    %     ThermalModelType = 'ThermalModel4'; %L-Shape
    if var(1) > 0 && var(2) < 0
        RotateBldn = 0;
    elseif var(1) < 0 && var(2) < 0 %flip West and East .- -.
        %         RotateBldn = 9999;
        RotateBldn = 0;
        flipBldnX = -1;
        flipBldnY = 1;
    elseif var(1) < 0 && var(2) > 0 % .-  _.
        RotateBldn = 180;
    elseif var(1) > 0 && var(2) > 0 % flip north south  .-  ._
        %         RotateBldn = 8888;
        RotateBldn = 0;
        flipBldnX = 1;
        flipBldnY = -1;
    end
elseif abs(var(1)) <= 0.5 && abs(var(2)) <= 0.5 && var(3) < 1
    % courtyard, typ 2
    %     ThermalModelType = 'ThermalModel2';
    RotateBldn = 0;
    flipBldnX = 1;
    flipBldnY = 1;
else %if var(3) >= 1
    % block, typ 1
    %     ThermalModelType = 'ThermalModel1';
    RotateBldn = 0;
    flipBldnX = 1;
    flipBldnY = 1;
end







%% ORIENTATION ----------------------------------------------------------------------------------------------------
% -----------------------------------------------------------------------------------------------------------------
EPobject='Building';
for i=1:size(lines,2)
    strTemp=strread(strtrim(lines{i}),'%s','delimiter',',');
    if isempty(strTemp)==0
        if strcmp(strTemp(1),EPobject)  %looking for Building
            
            % creating repobjects for orientation
            strTemp=strread(strtrim(lines{i+1}),'%s','delimiter',',');
            EPsrfname=strTemp{1};       %building name
            repobj(iRepobj) = struct('type',EPobject,'name',EPsrfname,'propno',2,'propval',var(4)+RotateBldn,'delete',false);
            iRepobj=iRepobj+1;      
        end
    end
end






%% BuildingSurface:Detailed  --------------------------------------------------------------------------------------
% -----------------------------------------------------------------------------------------------------------------
% Creating repobjects for "BuildingSurface:Detailed" -> replacing constructions and adjusting vertices

EPobject='BuildingSurface:Detailed';

% new array with all surface names:     Block1:Zone1_Wall_2_0_0, !- Name
% from BRCMbuildingelements:            EP Surface Names:Block1:Zone1_Wall_2_0_1



for i=1:size(lines,2)                                                           % go through each line of the IDF file
    strTemp=strread(strtrim(lines{i}),'%s','delimiter',',');
    if isempty(strTemp)==0
        if strcmp(strTemp(1),EPobject)  %looking for BuildingSurface:Detailed
            
            
            % replace ceiling with roof. if it has less than 10 storeys, ceiling has to be turned to roof
            if var(7) < 10
                strTemp = strread(strtrim(lines{i+1}),'%s','delimiter',',');    %checking surface name IDF File
                strlookfor = strcat('Block',num2str(var(7)),':Zone1_Ceiling');
                found = strfind(strTemp(1),strlookfor);
                if ~isempty(found{1,1})
                    stradd = strsplit(strTemp{1},strlookfor);
                    strreplacewith = strcat('Block',num2str(var(7)),':Zone1_Roof',stradd(2));
                    EPsrfname = strTemp(1);
                    for u=2:size(BRCMbuildingelements,2)
                        strTemp2 = strread(strtrim(BRCMbuildingelements{u}),'%s','delimiter',';');  %checking surface name BRCM file
                        strTemp2 = strsplit(strTemp2{2},'EP Surface Names:');
                        strTemp2 = strTemp2(2);
                        strTemp2 = strsplit(strTemp2{1},',');
                        if strcmp(strreplacewith,strTemp2(1))                                %if both surfaces coincide, change geometry and construction
                            linesFlag(i)=1;
                            
                            strTemp2 = strsplit(BRCMbuildingelements{u},';');
                            strTemp2 = strsplit(strTemp2{end-1},{'(',',',')'});
                            p1 = [num2str(str2num(strTemp2{2})*flipBldnX),',',num2str(str2num(strTemp2{3})*flipBldnY),',',num2str(str2num(strTemp2{4}))];
                            p2 = [num2str(str2num(strTemp2{5})*flipBldnX),',',num2str(str2num(strTemp2{6})*flipBldnY),',',num2str(str2num(strTemp2{7}))];
                            p3 = [num2str(str2num(strTemp2{8})*flipBldnX),',',num2str(str2num(strTemp2{9})*flipBldnY),',',num2str(str2num(strTemp2{10}))];
                            p4 = [num2str(str2num(strTemp2{11})*flipBldnX),',',num2str(str2num(strTemp2{12})*flipBldnY),',',num2str(str2num(strTemp2{13}))];
                            
                            repobj(iRepobj) = struct('type',EPobject,'name',EPsrfname,'propno',1,'propval',strreplacewith,'delete',false);
                            iRepobj=iRepobj+1;
                            repobj(iRepobj) = struct('type',EPobject,'name',EPsrfname,'propno',2,'propval','Roof','delete',false);
                            iRepobj=iRepobj+1;
                            repobj(iRepobj) = struct('type',EPobject,'name',EPsrfname,'propno',3,'propval',VarConstr.roof,'delete',false);
                            iRepobj=iRepobj+1;
                            
                            repobj(iRepobj) = struct('type',EPobject,'name',EPsrfname,'propno',5,'propval','Outdoors','delete',false);
                            iRepobj=iRepobj+1;
                            repobj(iRepobj) = struct('type',EPobject,'name',EPsrfname,'propno',6,'propval',' ','delete',false);
                            iRepobj=iRepobj+1;
                            repobj(iRepobj) = struct('type',EPobject,'name',EPsrfname,'propno',7,'propval','SunExposed','delete',false);
                            iRepobj=iRepobj+1;
                            repobj(iRepobj) = struct('type',EPobject,'name',EPsrfname,'propno',8,'propval','WindExposed','delete',false);
                            iRepobj=iRepobj+1;
                            
                            repobj(iRepobj) = struct('type',EPobject,'name',EPsrfname,'propno',11,'propval',p1,'delete',false);
                            iRepobj=iRepobj+1;
                            repobj(iRepobj) = struct('type',EPobject,'name',EPsrfname,'propno',12,'propval',p2,'delete',false);
                            iRepobj=iRepobj+1;
                            repobj(iRepobj) = struct('type',EPobject,'name',EPsrfname,'propno',13,'propval',p3,'delete',false);
                            iRepobj=iRepobj+1;
                            repobj(iRepobj) = struct('type',EPobject,'name',EPsrfname,'propno',14,'propval',p4,'delete',false);
                            iRepobj=iRepobj+1; 
                        end
                    end
                end
            end
            
            
            
            
            %handle floors, which are not existant in BRCM
            strTemp = strread(strtrim(lines{i+1}),'%s','delimiter',',');    %checking surface name IDF File
            strlookfor = ':Zone1_Floor';
            found = strfind(strTemp(1),strlookfor);
            if ~isempty(found{1,1})
                EPsrfname = strTemp(1);
                for u=2:size(BRCMbuildingelements,2)
                    
                    
                    
                    % BRCM ceilings become e+ floors
                    strTemp2 = strread(strtrim(BRCMbuildingelements{u}),'%s','delimiter',';');  %checking surface name BRCM file
                    strTemp2 = strsplit(strTemp2{2},'EP Surface Names:');
                    strTemp2 = strTemp2(2);
                    strTemp2 = strsplit(strTemp2{1},',');
                    stradd = strsplit(strTemp2{1},':Zone1_Ceiling_1');
                    if size(stradd,2) > 1
                        strTemp = EPsrfname;
                        strreplacewith = strcat(stradd{1},strlookfor,'_0',stradd{2});
                        if strcmp(strTemp,strreplacewith)                                %if both surfaces coincide, change geometry and construction
                            linesFlag(i)=1;
                            
                            %geometry
                            strTemp2 = strsplit(BRCMbuildingelements{u},';');
                            strTemp2 = strsplit(strTemp2{end-1},{'(',',',')'});
                            p1 = [num2str(str2num(strTemp2{2})*flipBldnX),',',num2str(str2num(strTemp2{3})*flipBldnY),',',num2str(str2num(strTemp2{4})-var(6))];
                            p2 = [num2str(str2num(strTemp2{5})*flipBldnX),',',num2str(str2num(strTemp2{6})*flipBldnY),',',num2str(str2num(strTemp2{7})-var(6))];
                            p3 = [num2str(str2num(strTemp2{8})*flipBldnX),',',num2str(str2num(strTemp2{9})*flipBldnY),',',num2str(str2num(strTemp2{10})-var(6))];
                            p4 = [num2str(str2num(strTemp2{11})*flipBldnX),',',num2str(str2num(strTemp2{12})*flipBldnY),',',num2str(str2num(strTemp2{13})-var(6))];                            
                            repobj(iRepobj) = struct('type',EPobject,'name',EPsrfname,'propno',11,'propval',p1,'delete',false);
                            iRepobj=iRepobj+1;
                            repobj(iRepobj) = struct('type',EPobject,'name',EPsrfname,'propno',12,'propval',p2,'delete',false);
                            iRepobj=iRepobj+1;
                            repobj(iRepobj) = struct('type',EPobject,'name',EPsrfname,'propno',13,'propval',p3,'delete',false);
                            iRepobj=iRepobj+1;
                            repobj(iRepobj) = struct('type',EPobject,'name',EPsrfname,'propno',14,'propval',p4,'delete',false);
                            iRepobj=iRepobj+1;
                            
                            %construction
                            repobj(iRepobj) = struct('type',EPobject,'name',EPsrfname,'propno',3,'propval',VarConstr.slabrev,'delete',false);
                            iRepobj=iRepobj+1;
                        end
                    end
                    
                    
                    
                    % BRCM roof becomes e+ floor, z-var(6)
                    %add if: var(7) storeys... for last level ,  take
                    %BRCM roof-geometries.... because BRCM doesnt have ceiling for last level
                    strCurrentLevel = strsplit(lines{i+1},'Block');
                    strCurrentLevel = strsplit(strCurrentLevel{2},':Zone');
                    if str2double(strCurrentLevel{1}) == var(7)
                        strTemp2 = strread(strtrim(BRCMbuildingelements{u}),'%s','delimiter',';');  %checking surface name BRCM file
                        strTemp2 = strsplit(strTemp2{2},'EP Surface Names:');
                        strTemp2 = strTemp2(2);
                        stradd = strsplit(strTemp2{1},':Zone1_Roof_1');
                        strlookfor = ':Zone1_Floor';
                        if size(stradd,2) > 1
                            strTemp = EPsrfname;
                            strreplacewith = strcat(stradd{1},strlookfor,'_0',stradd{2});
                            if strcmp(strTemp,strreplacewith)                                %if both surfaces coincide, change geometry and construction
                                linesFlag(i)=1;
                                
                                %geometry
                                strTemp2 = strsplit(BRCMbuildingelements{u},';');
                                strTemp2 = strsplit(strTemp2{end-1},{'(',',',')'});
                                p1 = [num2str(str2num(strTemp2{2})*flipBldnX),',',num2str(str2num(strTemp2{3})*flipBldnY),',',num2str(str2num(strTemp2{4})-var(6))];
                                p2 = [num2str(str2num(strTemp2{5})*flipBldnX),',',num2str(str2num(strTemp2{6})*flipBldnY),',',num2str(str2num(strTemp2{7})-var(6))];
                                p3 = [num2str(str2num(strTemp2{8})*flipBldnX),',',num2str(str2num(strTemp2{9})*flipBldnY),',',num2str(str2num(strTemp2{10})-var(6))];
                                p4 = [num2str(str2num(strTemp2{11})*flipBldnX),',',num2str(str2num(strTemp2{12})*flipBldnY),',',num2str(str2num(strTemp2{13})-var(6))];
                                repobj(iRepobj) = struct('type',EPobject,'name',EPsrfname,'propno',11,'propval',p1,'delete',false);
                                iRepobj=iRepobj+1;
                                repobj(iRepobj) = struct('type',EPobject,'name',EPsrfname,'propno',12,'propval',p2,'delete',false);
                                iRepobj=iRepobj+1;
                                repobj(iRepobj) = struct('type',EPobject,'name',EPsrfname,'propno',13,'propval',p3,'delete',false);
                                iRepobj=iRepobj+1;
                                repobj(iRepobj) = struct('type',EPobject,'name',EPsrfname,'propno',14,'propval',p4,'delete',false);
                                iRepobj=iRepobj+1;
                                
                                %construction
                                repobj(iRepobj) = struct('type',EPobject,'name',EPsrfname,'propno',3,'propval',VarConstr.slabrev,'delete',false);
                                iRepobj=iRepobj+1;
                            end
                        end
                    end
                end
            end
            
            
            
            
            % walls and  ceilings
            for u=2:size(BRCMbuildingelements,2)                                % go through all BRCMbuildingelements objects
                strTemp = strread(strtrim(lines{i+1}),'%s','delimiter',',');    %checking surface name IDF File
                strTemp2 = strread(strtrim(BRCMbuildingelements{u}),'%s','delimiter',';');  %checking surface name BRCM file
                strTemp2 = strsplit(strTemp2{2},'EP Surface Names:');
                strTemp2 = strTemp2(2);
                strTemp2 = strsplit(strTemp2{1},',');
                if strcmp(strTemp(1),strTemp2(1))                                %if both surfaces coincide, change geometry and construction
                    linesFlag(i)=1;                                                 %flag, not to delete this BuildingSurface:Detailed
                    EPsrfname = strTemp(1);
                    
                    % GEOMETRY
                    strTemp2 = strsplit(BRCMbuildingelements{u},';');
                    strTemp2 = strsplit(strTemp2{end-1},{'(',',',')'});
                    p1 = [num2str(str2num(strTemp2{2})*flipBldnX),',',num2str(str2num(strTemp2{3})*flipBldnY),',',num2str(str2num(strTemp2{4}))];
                    p2 = [num2str(str2num(strTemp2{5})*flipBldnX),',',num2str(str2num(strTemp2{6})*flipBldnY),',',num2str(str2num(strTemp2{7}))];
                    p3 = [num2str(str2num(strTemp2{8})*flipBldnX),',',num2str(str2num(strTemp2{9})*flipBldnY),',',num2str(str2num(strTemp2{10}))];
                    p4 = [num2str(str2num(strTemp2{11})*flipBldnX),',',num2str(str2num(strTemp2{12})*flipBldnY),',',num2str(str2num(strTemp2{13}))];
                    repobj(iRepobj) = struct('type',EPobject,'name',EPsrfname,'propno',11,'propval',p1,'delete',false);
                    iRepobj=iRepobj+1;
                    repobj(iRepobj) = struct('type',EPobject,'name',EPsrfname,'propno',12,'propval',p2,'delete',false);
                    iRepobj=iRepobj+1;
                    repobj(iRepobj) = struct('type',EPobject,'name',EPsrfname,'propno',13,'propval',p3,'delete',false);
                    iRepobj=iRepobj+1;
                    repobj(iRepobj) = struct('type',EPobject,'name',EPsrfname,'propno',14,'propval',p4,'delete',false);
                    iRepobj=iRepobj+1;
                    
                    %CONSTRUCTION
                    strTemp = strread(strtrim(lines{i+2}),'%s','delimiter',',');    %checking surface type (wall, floor, roof)
                    strTemp2 = strread(strtrim(lines{i+5}),'%s','delimiter',',');   %checking adjacency (ground, outdoor)
                    %    creating repobjects for walls
                    if strcmp(strTemp(1),'Wall')    %looking for Walls
                        repobj(iRepobj) = struct('type',EPobject,'name',EPsrfname,'propno',3,'propval',VarConstr.wall,'delete',false);
                        iRepobj=iRepobj+1;
                    elseif strcmp(strTemp(1),'Floor') && strcmp(strTemp2(1),'Ground')   %looking for groundfloor
                        repobj(iRepobj) = struct('type',EPobject,'name',EPsrfname,'propno',3,'propval',VarConstr.floor,'delete',false);
                        iRepobj=iRepobj+1;
                    elseif strcmp(strTemp(1),'Roof') && strcmp(strTemp2(1),'Outdoors')   %looking for roof
                        repobj(iRepobj) = struct('type',EPobject,'name',EPsrfname,'propno',3,'propval',VarConstr.roof,'delete',false);
                        iRepobj=iRepobj+1;
                    elseif strcmp(strTemp(1),'Ceiling')
                        repobj(iRepobj) = struct('type',EPobject,'name',EPsrfname,'propno',3,'propval',VarConstr.slab,'delete',false);
                        iRepobj=iRepobj+1;
                    end
                end           
            end  
            
            
        end
    end
end

for i=1:size(lines,2)                                                           % go through each line of the IDF file again and delete excessive surfaces (if it has less than 10 floors)
    strTemp=strread(strtrim(lines{i}),'%s','delimiter',',');
    if isempty(strTemp)==0
        if strcmp(strTemp(1),EPobject)  %looking for BuildingSurface:Detailed
            if linesFlag(i) == 0                                                %no flag, hence delete this object

                strTemp = strread(strtrim(lines{i+1}),'%s','delimiter',',');    %checking surface name IDF File
                EPsrfname = strTemp(1);
                for u=0:14
                    repobj(iRepobj) = struct('type',EPobject,'name',EPsrfname,'propno',u,'propval',' ','delete',true);
                    iRepobj=iRepobj+1;
                end
                % !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                % !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                % !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                 % !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!                   
                % lines(i)=' ';
                % linesFlag(i) =1;  for all lines here. 
            end
        end
    end
end






%% CHANGE DAYLIGHTING POINTS  -------------------------------------------------------------------------------------
% -----------------------------------------------------------------------------------------------------------------
% adjust lighting control coordinates, depending on floorplan geometry

%1.: loop through all floor surfaces. take BRCM building elements
%2.: take biggest floor surface
i=0;
biggest = 0;
for u=2:size(BRCMbuildingelements,2)  
    strTemp2 = strread(strtrim(BRCMbuildingelements{u}),'%s','delimiter',';');  %checking surface name BRCM file
    strTemp = strsplit(strTemp2{2},'EP Surface Names:');
    strTemp = strTemp(2);
    strlookfor = ':Zone1_GroundFloor';
    found = strfind(strTemp(1),strlookfor);
    if ~isempty(found{1,1})
        i=i+1;
        intGrndFloors{i} = u;
        areaGrndFloors{i} = str2double(strTemp2{7});
        if str2double(strTemp2{7}) > biggest
           biggest = str2double(strTemp2{7});
           intBiggest = u;
        end
    end
end


%3.: compute centroid of that surface. this is where daylight controlpoints will be located
strTemp2 = strsplit(BRCMbuildingelements{intBiggest},';');
strTemp2 = strsplit(strTemp2{end-1},{'(',',',')'});
clear p1;
clear p3;
p1.x = str2num(strTemp2{2})*flipBldnX;
p1.y = str2num(strTemp2{3})*flipBldnY;

p3.x = str2num(strTemp2{8})*flipBldnX;
p3.y = str2num(strTemp2{9})*flipBldnY;

pCen.x = (p1.x + p3.x) / 2;
pCen.y = (p1.y + p3.y) / 2;
pCen.z = 0.9;
p1 = num2str(pCen.x);
p2 = num2str(pCen.y);


%4.: set points for each level. using var(7) levels and var(6) room height
EPobject='Daylighting:Controls';
for i=1:size(lines,2)
    strTemp=strread(strtrim(lines{i}),'%s','delimiter',',');
    if isempty(strTemp)==0
        if strcmp(strTemp(1),EPobject)                                      %looking for daylight controls
            strTemp = strread(strtrim(lines{i+1}),'%s','delimiter',',');    %checking surface name IDF File
            EPsrfname = strTemp(1);
           
            strTemp2 = strsplit(strTemp{1},'Block');
            strTemp2 = strsplit(strTemp2{2},':');
            strTemp2 = str2double(strTemp2{1});
            if strTemp2 <= var(7)
                linesFlag(i)=1;
                z = (strTemp2-1) * var(6);
                p3 = num2str(pCen.z + z);
                repobj(iRepobj) = struct('type',EPobject,'name',EPsrfname,'propno',3,'propval',p1,'delete',false);
                iRepobj=iRepobj+1;
                repobj(iRepobj) = struct('type',EPobject,'name',EPsrfname,'propno',4,'propval',p2,'delete',false);
                iRepobj=iRepobj+1;
                repobj(iRepobj) = struct('type',EPobject,'name',EPsrfname,'propno',5,'propval',p3,'delete',false);
                iRepobj=iRepobj+1;
            end
        end
    end
end





for i=1:size(lines,2)                                                           % go through each line of the IDF file again and delete excessive lightcontrols (if it has less than 10 floors)
    strTemp=strread(strtrim(lines{i}),'%s','delimiter',',');
    if isempty(strTemp)==0
        if strcmp(strTemp(1),EPobject)  %looking for  Daylighting:Controls
            if linesFlag(i) == 0                                                %no flag, hence delete this object

                strTemp = strread(strtrim(lines{i+1}),'%s','delimiter',',');    %checking  name IDF File
                EPsrfname = strTemp(1);
                for u=0:19
                    repobj(iRepobj) = struct('type',EPobject,'name',EPsrfname,'propno',u,'propval',' ','delete',true);
                    iRepobj=iRepobj+1;
                end
                % !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                % !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                % !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                 % !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!                   
                % lines(i)=' ';
                % linesFlag(i) =1;  for all lines here. because keep ' ' as new line
            end
        end
    end
end





%% ADD INSULATION  ------------------------------------------------------------------------------------------------
% -----------------------------------------------------------------------------------------------------------------
% var(16)   -   Insulation      [50,300]    Insulation thickness for walls in mm. Same insulation material always

EPobject='Material';
for i=1:size(lines,2)
    strTemp=strread(strtrim(lines{i}),'%s','delimiter',',');
    if isempty(strTemp)==0
        if strcmp(strTemp(1),EPobject)                                      %looking for Material
            strTemp = strread(strtrim(lines{i+1}),'%s','delimiter',',');    %checking surface name IDF File
            EPsrfname = strTemp(1);
            
            if strcmp(EPsrfname, 'XPS Extruded Polystyrene  - CO2 Blowing_0.1280')
                blnAddInsulation = true;
            elseif strcmp(EPsrfname, 'XPS Extruded Polystyrene  - CO2 Blowing_0.1175')
                blnAddInsulation = true;
            else
                blnAddInsulation = false;
            end
            
            
            if blnAddInsulation == true
                linesFlag(i)=1;
                
                AddInsulation = str2double(strsplit(lines{i+3}, ','));
                AddInsulation = num2str(AddInsulation(1) + var(16)/1000);
                repobj(iRepobj) = struct('type',EPobject,'name',EPsrfname,'propno',3,'propval',AddInsulation,'delete',false);
                iRepobj=iRepobj+1;
            end

        end
    end
end








%% FenestrationSurface:Detailed -----------------------------------------------------------------------------------
% -----------------------------------------------------------------------------------------------------------------
% Creating repobjects for "FenestrationSurface:Detailed" -> construction name (win1, win2, win3). shading always external, controlled... change in IDF?!!!!!!


% 1: read in vertices and area for all walls in BRCM building elements(only Block1 -> level1!!)
i=0;
for u=2:size(BRCMbuildingelements,2)  
    strTemp2 = strread(strtrim(BRCMbuildingelements{u}),'%s','delimiter',';');  %checking surface name BRCM file
    strTemp = strsplit(strTemp2{2},'EP Surface Names:');
    strTemp = strTemp(2);
    strlookfor = 'Block1:Zone1_Wall';
    found = strfind(strTemp(1),strlookfor);
    if ~isempty(found{1,1})
        i=i+1;
        intWalls{i} = u;
        areaWalls{i} = str2double(strTemp2{7});
        strWalls{i} = strTemp(1);
        vertWalls{i} = strTemp2{8};
    end
end



% 2: read in BRCM window file and store glass area (only Block1 -> level1!!)

i=0;
for u=2:size(BRCMwindows,2)  
    strTemp2 = strread(strtrim(BRCMwindows{u}),'%s','delimiter',';');  %checking surface name BRCM file
    strTemp = strsplit(strTemp2{2},'EP Surface:');
    strTemp = strsplit(strTemp{2},'/');
    strTemp = strTemp(1);
    strlookfor = 'Block1:Zone1_Wall';
    found = strfind(strTemp(1),strlookfor);
    if ~isempty(found{1,1})
        i=i+1;
        intWindows{i} = u;
        areaWindows{i} = str2double(strTemp2{3});
        strTemp2 = strsplit(strTemp{1},':');
        strWindows{i} = strcat(strTemp2(2),'_Win');      % erase Block1: ... later BlockX: will be added X=level

        for k=1:size(strWalls,2)                        % look for corresponding wall. should have the same name as strTemp(1)
            if strcmp(strTemp(1),strWalls{k})
                % get galzing ratio
                % calculate window vertices from wall vertices
                dblRatio = areaWindows{i} / areaWalls{k};
                wallHeight = var(6);
               
                strTemp2 = strsplit(vertWalls{k},{'(',',',')'});
                clear p1;
                clear p2;
                p1.x = str2num(strTemp2{2})*flipBldnX;
                p1.y = str2num(strTemp2{3})*flipBldnY;
                p1.z = str2num(strTemp2{4});
                p2.x = str2num(strTemp2{5})*flipBldnX;
                p2.y = str2num(strTemp2{6})*flipBldnY;
                p2.z = str2num(strTemp2{7});
                
                wallLength = sqrt((p1.x-p2.x)^2+(p1.y-p2.y)^2);     %!!!!!!!!!!!!!!!!!!!!
                
                 
                wallSqrt = sqrt(areaWalls{k});
                wallHPerc = wallHeight / wallSqrt;
                wallLPerc = wallLength / wallSqrt;
                
                winSqrt = sqrt(areaWindows{i});
                winHeight = wallHPerc * winSqrt;
                winLength = wallLPerc * winSqrt;
                
                
                %now calculate window vertices from wall vertices
                if abs(p1.x - p2.x) > abs(p1.y - p2.y)                           %if wall is on x axis
                    p1w.x = ((p1.x+p2.x)/2) - (winLength/2);
                    p2w.x = ((p1.x+p2.x)/2) + (winLength/2);
                    p3w.x = p2w.x;
                    p4w.x = p1w.x;
                    
                    p1w.y = p1.y;
                    p2w.y = p2.y;
                    p3w.y = p2w.y;
                    p4w.y = p1w.y;
                    
                    p1w.z = ((p1.z+wallHeight)/2) - (winHeight/2);
                    p2w.z = p1w.z;
                    p3w.z = p1w.z + winHeight;
                    p4w.z = p3w.z;
                else                                                            %if wall is on y axis
                    p1w.y = ((p1.y+p2.y)/2) - (winLength/2);
                    p2w.y = ((p1.y+p2.y)/2) + (winLength/2);
                    p3w.y = p2w.y;
                    p4w.y = p1w.y;
                    
                    p1w.x = p1.x;
                    p2w.x = p2.x;
                    p3w.x = p2w.x;
                    p4w.x = p1w.x;
                    
                    p1w.z = ((p1.z+wallHeight)/2) - (winHeight/2);
                    p2w.z = p1w.z;
                    p3w.z = p1w.z + winHeight;
                    p4w.z = p3w.z;
                end
                
                vertWindows{i}=[p1w p2w p3w p4w]; %  access coordinate: vertWindows{1}(2).y     wall 1, p2, y
            end
            

            
        end



    end
end

% 3: split BRCM windows for ';', take str{2} name and str{3} glass area
% 4: split str{2} for '/'     :->"EP Surface:Block1:Zone1_Wall_2_0_0/EP Construction:1001"
% 5: take strstr{1} as wall identifier
% 6: calculate window vertices from wall vertices and ratio wall area / glass area.
% 7: keep z coordinate as dbl. loop through var(7) for all levels, and change z coordinate for windows



clear p1;
clear p2;
clear p3;
clear p4;

EPobject='FenestrationSurface:Detailed';
for i=1:size(lines,2)
    strTemp=strread(strtrim(lines{i}),'%s','delimiter',',');
    if isempty(strTemp)==0
        if strcmp(strTemp(1),EPobject)                                      %looking for FenestrationSurface:Detailed
            strTemp = strread(strtrim(lines{i+1}),'%s','delimiter',',');    %checking surface name IDF File
            EPsrfname=strTemp{1};
            
            strTemp = strsplit(strTemp{1},':'); %strTemp{1} = BlockX   strTemp{2}=Zone1_Wall_2_0_0_Win
            strTemp2 = strsplit(EPsrfname,'Block');
            strTemp2 = strsplit(strTemp2{2},':');
            strTemp2 = str2double(strTemp2{1});                      %current level
            if strTemp2 <= var(7)             %if this storey is used
                for u=1:size(strWindows,2)
                    
                    if strcmp(strTemp(2),strWindows{u})                          %looking for windows
                        linesFlag(i)=1;
                        

                        
                        z = (strTemp2-1) * var(6);
                        p1.z = num2str(vertWindows{u}(1).z + z);                                % add z to windows of first floor.
                        p2.z = num2str(vertWindows{u}(2).z + z);
                        p3.z = num2str(vertWindows{u}(3).z + z);
                        p4.z = num2str(vertWindows{u}(4).z + z);
                        
                        
                        p1.x = vertWindows{u}(1).x;
                        p2.x = vertWindows{u}(2).x;
                        p3.x = vertWindows{u}(3).x;
                        p4.x = vertWindows{u}(4).x;
                        
                        p1.y = vertWindows{u}(1).y;
                        p2.y = vertWindows{u}(2).y;
                        p3.y = vertWindows{u}(3).y;
                        p4.y = vertWindows{u}(4).y;
                        
                        % geometry
                        strP1 = [num2str(p1.x),',',num2str(p1.y),',',num2str(p1.z)];
                        strP2 = [num2str(p2.x),',',num2str(p2.y),',',num2str(p2.z)];
                        strP3 = [num2str(p3.x),',',num2str(p3.y),',',num2str(p3.z)];
                        strP4 = [num2str(p4.x),',',num2str(p4.y),',',num2str(p4.z)];
                        
                        repobj(iRepobj) = struct('type',EPobject,'name',EPsrfname,'propno',11,'propval',strP1,'delete',false);
                        iRepobj=iRepobj+1;
                        repobj(iRepobj) = struct('type',EPobject,'name',EPsrfname,'propno',12,'propval',strP2,'delete',false);
                        iRepobj=iRepobj+1;
                        repobj(iRepobj) = struct('type',EPobject,'name',EPsrfname,'propno',13,'propval',strP3,'delete',false);
                        iRepobj=iRepobj+1;
                        repobj(iRepobj) = struct('type',EPobject,'name',EPsrfname,'propno',14,'propval',strP4,'delete',false);
                        iRepobj=iRepobj+1;
                        
                        
                        % Construction
                        repobj(iRepobj) = struct('type',EPobject,'name',EPsrfname,'propno',3,'propval',VarConstr.window,'delete',false);
                        iRepobj=iRepobj+1;
                        
                        
                        
                        
                    end
                end
                
            end
            
            
            
        end
    end
end



for i=1:size(lines,2)                                                           % go through each line of the IDF file again and delete excessive windows surfaces (if it has less than 10 floors)
    strTemp=strread(strtrim(lines{i}),'%s','delimiter',',');
    if isempty(strTemp)==0
        if strcmp(strTemp(1),EPobject)  %looking for BuildingSurface:Detailed
            if linesFlag(i) == 0                                                %no flag, hence delete this object

                strTemp = strread(strtrim(lines{i+1}),'%s','delimiter',',');    %checking surface name IDF File
                EPsrfname = strTemp(1);
                for u=0:14
                    repobj(iRepobj) = struct('type',EPobject,'name',EPsrfname,'propno',u,'propval',' ','delete',true);
                    iRepobj=iRepobj+1;
                end
                % !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                % !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                % !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                 % !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!                   
                % lines(i)=' ';
                % linesFlag(i) =1;  for all lines here. 
            end
        end
    end
end


 



%% ZONES                        -----------------------------------------------------------------------------------
% -----------------------------------------------------------------------------------------------------------------

% 
% Zone, 
%     Block6:Zone1,    

EPobject='Zone';
for i=1:size(lines,2)
    strTemp=strread(strtrim(lines{i}),'%s','delimiter',',');
    if isempty(strTemp)==0
        if strcmp(strTemp(1),EPobject)                                      %looking for Zone
            strTemp = strread(strtrim(lines{i+1}),'%s','delimiter',',');    %checking  name IDF File
            EPsrfname = strTemp(1);
           
            strTemp2 = strsplit(strTemp{1},'Block');
            strTemp2 = strsplit(strTemp2{2},':');
            strTemp2 = str2double(strTemp2{1});
            if strTemp2 <= var(7)
                linesFlag(i)=1;
            else
                strTemp = strread(strtrim(lines{i+1}),'%s','delimiter',',');    %checking  name IDF File
                EPsrfname = strTemp(1);
                for u=0:9
                    repobj(iRepobj) = struct('type',EPobject,'name',EPsrfname,'propno',u,'propval',' ','delete',true);
                    iRepobj=iRepobj+1;
                end
                % !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                % !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                % !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                % !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                % lines(i)=' ';
                % linesFlag(i) =1;  for all lines here. because keep ' ' as new line
            end
        end
    end
end








%% PEOPLE                        -----------------------------------------------------------------------------------
% -----------------------------------------------------------------------------------------------------------------

% 
% Zone, 
%     Block6:Zone1,    
EPobject='People';
for i=1:size(lines,2)
    strTemp=strread(strtrim(lines{i}),'%s','delimiter',',');
    if isempty(strTemp)==0
        if strcmp(strTemp(1),EPobject)                                      %looking for Zone
            strTemp = strread(strtrim(lines{i+1}),'%s','delimiter',',');    %checking  name IDF File
            EPsrfname = strTemp(1);
           
            strTemp2 = strsplit(strTemp{1},'Block');
            strTemp2 = strsplit(strTemp2{2},':');
            strTemp2 = str2double(strTemp2{1});
            if strTemp2 <= var(7)
                linesFlag(i)=1;
            else
                strTemp = strread(strtrim(lines{i+1}),'%s','delimiter',',');    %checking  name IDF File
                EPsrfname = strTemp(1);
                for u=0:19
                    repobj(iRepobj) = struct('type',EPobject,'name',EPsrfname,'propno',u,'propval',' ','delete',true);
                    iRepobj=iRepobj+1;
                end
                % !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                % !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                % !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                % !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                % lines(i)=' ';
                % linesFlag(i) =1;  for all lines here. because keep ' ' as new line
            end
        end
    end
end






%% Lights                        -----------------------------------------------------------------------------------
% -----------------------------------------------------------------------------------------------------------------

% 
% Zone, 
%     Block6:Zone1,    
EPobject='Lights';
for i=1:size(lines,2)
    strTemp=strread(strtrim(lines{i}),'%s','delimiter',',');
    if isempty(strTemp)==0
        if strcmp(strTemp(1),EPobject)                                      %looking for Zone
            strTemp = strread(strtrim(lines{i+1}),'%s','delimiter',',');    %checking  name IDF File
            EPsrfname = strTemp(1);
           
            strTemp2 = strsplit(strTemp{1},'Block');
            strTemp2 = strsplit(strTemp2{2},':');
            strTemp2 = str2double(strTemp2{1});
            if strTemp2 <= var(7)
                linesFlag(i)=1;
            else
                strTemp = strread(strtrim(lines{i+1}),'%s','delimiter',',');    %checking  name IDF File
                EPsrfname = strTemp(1);
                for u=0:12
                    repobj(iRepobj) = struct('type',EPobject,'name',EPsrfname,'propno',u,'propval',' ','delete',true);
                    iRepobj=iRepobj+1;
                end
                % !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                % !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                % !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                % !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                % lines(i)=' ';
                % linesFlag(i) =1;  for all lines here. because keep ' ' as new line
            end
        end
    end
end






%% ElectricEquipment                        -----------------------------------------------------------------------------------
% -----------------------------------------------------------------------------------------------------------------

% 
% Zone, 
%     Block6:Zone1,    
EPobject='ElectricEquipment';
for i=1:size(lines,2)
    strTemp=strread(strtrim(lines{i}),'%s','delimiter',',');
    if isempty(strTemp)==0
        if strcmp(strTemp(1),EPobject)                                      %looking for Zone
            strTemp = strread(strtrim(lines{i+1}),'%s','delimiter',',');    %checking  name IDF File
            EPsrfname = strTemp(1);
           
            strTemp2 = strsplit(strTemp{1},'Block');
            strTemp2 = strsplit(strTemp2{2},':');
            strTemp2 = str2double(strTemp2{1});
            if strTemp2 <= var(7)
                linesFlag(i)=1;
            else
                strTemp = strread(strtrim(lines{i+1}),'%s','delimiter',',');    %checking  name IDF File
                EPsrfname = strTemp(1);
                for u=0:11
                    repobj(iRepobj) = struct('type',EPobject,'name',EPsrfname,'propno',u,'propval',' ','delete',true);
                    iRepobj=iRepobj+1;
                end
                % !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                % !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                % !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                % !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                % lines(i)=' ';
                % linesFlag(i) =1;  for all lines here. because keep ' ' as new line
            end
        end
    end
end

%% ZoneInfiltration                        -----------------------------------------------------------------------------------
% -----------------------------------------------------------------------------------------------------------------

% 
% ZoneInfiltration
%     Block6:Zone1,    
EPobject='ZoneInfiltration:DesignFlowRate';
for i=1:size(lines,2)
    strTemp=strread(strtrim(lines{i}),'%s','delimiter',',');
    if isempty(strTemp)==0
        if strcmp(strTemp(1),EPobject)                                      %looking for ZoneInfiltration
            strTemp = strread(strtrim(lines{i+1}),'%s','delimiter',',');    %checking  name IDF File
            EPsrfname = strTemp(1);
           
            strTemp2 = strsplit(strTemp{1},'Block');
            strTemp2 = strsplit(strTemp2{2},':');
            strTemp2 = str2double(strTemp2{1});
            if strTemp2 <= var(7)
                linesFlag(i)=1;
            else
                strTemp = strread(strtrim(lines{i+1}),'%s','delimiter',',');    %checking  name IDF File
                EPsrfname = strTemp(1);
                for u=0:12
                    repobj(iRepobj) = struct('type',EPobject,'name',EPsrfname,'propno',u,'propval',' ','delete',true);
                    iRepobj=iRepobj+1;
                end
                % !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                % !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                % !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                % !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                % lines(i)=' ';
                % linesFlag(i) =1;  for all lines here. because keep ' ' as new line
            end
        end
    end
end





%% HVACTemplate:Zone:IdealLoadsAirSystem  -----------------------------------------------------------------------
% -----------------------------------------------------------------------------------------------------------------

% 
% HVACTemplate:Zone:IdealLoadsAirSystem
%     Block6:Zone1,    
EPobject='HVACTemplate:Zone:IdealLoadsAirSystem';
for i=1:size(lines,2)
    strTemp=strread(strtrim(lines{i}),'%s','delimiter',',');
    if isempty(strTemp)==0
        if strcmp(strTemp(1),EPobject)                                      %looking for ZoneInfiltration
            strTemp = strread(strtrim(lines{i+1}),'%s','delimiter',',');    %checking  name IDF File
            EPsrfname = strTemp(1);
           
            strTemp2 = strsplit(strTemp{1},'Block');
            strTemp2 = strsplit(strTemp2{2},':');
            strTemp2 = str2double(strTemp2{1});
            if strTemp2 <= var(7)
                linesFlag(i)=1;
            else
                strTemp = strread(strtrim(lines{i+1}),'%s','delimiter',',');    %checking  name IDF File
                EPsrfname = strTemp(1);
                for u=0:30
                    repobj(iRepobj) = struct('type',EPobject,'name',EPsrfname,'propno',u,'propval',' ','delete',true);
                    iRepobj=iRepobj+1;
                end
                % !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                % !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                % !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                % !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                % lines(i)=' ';
                % linesFlag(i) =1;  for all lines here. because keep ' ' as new line
            end
        end
    end
end






%% parse to eplus_edit
% !!!!!!!!!!!!!!!! TEST: prepare idf files WITHOUT geometry. then, just add
% geometries, instead of usin eplus_edit.... takes too long!!

% !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!


% for i=1:size(lines,2)
%     if flag(i) == 0
%         NEWlines(i) = lines(i);
%     elseif flag(i) == 1
%         NEWlines(i) = repLines(i);
%     end
% end
% WRITE FILE WITH NEWlines. no eplus_edit anymore. but copy stuff from
% there for writing new idf file.


err = SeqAss_eplus_edit(repobj, file, generation, job); % edit idf


end






