%% EPlus Run
% Set up and execute an EnergyPlus model
% repobj.type = 'Shading:Site:Detailed'; % type of object
% repobj.name = '*'; % name of specific object instance, or * for all
% repobj.propno = 3; % property number i.e. line number in idf editor EXCLUDING name
% repobj.propval = val(2); % new value
%%
% var = [-0.5  	-0.9   	0     0       0       5     5       0.5     0.3     0.3     0.4     1       1       1       3       0];
% var = [0.9  	0.8   	0     0       -0.8       5     8       0.9     0.9     0.9     0.9     1       1       1       3       500];
% generation = 1;
% job = 1;
% ......get_input -> from SeqAss_RUN

% Variables:
% var(1)    -   Control Point X [-1,1]      changing typology and floorplan size
% var(2)    -   Control Point Y [-1,1]      changing typology and floorplan size
% var(3)    -   Control Point Z [0,2]       changing typology and floorplan size
% var(4)    -   Orientation     [-30,30]    orientation of the building, in degree deviation from North (0�)
% var(5)    -   Length/Width    [-1,1]      Length/Width Ratio. stretching east/west (1) or north/south (-1)
% var(6)    -   Ceiling Height  [3,5]       Ceiling height per floor, in meters
% var(7)    -   No. of storeys  [1,10]      Number of storeys.
% var(8)    -   Glazing S       [0.01,0.9]  Glazing Ratio on South Facades
% var(9)    -   Glazing E       [0.01,0.9]  Glazing Ratio on East Facades
% var(10)    -   Glazing N       [0.01,0.9]  Glazing Ratio on North Facades
% var(11)   -   Glazing W       [0.01,0.9]  Glazing Ratio on West Facades
% var(12)   -   Walls Constr.   [1,2]       Construction type for Walls (light heavy)
% var(13)   -   Floors Constr.  [1,2]       Construction type for Floors (light heavy)
% var(14)   -   Roof Constr.    [1,2]       Construction type for Roof (light heavy)
% var(15)   -   Glazing Type    [1,3]       Glazing Type windows (WSG 2, WSG 3, SSG 2)
% var(16)   -   Insulation      [50,300]    Insulation thickness for walls in mm. Same insulation material always



function [ENERGY, CAPITAL] = SeqAss_eplus_run(var, generation, job, get_input)
% files = get_input.inputFiles; % IDF file name
zones=get_input.zones;  
outvars=get_input.outvars;
weatherfile=get_input.weatherfile;
EPbat=get_input.EPbat;

[file, dblFloorarea, CAPITAL] = SeqAss_eplus_build(get_input, var, generation, job); % edit the IDF file
% file = get_input.inputFiles{4};
fprintf(['Running E+ (vars:' repmat(' %g',1,size(var,2)) ')...'],var); % report
tic

newfile=strcat(file,'_',num2str(generation),'_',num2str(job));

% the batch file RunEPlusEdit.bat should point to C:\eplus and C:\eplus\Outputs
[out.status,out.output] = system([EPbat ' ' newfile ' ' weatherfile]);
timetaken = toc;
fprintf(' Done (%g seconds).\n',timetaken); % report



s={};
for j=1:size(outvars,1)
    for i=1:var(7)  %for i=1:size(zones,1)  % var(7) is no of storeys
        s{end+1} = [zones{i} outvars{j}];
        %s{end+1} = [outvars{j}];
    end
end
%s{end+1} = 'CORE_BOTTOM:Zone Hot Water Equipment Total Heating Energy [J](Hourly)';

CSVFileToRead=['C:\eplus\Outputs\' newfile '.csv'];
while exist(CSVFileToRead, 'file') ~= 2
    'waiting for csv file'
end

[~,data] = eplus_readcsv(CSVFileToRead,s);
%sumdata = [sum(data(:,1:30),2)/1000 sum(data(:,46:60),2)/3600000 sum(data(:,[31:45 61]),2)/3600000]; % sum loads: elec, cool, heat
%sumdata = [sum(data(:,1),2)/3600000 sum(data(:,2),2)/3600000]; % sum loads: elec, cool, heat
sumdata= [sum(data(:,1:var(7)),2)/3600000 sum(data(:,(var(7)+1):(var(7)*2)),2)/3600000 sum(data(:,(var(7)*2+1):(var(7)*3)),2)/1000]; %sum loads (heat, cool, light), to get demand
sumdata = sumdata / var(7);
HEAT = sum(sumdata(:,1))/dblFloorarea;
COOL = sum(sumdata(:,2))/dblFloorarea;
LIGHT = sum(sumdata(:,3))/dblFloorarea;
% fprintf('EPlus results read (HEAT: %g, COOL: %g).\n',sumdata); % report number of replacements


out.job = job;
out.data = sumdata;
out.timetaken = timetaken;




ENERGY = HEAT + COOL + LIGHT;

% CAPITAL=sumdata(2); % !!!!!!!!!!!!!!!!!! cp. BRCM capital... depends on Typology. here: 'file' tells you which typology it is
% get costs from BRCM scripts...
% correct capital calculation



end


