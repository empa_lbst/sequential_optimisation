%% This example runs Sequential Optimisation with
% 1st Model: RC-Network Model BRCM (http://www.brcm.ethz.ch/doku.php)
% 2nd Model: Energy-Plus (https://energyplus.net/)
% Optimizer: NSGA_2 (http://www.iitk.ac.in/kangal/)
% 
% Christoph Waibel 2015 - christoph.waibel@empa.ch





%% Paths and Folders
addpath(genpath('C:\Users\wach\Documents\MATLAB\tbxmanager\toolboxes\brcm\v1.02\all\BRCMToolbox_v1.02')); %add path for BRCM toolbox
addpath('EnergyPlus');  %energyplus scripts are here
addpath(genpath('Optimisers'));  %optimizers are here
addpath('brcm');    %brcm modification script
addpath('others');
BRCMfilepath = 'H:\PROJEKTE\14_Sequential Assessment\4_Case Study\brcm'; %add path for BRCM files (have to be prepared in advance)
disturbancesBRCM=importdata('H:\PROJEKTE\14_Sequential Assessment\4_Case Study\brcm\disturbances geneva.csv'); % Weatherfile and temperature setpoints for BRCM
zones = importdata('C:\eplus\zonenames.csv');
outvars = importdata('C:\eplus\outvars.csv');
weatherfile = 'CHE_GENEVA_IWEC';   %must be in C:\EnergyPlusV8-1-0\WeatherData. or change weather_path in EPbat
EPbat = 'C:\EnergyPlusV8-1-0\RunEPlusChris.bat';
inputFiles={'Block_10_Floors', 'Courtyard_10_Floors', 'U-Shape_10_Floors', 'L-Shape_10_Floors'};     %must be in C:\eplus


%% settings for evaluation models
m1sets.get_input=struct('disturbances',{disturbancesBRCM}, 'BRCMfilepath',{BRCMfilepath});
m2sets.get_input=struct('zones',{zones},'outvars',{outvars},'weatherfile',{weatherfile},'EPbat',{EPbat},'inputFiles',{inputFiles}, 'BRCMfilepath',{BRCMfilepath});



%% Evaluation Models
model1=@BRCM_run;
model2=@SeqAss_eplus_run;  %evaluation function, specific to this idf file






%% NSGA2 settings
optsets.pop=50; %population size
optsets.gen=100; %might converge earlier -> hypervolume convergence, inside nsga_2
optsets.M=2; %2 obejctives: capital and energy
optsets.V=16; %19 decision variables:
optsets.minV =      [-1     -1      0       -30     -1      3       1       0.01    0.01    0.01    0.01    1       1       1       1       0];
optsets.maxV =      [1      1       2       30      1       5       10      0.9     0.9     0.9     0.9     2       2       2       3       300];
optsets.IntegerV =  [0      0       0       0       0       0       1       0       0       0       0       1       1       1       1       0];
% Variables:
% var(1)    -   Control Point X [-1,1]      changing typology and floorplan size
% var(2)    -   Control Point Y [-1,1]      changing typology and floorplan size
% var(3)    -   Control Point Z [0,2]       changing typology and floorplan size
% var(4)    -   Orientation     [-30,30]    orientation of the building, in degree deviation from North (0�)
% var(5)    -   Length/Width    [-1,1]      Length/Width Ratio. stretching east/west (1) or north/south (-1)
% var(6)    -   Ceiling Height  [3,5]       Ceiling height per floor, in meters
% var(7)    -   No. of storeys  [1,10]      Number of storeys.
% var(8)    -   Glazing S       [0.01,0.9]  Glazing Ratio on South Facades
% var(9)    -   Glazing E       [0.01,0.9]  Glazing Ratio on East Facades
% var(10)    -   Glazing N       [0.01,0.9] Glazing Ratio on North Facades
% var(11)   -   Glazing W       [0.01,0.9]  Glazing Ratio on West Facades
% var(12)   -   Walls Constr.   [1,2]       Construction type for Walls (light heavy)
% var(13)   -   Floors Constr.  [1,2]       Construction type for Floors (light heavy)
% var(14)   -   Roof Constr.    [1,2]       Construction type for Roof (light heavy)
% var(15)   -   Glazing Type    [1,3]       Glazing Type windows (WSG 2, WSG 3, SSG 2)
% var(16)   -   Insulation      [0,300]     Insulation thickness for walls in mm. Same insulation material always




%% Execute Sequential Optimisation
Generic_SeqOpt(model1, model2, m1sets, m2sets, optsets);




