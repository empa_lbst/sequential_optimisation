function Generic_SeqOpt(model1, model2, m1sets, m2sets, optsets)

%% function Generic_SeqOpt(k, model1, model2, m1sets, m2sets)
%
% SEQUENTIAL OPTIMISATION
% Two-Objective Optimisation using two performance evaluation models.
% Optimizer: NSGA_2 (http://www.iitk.ac.in/kangal/)
%
% INPUT
% model1	:       Simple Model        [Function]
% model2	:       Detailed Model      [Function]
% m1sets	:       Model 1 Settings    [Structure]       
% m2sets	:       Model 2 Settings    [Structure]    
% optsets   :       Optimizer Settings  [Structure]
%
% OUTPUT
% saves two .mat-files into executing folder with results of optimitzation
% per sequence
%
%
% Christoph Waibel 2015 - christoph.waibel@empa.ch





%% Optimizer Settings        --------------------------------------------------------------------------------------
% -----------------------------------------------------------------------------------------------------------------
pop = optsets.pop;
gen = optsets.gen;
V = optsets.V;
M = optsets.M;
minV = optsets.minV;
maxV = optsets.maxV;
IntegerV = optsets.IntegerV;





%% 1st Sequence:        -------------------------------------------------------------------------------------------
% -----------------------------------------------------------------------------------------------------------------
evalFnc = model1;
get_input = m1sets.get_input;

tic
solutionsModel1=nsga_2(pop, gen, V, M, minV, maxV, IntegerV, evalFnc, get_input);
toc
save('solutionsModel1','solutionsModel1');



%% Select solutions from 1st sequence:        ---------------------------------------------------------------------
% -----------------------------------------------------------------------------------------------------------------
% 1. normalize two objectives M1 and M2
% 2. cluster k means, 4 clusters.  take same amount of solution from each
% cluster. if one cluster has less than solutions to be taken, take more
% from next cluster.
% 3. create set of solutions to be migrated into 2nd Sequence
% 4. some new solutions will be created in the 2nd Sequence
%   4.a) some in the vicinity of the migrated solutions
%   4.b) some entirely new, by random, within the variable search-space

pop = 30;
intClusters = 4;
intMigratePerCluster=round((pop/intClusters)/2.5);

F1=solutionsModel1.chrom(:,17);
F2=solutionsModel1.chrom(:,18);
F1max = max(F1);
F1min = min(F1);
F2max = max(F2);
F2min = min(F2);
F1norm = (F1(:) - F1min) / (F1max - F1min);
F2norm = (F2(:) - F2min) / (F2max - F2min);
Fnorm = [F1norm F2norm];
[idFnorm,Model1ClusterCentroids] = kmeans(Fnorm,intClusters,'Distance','cityblock','Replicates',5);



for i=1:intClusters
    Model1Clusters{i}= solutionsModel1.chrom((idFnorm == i),:);
    intClusterSize = size(Model1Clusters{1,i},1);
    if intClusterSize < intMigratePerCluster
        temp =datasample(Model1Clusters{1,i},intClusterSize,'Replace',false);
    else
        temp =datasample(Model1Clusters{1,i},intMigratePerCluster,'Replace',false);
        intClusterSize = intMigratePerCluster;
    end
    if i==1
        Model1selectedSolutions = temp;
    else
        Model1selectedSolutions = [Model1selectedSolutions; temp];
    end
end
clear temp;






%% 2nd Sequence:        -------------------------------------------------------------------------------------------
% -----------------------------------------------------------------------------------------------------------------
evalFnc = model2;
get_input = m2sets.get_input;

solutionsModel2=nsga_2(pop, gen, V, M, minV, maxV, IntegerV, evalFnc, get_input, Model1selectedSolutions);
solutionsModel2=nsga_2(pop, gen, V, M, minV, maxV, IntegerV, evalFnc, get_input);
save('solutionsModel2','solutionsModel2');




end




